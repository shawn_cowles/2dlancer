﻿/// <reference path="AbstractStationPage.ts" />

// The station menu page for buying a module for a ship.
class ModuleBuyPage extends AbstractStationPage
{
    private moduleListing: AbstractModuleListing;
    private moduleSlot: ModuleSlot;

    constructor(moduleListing: AbstractModuleListing, moduleSlot: ModuleSlot)
    {
        super();

        this.moduleListing = moduleListing;
        this.moduleSlot = moduleSlot;
    }

    public BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void
    {
        title.text = this.moduleListing.Name;

        var currentLoadout = state.PlayerDataService.GetCurrentLoadout();
        
        var sellPrice = 0;

        if (this.moduleSlot.IsModuleMounted())
        {
            sellPrice = this.moduleSlot
                .GetMountedModule()
                .GetListing()
                .CostForSize(this.moduleSlot.Size) / 2;
        }

        var purchaseCost = this.moduleListing.CostForSize(this.moduleSlot.Size) - sellPrice;
        var balance = state.PlayerDataService.GetBalance();

        var moduleAlreadyMounted = this.moduleSlot.IsModuleMounted()
            && this.moduleSlot.GetMountedModule().GetListing().Name == this.moduleListing.Name;

        if (balance >= purchaseCost && !moduleAlreadyMounted)
        {
            buttons.push(new TextButton(state.game, 0, 0, "Purchase",
                () =>
                {
                    state.Analytics.MoneySpent("Module", this.moduleListing.Name, purchaseCost);

                    state.PlayerDataService.SetBalance(balance - purchaseCost);
                    var slotIndex = state.PlayerShip.GetComponents<ModuleSlot>(ModuleSlot)
                        .indexOf(this.moduleSlot);
                    currentLoadout.MountedModules[this.moduleSlot.Type] = this.moduleListing.Name;
                    state.PlayerDataService.SetCurrentLoadout(currentLoadout);

                    this.moduleSlot.MountModule(this.moduleListing.BuildModule());

                    panel.SetPage(new OutfittingStationPage());
                },
                this));
        }

        buttons.push(new TextButton(state.game, 0, 0, "Back",
            () =>
            {
                panel.SetPage(new OutfitSlotPage(this.moduleSlot));
            },
            this));

        var textStyle = {
            font: "16px courier",
            fill: "#00FFFF",
            align: "left",
        };

        var descriptions = this.moduleListing.GetDescriptionsForSize(this.moduleSlot.Size);

        for (var i = 0; i < descriptions.length; i++)
        {
            var text = descriptions[i];

            // append purchase cost to last line of descriptions
            if (i == descriptions.length - 1)
            {
                if (!moduleAlreadyMounted)
                {
                    text = text
                        + " \n \n "
                        + Util.FormatAsMoney(purchaseCost);

                    if (balance < purchaseCost)
                    {
                        text += " (insufficient funds)";
                    }
                }
                else
                {
                    text = text
                        + " \n \n "
                        + "This module is already mounted.";
                }
            }

            var descriptionElement = new Phaser.Text(state.game, 0, 0,
                Util.WrapText(text, 55),
                textStyle);
            state.game.add.existing(descriptionElement);
            mainBodyContent.push(descriptionElement);
        }
    }

    protected OnUpdate(state: FlightState): void
    {
    }
}