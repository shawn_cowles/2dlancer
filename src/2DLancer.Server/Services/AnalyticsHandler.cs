﻿using System;
using System.Collections.Generic;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Common.Messages;
using _2DLancer.Common.Messages.Analytics;
using _2DLancer.Server.Interfaces;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// The AnalyticsHandler handles incoming event messages from clients and passes them on to the analytics provider.
    /// </summary>
    public class AnalyticsHandler : AbstractBusService
    {
        private const int DELAYED_PROCESSING_QUEUE_MAX = 100;

        private readonly ISettingsProvider _settingsProvider;
        private readonly ILogger _logger;

        private Dictionary<string, UserSession> _sessionsByClientId;

        private Queue<AbstractIncomingClientMessage> _delayedProcessingQueue;

        /// <summary>
        /// Construct a new AnalyticsHandler
        /// </summary>
        /// <param name="settingsProvider">The settings provider to use.</param>
        /// <param name="loggerProvider">The logger provider to use.</param>
        public AnalyticsHandler(
            ISettingsProvider settingsProvider,
            ILoggerProvider loggerProvider) 
            : base(MatchPredicate)
        {
            _settingsProvider = settingsProvider;
            _logger = loggerProvider.GetLoggerFor(typeof(AnalyticsHandler));

            _sessionsByClientId = new Dictionary<string, UserSession>();
            _delayedProcessingQueue = new Queue<AbstractIncomingClientMessage>();
        }

        /// <summary>
        /// Handle messages from the bus.
        /// </summary>
        /// <param name="messages">The incoming messages.</param>
        protected override void HandleMessages(AbstractMessage[] messages)
        {
            try
            {
                foreach (var message in messages.OfType<AbstractIncomingClientMessage>())
                {
                    HandleMessage(message);
                }

                // Try processing any delayed messages in the queue
                var queue = _delayedProcessingQueue.ToArray();
                _delayedProcessingQueue.Clear();
                foreach(var message in queue)
                {
                    HandleMessage(message);
                }
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private void HandleMessage(AbstractIncomingClientMessage message)
        {
            if (message is UserConnectedMessage)
            {
                StartSession(message as UserConnectedMessage);
            }
            else if (message is UserDisconnectedMessage)
            {
                EndSession(message as UserDisconnectedMessage);
            }
            else
            {
                HandleEvent(message);
            }
        }
        
        private void StartSession(UserConnectedMessage message)
        {
            if(_sessionsByClientId.ContainsKey(message.ClientId))
            {
                _logger.Error("User connected but a session is already in progress.");
                return;
            }

            var session = new UserSession
            {
                UserId = message.UserId,
                StartDate = DateTime.UtcNow
            };

            _sessionsByClientId.Add(message.ClientId, session);
        }

        private void EndSession(UserDisconnectedMessage message)
        {
            if (!_sessionsByClientId.ContainsKey(message.ClientId))
            {
                _logger.Error("User disconnected, but the session cannot be found.");
                return;
            }
            
            var session = _sessionsByClientId[message.ClientId];
            _sessionsByClientId.Remove(message.ClientId);

            session.EndDate = DateTime.UtcNow;

            SendMessage(new SessionEndedMessage(session));
        }

        private void HandleEvent(AbstractIncomingClientMessage message)
        {
            // Seems to happen fairly often at the start of a session. The session start message is
            // in the same processing batch, so queue it for handling later
            if (!_sessionsByClientId.ContainsKey(message.ClientId))
            {
                QueueMessageForLater(message);
                return;
            }

            var session = _sessionsByClientId[message.ClientId];

            if (message is DockedAtStationEvent)
            {
                session.DockCount += 1;
            }

            if (message is MoneyEarnedEvent)
            {
                session.TotalIncome += (message as MoneyEarnedEvent).Amount;
            }

            if (message is MoneySpentEvent)
            {
                session.TotalSpending += (message as MoneySpentEvent).Amount;
                session.ItemsBought.Add((message as MoneySpentEvent).Category + " : " + (message as MoneySpentEvent).Item);
            }

            if (message is PlayerSpawnedEvent)
            {
                session.SpawnCount += 1;
            }

            if (message is ShipRepairedEvent)
            {
                session.RepairCount += 1;
            }

            if (message is ZoneVisitedEvent)
            {
                session.ZoneTransitions += 1;
                session.ZonesVisited.Add((message as ZoneVisitedEvent).ZoneName);
            }
        }

        private void QueueMessageForLater(AbstractIncomingClientMessage message)
        {
            _delayedProcessingQueue.Enqueue(message);

            while(_delayedProcessingQueue.Count > DELAYED_PROCESSING_QUEUE_MAX)
            {
                _logger.Warn("Delayed processing queue is too full, purging messages.");
                _delayedProcessingQueue.Dequeue();
            }
        }

        private static bool MatchPredicate(AbstractMessage obj)
        {
            return obj is UserConnectedMessage
                || obj is UserDisconnectedMessage
                || obj is DockedAtStationEvent
                || obj is MoneyEarnedEvent
                || obj is MoneySpentEvent
                || obj is PlayerSpawnedEvent
                || obj is ShipRepairedEvent
                || obj is ZoneVisitedEvent;
        }
    }
}
