﻿using System;

namespace _2DLancer.Import
{
    /// <summary>
    /// An exception thrown when something unexpected happens during import.
    /// </summary>
    public class ImportException : Exception
    {
        /// <summary>
        /// Construct a new ImportException.
        /// </summary>
        /// <param name="message">A message describing the error.</param>
        public ImportException(string message)
            :base(message)
        {
        }

        /// <summary>
        /// Construct a new ImportException.
        /// </summary>
        /// <param name="message">A message describing the error.</param>
        /// <param name="innerException">The inner exception.</param>
        public ImportException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
