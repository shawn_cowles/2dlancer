﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A space station.
    /// </summary>
    [TsClass(Module = "", Name = "ISpaceStation")]
    public class SpaceStation
    {
        /// <summary>
        /// The position of the space station.
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// The Name of the space station.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of the space station.
        /// </summary>
        public string Type { get; set; }
    }
}
