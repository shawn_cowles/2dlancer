﻿// Handles collision processing
class CollisionSystem
{
    // Process all collisions
    public ProcessCollisions(zone: Zone, state: FlightState): void
    {
        // Collect Colliders
        var allColliders = new Array<Collider>();
        for (var i = 0; i < zone.GameObjects.length; i++)
        {
            var colliders = zone.GameObjects[i].GetComponents<Collider>(Collider);

            for (var j = 0; j < colliders.length; j++)
            {
                allColliders.push(colliders[j]);
            }
        }
        
        // Collision Processing
        for (var i = 0; i < allColliders.length; i++)
        {
            for (var j = i + 1; j < allColliders.length; j++)
            {
                var a = allColliders[i];
                var b = allColliders[j];

                if (a.CollidesWith(b))
                {
                    a.GameObject.OnCollided(zone, state, b.GameObject);
                    b.GameObject.OnCollided(zone, state, a.GameObject);

                    // for normal collisions, the objects should bounce off eachother
                    if (!a.TriggerOnly && !b.TriggerOnly)
                    {
                        this.HandlePhysicsCollisions(zone, a.GameObject, b.GameObject);
                    }
                }
            }
        }
    }

    private HandlePhysicsCollisions(zone: Zone, a: GameObject, b: GameObject): void
    {
        var aColliders = a.GetComponents<Collider>(Collider);
        var bColliders = b.GetComponents<Collider>(Collider);

        var overlap = this.DetermineOverlap(aColliders, bColliders);

        if (overlap > 0)
        {
            var bToA = a.Position.Subtract(b.Position).Normalize();
            var aToB = b.Position.Subtract(a.Position).Normalize();

            // move the objects away from eachother until they don't overlap, but don't
            // move objects without a PhysicsBody
            if (a.PhysicsBody != null)
            {
                a.Position = a.Position
                    .Add(bToA.Scale(overlap / 2));
            }
            if (b.PhysicsBody != null)
            {
                b.Position = b.Position
                    .Add(aToB.Scale(overlap / 2));
            }

            var aVel = Vector2.ZERO;
            var bVel = Vector2.ZERO;

            if (a.PhysicsBody != null)
            {
                aVel = a.PhysicsBody.Velocity;
            }
            if (b.PhysicsBody != null)
            {
                bVel = b.PhysicsBody.Velocity;
            }

            // Bounce back closing velocities
            var aClosingVel = aVel.Dot(aToB);
            var bClosingVel = bVel.Dot(bToA);

            if (a.PhysicsBody != null)
            {
                a.PhysicsBody.Velocity = aVel.Add(bToA.Scale(aClosingVel));
            }

            if (b.PhysicsBody != null)
            {
                b.PhysicsBody.Velocity = bVel.Add(aToB.Scale(bClosingVel));
            }

            var collisionDamage = aClosingVel / 25 + bClosingVel / 25;

            // Damage for high speed collisions
            if (collisionDamage >= 1)
            {
                Mechanics.ResolveAttack(zone, null, a, collisionDamage / 2, false);
                Mechanics.ResolveAttack(zone, null, b, collisionDamage / 2, false);
            }
        }
    }

    private DetermineOverlap(aColliders: Collider[], bColliders: Collider[]): number
    {
        var max = -1;

        for (var i = 0; i < aColliders.length; i++)
        {
            for (var j = 0; j < bColliders.length; j++)
            {
                var overlap = aColliders[i].GreatestOverlap(bColliders[j]);

                if (overlap > max)
                {
                    max = overlap;
                }
            }
        }

        return max;
    }
}