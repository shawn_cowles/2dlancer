﻿// Holds constants for ship design
class DesignConstants
{
    public static EngineThrustPerSize = 250000;
    public static EngineTorquePerSize = 750;
    public static ShieldHealthPerSize = 10;
    public static ShieldRegenPerSize = 1;
    public static ShieldRechargeDelay = 5;
    public static LateralAccelerationFactor = 0.5;
    public static WeaponDpsPerSize = 3;
    public static COST_PER_WEAPON_SIZE = 50;
    public static GimballedWeaponArc = Math.PI / 4;
    public static TurretTurnRate = 3;
}