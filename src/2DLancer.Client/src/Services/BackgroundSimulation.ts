﻿// The background simulation of the game, handling economics and station ownership
class BackgroundSimulation
{
    public Stations: SpaceStationBehavior[];
    public Actors: Actor[];
    private state: FlightState;

    constructor()
    {
        this.Stations = [];
        this.Actors = [];
    }

    public Initialize(state: FlightState): void
    {
        this.state = state;
        var zones = state.Zones;
        var navigationSystem = state.Navigation;

        for (var i = 0; i < zones.length; i++)
        {
            var zone = zones[i];
            
            for (var j = 0; j < zone.GameObjects.length; j++)
            {
                var stationBehavior = zone.GameObjects[j].GetComponent<SpaceStationBehavior>(SpaceStationBehavior);

                if (stationBehavior != null)
                {
                    this.Stations.push(stationBehavior);
                    stationBehavior.NearestWaypoint = this.FindNearestWaypoint(
                        zone.Name,
                        stationBehavior.GameObject.Position,
                        navigationSystem.Waypoints);
                }
            }

            for (var j = 0; j < zone.Actors.length; j++)
            {
                this.Actors.push(zone.Actors[j]);
            }
        }
    }

    public Update(state: FlightState, elapsedSeconds: number): void
    {
        for (var i = 0; i < this.Actors.length; i++)
        {
            var actor = this.Actors[i];

            if (this.Actors[i].IsDead)
            {
                actor.Zone.Actors.splice(actor.Zone.Actors.indexOf(actor), 1);
                this.Actors.splice(i, 1);
                i--;
            }
            else
            {
                actor.Update(state, this, elapsedSeconds);
            }
        }
    }

    private FindNearestWaypoint(zoneName: string, position: Vector2, waypoints: Waypoint[]): Waypoint
    {
        var nearest = <Waypoint>null;
        var nearDist = 0;

        for (var i = 0; i < waypoints.length; i++)
        {
            var waypoint = waypoints[i];

            if (waypoint.ZoneName == zoneName)
            {
                var dist = waypoint.Position.DistanceTo(position);

                if (nearest == null || dist < nearDist)
                {
                    nearest = waypoint;
                    nearDist = dist;
                }
            }
        }

        return nearest;
    }

    public AddActor(actor: Actor): void
    {
        // add actor to tracking
        this.Actors.push(actor);

        // add actor to zone
        this.AddActorToZone(actor, actor.Zone, false);
    }

    public MoveToZone(actor: Actor, destination: Zone): void
    {
        // remove actor from old zone
        actor.Zone.Actors.splice(actor.Zone.Actors.indexOf(actor), 1);

        // add actor to new zone
        this.AddActorToZone(actor, destination, true);
    }

    private AddActorToZone(actor: Actor, zone: Zone, showSpawnEffect: boolean): void
    {
        actor.Zone = zone;
        actor.Zone.Actors.push(actor);

        // if adding an actor to the active zone, create it's GameObject at the same time
        if (this.state.GetActiveZone() == actor.Zone)
        {
            var obj = actor.BuildGameObject(this.state);

            if (obj != null)
            {
                if (showSpawnEffect)
                {
                    var jumpInEffect = ShipBuilder.BuildJumpInEffect(
                        actor.Position,
                        actor.Position.DirectionTo(zone.ZoneCenter),
                        obj);

                    this.state.GetActiveZone().GameObjects.push(jumpInEffect);
                }
                else
                {
                    this.state.GetActiveZone().GameObjects.push(obj);
                }
            }
        }
    }
}