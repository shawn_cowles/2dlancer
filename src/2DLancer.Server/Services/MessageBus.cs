﻿using System;
using System.Collections.Generic;
using System.Linq;
using _2DLancer.Common.Messages;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// A message bus that handles inter-service communication.
    /// </summary>
    public class MessageBus
    {
        private List<AbstractMessage> _messages;

        /// <summary>
        /// Construct a new MessageBus
        /// </summary>
        public MessageBus()
        {
            _messages = new List<AbstractMessage>();
        }

        /// <summary>
        /// Put a message on the bus.
        /// </summary>
        /// <param name="message">The message</param>
        public void SendMessage(AbstractMessage message)
        {
            lock(_messages)
            {
                _messages.Add(message);
            }
        }

        /// <summary>
        /// Remove and return a set of messages from the bus.
        /// </summary>
        /// <param name="match">The predicate to match messages on.</param>
        /// <returns>All messages on the bus that match the predicate.</returns>
        public AbstractMessage[] GetMessages(Predicate<AbstractMessage> match)
        {
            lock(_messages)
            {
                var toReturn = _messages.Where(m => match(m)).ToArray();

                _messages.RemoveAll(match);

                return toReturn;
            }
        }
    }
}
