﻿using System.Configuration;
using _2DLancer.Server.Interfaces;
using SuperSocket.SocketBase.Config;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// Provide configuration settings from the application configuration file.
    /// </summary>
    public class AppConfigSettingsProvider : ISettingsProvider
    {
        /// <summary>
        /// The directory for loading and storing data.
        /// </summary>
        public string GetDataDirectory
        {
            get { return ConfigurationManager.AppSettings["data_dir"]; }
        }

        /// <summary>
        /// Get the websocket configuration section. 
        /// </summary>
        /// <returns> The websocket configuration section.</returns>
        public IConfigurationSource GetWebsocketConfiguration()
        {
            return ConfigurationManager.GetSection("superSocket") as IConfigurationSource;
        }
    }
}
