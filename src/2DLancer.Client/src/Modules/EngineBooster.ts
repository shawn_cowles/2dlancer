﻿/// <reference path="AbstractModule.ts" />

// The EngineBooster can provide a temporary speed boost.
class EngineBooster extends AbstractModule
{
    public static BOOST_FACTOR = 1.5;

    private boostDuration: number;
    private boostCooldown: number;
    private cooldownTimer: number;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.boostDuration = EngineBooster.DurationForSize(slot.Size);
        this.boostCooldown = EngineBooster.CooldownForSize(slot.Size);
        
        this.cooldownTimer = 0;
    }
    
    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = this.boostCooldown;

            parentShip
                .GetComponent<Thrusters>(Thrusters)
                .TriggerBoost(this.boostDuration);
        }
    }
    
    public UnInitialize(parentShip: GameObject): void
    {
    }
    
    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / this.boostCooldown, 1, 0);
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public IconSprite(): string
    {
        return "icon_engine_boost";
    }

    public Name(): string
    {
        return "Engine Boost";
    }

    public GetListing(): AbstractModuleListing
    {
        return new EngineBoosterListing();
    }

    public static DurationForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 5;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 10;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 15;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }

    public static CooldownForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        return EngineBooster.DurationForSize(moduleSize) + 5;
    }
}

class EngineBoosterListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Engine Boost";
        this.BaseCost = 100;
        this.Type = _2DLancer.Common.Data.ModuleType.Booster;
    }

    public BuildModule(): AbstractModule
    {
        return new EngineBooster();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        return [
            "Speed Boost: " + EngineBooster.BOOST_FACTOR + "x"
            + "  Duration: " + EngineBooster.DurationForSize(moduleSize) + "s"
            + "  Cooldown: " + EngineBooster.CooldownForSize(moduleSize) + "s",

            "An Engine Booster is a series of capacitors tuned to deliver a boost of power to a ship's thrusters. The effect is a momentary boost of speed, enough to escape a dangerous situation or hasten a long journey."
        ];
    }
}