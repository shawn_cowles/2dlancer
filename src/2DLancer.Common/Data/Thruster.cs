﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// An thruster mount on a ship.
    /// </summary>
    [TsClass(Module = "", Name = "IThruster")]
    public class Thruster
    {
        /// <summary>
        /// The position offset of the thruster on the ship.
        /// </summary>
        public Position Offset { get; set; }

        /// <summary>
        /// The facing direction of the thruster.
        /// </summary>
        public double Direction { get; set; }
    }
}
