﻿/// <reference path="UiPanel.ts" />

// A widget that displays ship status information.
class ShipStatusPanel extends UiPanel
{
    private static WIDTH = 200;
    private static HEIGHT = 75;

    private state: FlightState;

    private shieldText: Phaser.Text;
    private hullText: Phaser.Text;
    
    private moduleButtons: ModuleButton[];

    constructor()
    {
        super(new Phaser.Point(0, 0), ShipStatusPanel.WIDTH, ShipStatusPanel.HEIGHT);
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.shieldText = state.game.add.text(0, 0, "", textStyle);
        this.shieldText.anchor.set(0.5, 0.5);
        this.hullText = state.game.add.text(0, 0, "", textStyle);
        this.hullText.anchor.set(0.5, 0.5);
    }

    // Update the minimap
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        var xPosition = state.game.camera.width / 2 + state.game.camera.view.x;
        var yPosition = state.game.camera.height - ShipStatusPanel.HEIGHT + state.game.camera.view.y;

        this.CenterPosition = new Phaser.Point(xPosition, yPosition);

        // Call update after we find the center, so UI elements are arrayed correctly
        super.Update(state, elapsedSeconds);

        this.shieldText.position.set(
            xPosition,
            yPosition - 20);
        this.hullText.position.set(
            xPosition,
            yPosition + 20);

        if (this.state.PlayerShip != null && !this.state.PlayerShip.ShouldBeRemoved)
        {
            if (this.state.PlayerShip.HasComponent(ShieldGenerator))
            {
                this.shieldText.text = "Shields: "
                    + this.state.PlayerShip
                        .GetComponent<ShieldGenerator>(ShieldGenerator)
                        .StrengthPercent()
                    + "%";
            }
            else
            {
                this.shieldText.text = "";
            }

            this.hullText.text = "Hull: "
                + this.state.PlayerShip
                    .GetComponent<HullHealth>(HullHealth)
                    .HealthPercent()
                + "%";

            // rebuild the module buttons
            if (this.moduleButtons == null)
            {
                var hotkey = 1;
                this.moduleButtons = [];
                var moduleSlots = state.PlayerShip.GetComponents<ModuleSlot>(ModuleSlot);
                var types = [
                    _2DLancer.Common.Data.ModuleType.PrimaryWeapon,
                    _2DLancer.Common.Data.ModuleType.SecondaryWeapon,
                    _2DLancer.Common.Data.ModuleType.Bay,
                    _2DLancer.Common.Data.ModuleType.Booster
                ];

                for (var i = 0; i < types.length; i++)
                {
                    for (var j = 0; j < moduleSlots.length; j++)
                    {
                        if (moduleSlots[j].Type == types[i] &&  moduleSlots[j].IsModuleMounted())
                        {
                            this.moduleButtons.push(new ModuleButton(
                                state.game,
                                0, 0,
                                moduleSlots[j],
                                state));

                            hotkey += 1;
                        }
                    }
                }
            }
        }
        else
        {
            this.shieldText.text = "";
            this.hullText.text = "";

            if (this.moduleButtons != null)
            {
                // clear the module buttons so they can be rebuilt
                for (var i = 0; i < this.moduleButtons.length; i++)
                {
                    this.moduleButtons[i].destroy();
                }
                this.moduleButtons = null;
            }
        }

        if (this.moduleButtons != null)
        {
            var row = 0;
            var column = 0;
            
            for (var i = 0; i < this.moduleButtons.length; i++)
            {
                if (row > 2)
                {
                    row = 0;
                    column += 1;
                }

                this.moduleButtons[i].position.set(
                    xPosition + ShipStatusPanel.WIDTH / 2 + 45 + 55 * column,
                    yPosition - ShipStatusPanel.HEIGHT / 2 + 35 * row);

                row += 1;
            }
        }
    }

    public Destroy()
    {
        super.Destroy();

        this.shieldText.destroy();
        this.hullText.destroy();

        for (var i = 0; i < this.moduleButtons.length; i++)
        {
            this.moduleButtons[i].destroy();
        }
    }

    // Is the pointer over the panel?
    public PointIsInPanel(point: Vector2): boolean
    {
        if (super.PointIsInPanel(point))
        {
            return true;
        }

        if (this.moduleButtons != null)
        {
            for (var i = 0; i < this.moduleButtons.length; i++)
            {
                if (this.moduleButtons[i].input.pointerOver())
                {
                    return true;
                }
            }
        }

        return false;
    }
}