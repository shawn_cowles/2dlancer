﻿// Handles creating ships.
class ShipBuilder
{
    public static CLASS_TALIS = "Talis";
    public static CLASS_PULSAR = "Pulsar";
    public static CLASS_CANIS = "Canis";
    public static CLASS_CHERUBIM = "Cherubim";
    public static CLASS_JUNKER = "Junker";
    public static CLASS_LANCER = "Lancer";
    public static CLASS_QUASAR = "Quasar";

    public static WEAPON_GUN = "Gun";
    public static WEAPON_LASER = "Laser";

    private shipDataList: IShipData[];

    public Initalize(state: FlightState): void
    {
        this.shipDataList = <IShipData[]>state.game.cache.getJSON("ships");
    }

    // Build a ship.
    public BuildShip(
        position: Vector2,
        facing: number,
        loadout: ShipLoadout,
        controlFactory: () => AbstractComponent): GameObject
    {
        var shipData = this.GetShipData(loadout.ShipClass);
        
        var ship = this.BuildFromData(
            position,
            facing,
            shipData,
            loadout.Color,
            loadout.FactionId,
            controlFactory,
            loadout.MountedModules);
        
        if (loadout.ShowSpawnEffect)
        {
            var jumpEffect = ShipBuilder.BuildJumpEffect(position, facing);

            jumpEffect.AddComponent(new DelayedSpawn(0.15, ship));

            return jumpEffect;
        }
        else
        {
            return ship;
        }
    }

    public GetShipData(className: string): IShipData
    {
        for (var i = 0; i < this.shipDataList.length; i++)
        {
            if (this.shipDataList[i].Name == className)
            {
                return this.shipDataList[i];
            }
        }
        
        throw "Unknown ship class " + className;
    }

    // Internal, build a ship from loaded data
    private BuildFromData(
        position: Vector2,
        facing: number,
        shipData: IShipData,
        color: number,
        factionId: string,
        controlFactory: () => AbstractComponent,
        mountedModules: string[]): GameObject
    {
        var ship = new GameObject(position, facing);

        ship.AddComponent(new PhysicsBody(shipData.Mass));

        ship.AddComponent(new SpriteDisplay(
            shipData.BaseSprite,
            FlightState.LAYER_MIDGROUND,
            HexColor.WHITE,
            shipData.Scale));

        ship.AddComponent(new SpriteDisplay(
            shipData.PaintSprite,
            FlightState.LAYER_MIDGROUND,
            color,
            shipData.Scale));

        ship.AddComponent(new FactionIdentifier(factionId));

        var colliders = new Array<CollisionCircle>();
        for (var i = 0; i < shipData.Colliders.length; i++)
        {
            colliders.push(new CollisionCircle(
                Vector2.FromPosition(shipData.Colliders[i].Offset),
                shipData.Colliders[i].Radius));
        }

        ship.AddComponent(new Collider(colliders, false));

        for (var i = 0; i < shipData.Engines.length; i++)
        {
            ship.AddComponent(new EngineJet(
                Vector2.FromPosition(shipData.Engines[i].Offset),
                color));

            ship.AddComponent(new EngineTrail(
                Vector2.FromPosition(shipData.Engines[i].Offset),
                shipData.Engines[i].Width,
                color));
        }
        
        ship.AddComponent(new InputSource());

        ship.AddComponent(new HullHealth(shipData.Health));
        ship.AddComponent(new ShieldGenerator(shipData.MaxShieldClass, shipData.ShieldRadius));
        ship.AddComponent(new SmokeOnDamageBehavior(
            shipData.SpriteWidth * shipData.Scale,
            shipData.SpriteHeight * shipData.Scale));

        ship.AddComponent(new Thrusters(shipData.MaxEngineClass, shipData.Drag));

        for (var i = 0; i < shipData.Thrusters.length; i++)
        {
            ship.AddComponent(new ThrusterJet(
                Vector2.FromPosition(shipData.Thrusters[i].Offset),
                color,
                shipData.Thrusters[i].Direction));
        }

        var debrisSprites = new Array<string>();
        var debrisDirections = new Array<number>();

        for (var i = 0; i < shipData.Debris.length; i++)
        {
            debrisSprites.push(shipData.Debris[i].SpriteName);
            debrisDirections.push(shipData.Debris[i].LaunchDirection);
        }

        ship.AddComponent(new ExplodeIntoDebris(
            debrisSprites,
            shipData.Scale,
            debrisDirections,
            20,
            10,
            shipData.SpriteWidth,
            shipData.SpriteHeight));

        for (var i = 0; i < shipData.ModuleSlots.length; i++)
        {
            var offsets = new Array<Vector2>();
            for (var j = 0; j < shipData.ModuleSlots[i].Offsets.length; j++)
            {
                offsets.push(Vector2.FromPosition(shipData.ModuleSlots[i].Offsets[j]));
            }

            var slot = new ModuleSlot(
                offsets,
                shipData.ModuleSlots[i].Size,
                shipData.ModuleSlots[i].Type,
                shipData.TurretedWeapons);

            if (mountedModules[i] != null)
            {
                var listing = AbstractModuleListing.GetByName(mountedModules[slot.Type]);

                // silently swallow modules who's type doesn't match, likely due to old layouts
                if (listing != null && listing.Type == slot.Type)
                {
                    slot.MountModule(listing.BuildModule());
                }
            }
            
            ship.AddComponent(slot);
        }

        ship.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_SHIP));

        ship.AddComponent(new LeadIndicator());

        if (controlFactory != null)
        {
            ship.AddComponent(controlFactory());
        }

        ship.AddComponent(new PurchaseInformation(shipData));

        return ship;
    }
    
    // Build a ftl jump effect for spawning or despawning ships
    public static BuildJumpEffect(position: Vector2, facing: number): GameObject
    {
        var jumpEffect = new GameObject(position, facing)

        jumpEffect.AddComponent(new FadingLine(
            Vector2.ZERO,
            position.Add(Vector2.FromPolar(-500, facing)),
            null,
            HexColor.WHITE,
            10,
            0.25,
            true));
        jumpEffect.AddComponent(new TimedLife(0.5, false));
        jumpEffect.AddComponent(new GrowingAndFadingSprite(
            "huge_blast",
            FlightState.LAYER_OVERLAY,
            HexColor.WHITE,
            0.3,
            0.5,
            3,
            1,
            0));

        return jumpEffect;
    }

    // Build an FTL jump effect for a ship leaving a zone.
    public static BuildJumpOutEffect(jumpingShip: GameObject): GameObject
    {
        var jumpEffect = new GameObject(jumpingShip.Position, jumpingShip.Facing);

        var engineTrails = jumpingShip.GetComponents<EngineTrail>(EngineTrail);

        for (var i = 0; i < engineTrails.length; i++)
        {
            var from = Vector2.FromPolarOffset(
                engineTrails[i].OffsetMagnitude,
                engineTrails[i].OffsetDirection,
                jumpingShip);

            var to = from.Add(Vector2.FromPolar(1000, jumpingShip.Facing));

            jumpEffect.AddComponent(new FadingLine(
                from.Subtract(jumpingShip.Position),
                to,
                null,
                HexColor.WHITE,
                engineTrails[i].Width,
                0.25,
                true));
        }

        jumpEffect.AddComponent(new SoundEffect("jump_leave"));
        jumpEffect.AddComponent(new TimedLife(1, false));
        
        return jumpEffect;
    }

    // Build an FTL jump effect for a ship entering a zone.
    public static BuildJumpInEffect(position: Vector2, facing: number, jumpingShip: GameObject): GameObject
    {
        var jumpEffect = new GameObject(position, facing);

        var engineTrails = jumpingShip.GetComponents<EngineTrail>(EngineTrail);

        for (var i = 0; i < engineTrails.length; i++)
        {
            var fromOffset = Vector2.FromPolarOffset(
                engineTrails[i].OffsetMagnitude,
                engineTrails[i].OffsetDirection,
                jumpingShip)
                .Subtract(jumpingShip.Position);

            var to = position
                .Add(fromOffset)
                .Add(Vector2.FromPolar(-1000, facing));

            jumpEffect.AddComponent(new FadingLine(
                fromOffset,
                to,
                null,
                HexColor.WHITE,
                engineTrails[i].Width,
                0.25,
                true));
        }

        jumpEffect.AddComponent(new DelayedSpawn(0, jumpingShip, 100));

        jumpEffect.AddComponent(new TimedLife(0.5, false));

        var wrapperObject = new GameObject(position, facing);
        wrapperObject.AddComponent(new DelayedSpawn(1, jumpEffect));
        wrapperObject.AddComponent(new TimedLife(2, false));
        wrapperObject.AddComponent(new SoundEffect("jump_enter"));

        return wrapperObject;
    }
}