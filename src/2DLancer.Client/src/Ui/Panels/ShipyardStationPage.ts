﻿/// <reference path="AbstractStationPage.ts" />

// The station menu page for the shipyard. Ships available for purchase.
class ShipyardStationPage extends AbstractStationPage
{
    public BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void
    {
        title.text = "Shipyard";

        var shipTypesAvailable = [
            state.ShipBuilder.GetShipData(ShipBuilder.CLASS_CANIS),
            state.ShipBuilder.GetShipData(ShipBuilder.CLASS_CHERUBIM),
            state.ShipBuilder.GetShipData(ShipBuilder.CLASS_PULSAR),
            state.ShipBuilder.GetShipData(ShipBuilder.CLASS_TALIS),
            state.ShipBuilder.GetShipData(ShipBuilder.CLASS_QUASAR)
        ];

        for (var i = 0; i < shipTypesAvailable.length; i++)
        {
            // becuase javascript scoping
            buttons.push(this.BuildShipBuyButton(shipTypesAvailable[i], state, panel));
        }
        
        buttons.push(new TextButton(state.game, 0, 0, "Back",
            () =>
            {
                panel.SetPage(new MainStationPage());
            },
            this));

        var descriptionElement = new Phaser.Text(state.game, 0, 0,
            Util.WrapText("The Shipyard is a massive factory, mostly automated, capable of producing a variety of ships in its massive auto-fabricator bays. \n \n Shipyards license ship designs from the various ship designers throughout the galaxy, building them all locally.", 55),
            this.BodyTextStyle);
        state.game.add.existing(descriptionElement);
        mainBodyContent.push(descriptionElement);
    }

    protected OnUpdate(state: FlightState): void
    {
    }

    private BuildShipBuyButton(
        shipData: IShipData,
        state: FlightState,
        panel: SpaceStationInteractionPanel): TextButton
    {
        var buttonName = shipData.Name;

        if (state.PlayerDataService.GetCurrentLoadout().ShipClass == shipData.Name)
        {
            buttonName = "* " + buttonName + " *";
        }

        return new TextButton(state.game, 0, 0, buttonName,
            () =>
            {
                panel.SetPage(new ShipBuyPage(shipData));
            },
            this); 
    }
}