﻿/// <reference path="AbstractModule.ts" />

// The EmpBurst fires an area of effect disabling blast.
class EmpBurst extends AbstractModule
{
    public static COOLDOWN = 30;
    private static SPRITE_SIZE = 200;

    private duration: number;
    private effectRadius: number;
    private cooldownTimer: number;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.effectRadius = EmpBurst.AreaForSize(slot.Size);
        this.duration = EmpBurst.DurationForSize(slot.Size);
        this.cooldownTimer = 0;
    }
    
    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = EmpBurst.COOLDOWN;

            var finalScale = 1 / EmpBurst.SPRITE_SIZE * this.effectRadius;

            var burst = new GameObject(parentShip.Position, 0);
            burst.AddComponent(new OffsetFollow(parentShip));
            burst.AddComponent(new GrowingAndFadingSprite(
                "emp_shockwave",
                FlightState.LAYER_MIDGROUND,
                HexColor.WHITE,
                0.3,
                0.1,
                finalScale,
                1,
                0));
            burst.AddComponent(new SoundEffect("emp_blast"));
            burst.AddComponent(new TimedLife(1, false));

            zone.GameObjects.push(burst);

            for (var i = 0; i < zone.GameObjects.length; i++)
            {
                var obj = zone.GameObjects[i];

                var distance = obj.Position.DistanceTo(parentShip.Position);

                if (obj != parentShip && distance < this.effectRadius)
                {
                    if (obj.HasComponent(InputSource))
                    {
                        obj.AddComponent(new TemporaryDisable(this.duration));
                    }

                    var shield = obj.GetComponent<ShieldGenerator>(ShieldGenerator);

                    if (shield != null)
                    {
                        shield.ForceDropShields();
                    }

                    // kill mines and missiles
                    if (obj.HasComponent(HomingProjectileBehavior) || obj.HasComponent(SnareMineBehavior))
                    {
                        var blast = new GameObject(obj.Position, 0);
                        blast.AddComponent(new TimedLife(0.5, false));
                        blast.AddComponent(new GrowingAndFadingSprite(
                            "large_blast", FlightState.LAYER_MIDGROUND, HexColor.WHITE, 0.5,
                            0, 1, // scale
                            1, 0)); // alpha
                        zone.GameObjects.push(blast);

                        obj.ShouldBeRemoved = true;
                    }
                }
            }
        }
    }
    
    public UnInitialize(parentShip: GameObject): void
    {
    }
    
    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / EmpBurst.COOLDOWN, 1, 0);
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public IconSprite(): string
    {
        return "icon_emp";
    }

    public Name(): string
    {
        return "EMP";
    }

    public GetListing(): AbstractModuleListing
    {
        return new EmpBurstListing();
    }

    public static DurationForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 2;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 4;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 6;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }

    public static AreaForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 500;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 700;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 900;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }
}

class EmpBurstListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "EMP";
        this.BaseCost = 300;
        this.Type = _2DLancer.Common.Data.ModuleType.Booster;
    }

    public BuildModule(): AbstractModule
    {
        return new EmpBurst();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        return [
            "Duration: " + EmpBurst.DurationForSize(moduleSize) + "s"
            + "  Cooldown: " + EmpBurst.COOLDOWN + "s"
            + "  Area: " + EmpBurst.AreaForSize(moduleSize) + "m",

            "The EMP Burst, when triggered, emits an blast of energy that disables nearby ships (friend or foe) for a short duration and destroys any mines or missiles."
        ];
    }
}