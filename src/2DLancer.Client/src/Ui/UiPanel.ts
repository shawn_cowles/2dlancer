﻿/// <reference path="AbstractUiWidget.ts" />

// Base class for UiWidgets that use the sliced window.
abstract class UiPanel extends AbstractUiWidget
{
    public static SLICE_SIZE = 15;

    public CenterPosition: Phaser.Point;

    private width: number;
    private height: number;

    private ulSprite: Phaser.Sprite;
    private topSprite: Phaser.Sprite;
    private urSprite: Phaser.Sprite;
    private leftSprite: Phaser.Sprite;
    private centerSprite: Phaser.Sprite;
    private rightSprite: Phaser.Sprite;
    private llSprite: Phaser.Sprite;
    private bottomSprite: Phaser.Sprite;
    private lrSprite: Phaser.Sprite;

    constructor(center: Phaser.Point, width: number, height: number)
    {
        super();

        this.CenterPosition = center;
        this.width = width;
        this.height = height;
    }

    // Create the sprites for the panel
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);
         
        this.ulSprite = state.game.add.sprite(0, 0, "sliced_window", 0,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.ulSprite.anchor.set(0.5, 0.5);
        this.topSprite = state.game.add.sprite(0, 0, "sliced_window", 1,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.topSprite.anchor.set(0.5, 0.5);
        this.urSprite = state.game.add.sprite(0, 0, "sliced_window", 2,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.urSprite.anchor.set(0.5, 0.5);
        this.leftSprite = state.game.add.sprite(0, 0, "sliced_window", 3,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.leftSprite.anchor.set(0.5, 0.5);
        this.centerSprite = state.game.add.sprite(0, 0, "sliced_window", 4,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.centerSprite.anchor.set(0.5, 0.5);
        this.rightSprite = state.game.add.sprite(0, 0, "sliced_window", 5,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.rightSprite.anchor.set(0.5, 0.5);
        this.llSprite = state.game.add.sprite(0, 0, "sliced_window", 6,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.llSprite.anchor.set(0.5, 0.5);
        this.bottomSprite = state.game.add.sprite(0, 0, "sliced_window", 7,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.bottomSprite.anchor.set(0.5, 0.5);
        this.lrSprite = state.game.add.sprite(0, 0, "sliced_window", 8,
            state.SpriteGroups[FlightState.LAYER_OVERLAY]);
        this.lrSprite.anchor.set(0.5, 0.5);
    }

    // Update sprite positions around the center
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        this.ForceUpdatePosition();
    }

    public ForceUpdatePosition(): void
    {
        var hScale = (this.width - UiPanel.SLICE_SIZE) / UiPanel.SLICE_SIZE;
        var vScale = (this.height - UiPanel.SLICE_SIZE) / UiPanel.SLICE_SIZE;

        this.ulSprite.position.set(
            this.CenterPosition.x - this.width / 2,
            this.CenterPosition.y - this.height / 2);

        this.topSprite.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y - this.height / 2);
        this.topSprite.scale.set(hScale, 1);

        this.urSprite.position.set(
            this.CenterPosition.x + this.width / 2,
            this.CenterPosition.y - this.height / 2);


        this.leftSprite.position.set(
            this.CenterPosition.x - this.width / 2,
            this.CenterPosition.y);
        this.leftSprite.scale.set(1, vScale);

        this.centerSprite.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y);
        this.centerSprite.scale.set(hScale, vScale);

        this.rightSprite.position.set(
            this.CenterPosition.x + this.width / 2,
            this.CenterPosition.y);
        this.rightSprite.scale.set(1, vScale);


        this.llSprite.position.set(
            this.CenterPosition.x - this.width / 2,
            this.CenterPosition.y + this.height / 2);

        this.bottomSprite.position.set(
            this.CenterPosition.x,
            this.CenterPosition.y + this.height / 2);
        this.bottomSprite.scale.set(hScale, 1);

        this.lrSprite.position.set(
            this.CenterPosition.x + this.width / 2,
            this.CenterPosition.y + this.height / 2);
    }

    public Destroy()
    {
        super.Destroy();

        this.ulSprite.destroy();
        this.topSprite.destroy();
        this.urSprite.destroy();
        this.leftSprite.destroy();
        this.centerSprite.destroy();
        this.rightSprite.destroy();
        this.llSprite.destroy();
        this.bottomSprite.destroy();
        this.lrSprite.destroy();
    }

    public SetVisible(visible: boolean): void
    {
        this.ulSprite.visible = visible;
        this.topSprite.visible = visible;
        this.urSprite.visible = visible;
        this.leftSprite.visible = visible;
        this.centerSprite.visible = visible;
        this.rightSprite.visible = visible;
        this.llSprite.visible = visible;
        this.bottomSprite.visible = visible;
        this.lrSprite.visible = visible;
    }

    // Is the pointer over the panel?
    public PointIsInPanel(point: Vector2): boolean
    {
        if (this.centerSprite == null || !this.centerSprite.visible)
        {
            return false;
        }

        return this.CenterPosition.x - this.width / 2 <= point.X
            && point.X <= this.CenterPosition.x + this.width / 2
            && this.CenterPosition.y - this.height / 2 <= point.Y
            && point.Y <= this.CenterPosition.y + this.height / 2;
    }
}