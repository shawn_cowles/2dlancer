﻿using _2DLancer.Common.Data;

namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// A message signifying that a user's session has ended.
    /// </summary>
    public class SessionEndedMessage : AbstractMessage
    {
        /// <summary>
        /// The session that ended.
        /// </summary>
        public UserSession Session { get; private set; } 

        /// <summary>
        /// Construct a new SessionEndedMessage.
        /// </summary>
        /// <param name="session">The session that ended.</param>
        public SessionEndedMessage(UserSession session)
        {
            Session = session;
        }
    }
}
