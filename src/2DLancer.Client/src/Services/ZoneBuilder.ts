﻿// Builds preset zones
class ZoneBuilder
{
    private game: Phaser.Game;
    
    constructor(game: Phaser.Game)
    {
        this.game = game;
    }
    
    public BuildZones(): Zone[]
    {
        var zoneData = <IZone[]>this.game.cache.getJSON("zones");
        var zones = new Array<Zone>();

        for (var i = 0; i < zoneData.length; i++)
        {
            zones.push(this.BuildZone(zoneData[i]));
        }

        return zones;
    }

    private BuildZone(zoneData: IZone): Zone
    {
        var zone = new Zone(zoneData.Name, zoneData.FriendlyName, zoneData.Width, zoneData.Height);
        
        for (var i = 0; i < zoneData.Celestials.length; i++)
        {
            var backgroundObject = zoneData.Celestials[i];

            switch (backgroundObject.Type)
            {
                case "asteroid_0":
                    zone.GameObjects.push(this.BuildAsteroid0(backgroundObject));
                    break;
                case "asteroid_1":
                    zone.GameObjects.push(this.BuildAsteroid1(backgroundObject));
                    break;
                default:
                    throw "Unknown celestial type: " + backgroundObject.Type;
            }
        }

        this.PlaceBackgroundDecorations(zone, zoneData);

        for (var i = 0; i < zoneData.BackgroundObjects.length; i++)
        {
            zone.GameObjects.push(this.BuildBackgroundObject(zoneData.BackgroundObjects[i]));
        }
        
        for (var i = 0; i < zoneData.Spawners.length; i++)
        {
            var spawner = zoneData.Spawners[i];

            if (spawner.IsPlayerSpawner)
            {
                zone.Actors.push(new PlayerSpawner(
                    Vector2.FromPosition(spawner.Position),
                    zone,
                    spawner.Color,
                    spawner.Faction,
                    3,
                    spawner.Facing));
            }
            else // bot spawner
            {
                var loadout = new ShipLoadout(
                    spawner.ShipClass,
                    spawner.Color,
                    spawner.Faction,
                    false,
                    [spawner.WeaponClass]);

                zone.Actors.push(new BotSpawner(
                    Vector2.FromPosition(spawner.Position),
                    zone,
                    loadout,
                    spawner.Behavior,
                    spawner.SpawnDelay,
                    spawner.Population));
            }
        }
        
        for (var i = 0; i < zoneData.NavPoints.length; i++)
        {
            if (zoneData.NavPoints[i].IsZoneLink)
            {
                zone.GameObjects.push(this.BuildNavBouy(zoneData.NavPoints[i]));
            }
        }

        for (var i = 0; i < zoneData.SpaceStations.length; i++)
        {
            zone.GameObjects.push(this.BuildSpaceStation(zoneData.SpaceStations[i]));
        }

        zone.CacheZoneLinks();

        return zone;
    }

    private BuildAsteroid0(celestial: ICelestial): GameObject
    {
        var color = this.GetColorForAsteroidSubType(celestial.Subtype);
        var position = Vector2.FromPosition(celestial.Position);

        var asteroid = new GameObject(position, 0);

        asteroid.AddComponent(new SpriteDisplay("asteroid_0", FlightState.LAYER_MIDGROUND, color, 1));

        asteroid.AddComponent(new Collider(
            [
                new CollisionCircle(new Vector2(-2,217), 138),
                new CollisionCircle(new Vector2(-2, -97), 254),
                new CollisionCircle(new Vector2(-10, 43), 252)
            ],
            false));

        asteroid.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_TERRAIN));
        asteroid.AddComponent(new StaticObjectIdentifier());

        return asteroid;
    }

    private BuildAsteroid1(celestial: ICelestial): GameObject
    {
        var color = this.GetColorForAsteroidSubType(celestial.Subtype);
        var position = Vector2.FromPosition(celestial.Position);

        var asteroid = new GameObject(position, 0);

        asteroid.AddComponent(new SpriteDisplay("asteroid_1", FlightState.LAYER_MIDGROUND, color, 1));

        asteroid.AddComponent(new Collider(
            [
                new CollisionCircle(new Vector2(-26.5, 125.5), 233),
                new CollisionCircle(new Vector2(2.5, 43.5), 277),
                new CollisionCircle(new Vector2(-21.5, -88.5), 264),
                new CollisionCircle(new Vector2(151.5, -60.5), 134)
            ],
            false));

        asteroid.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_TERRAIN));
        asteroid.AddComponent(new StaticObjectIdentifier());

        return asteroid;
    }
    
    private BuildNavBouy(navPoint: INavPoint): GameObject
    {
        var obj = new GameObject(Vector2.FromPosition(navPoint.Position), 0);

        obj.AddComponent(new SpriteDisplay("NavBouy",
            FlightState.LAYER_MIDGROUND,
            HexColor.WHITE,
            1));
        
        obj.AddComponent(new BlinkingSprite("NavBouy_blink",
            FlightState.LAYER_MIDGROUND,
            HexColor.WHITE,
            1,
            0.1,
            1.5));
        
        obj.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_NAVIGATION));

        obj.AddComponent(new ZoneLink(
            navPoint.Name,
            navPoint.TargetZone,
            Vector2.FromPosition(navPoint.TargetPosition)));

        obj.AddComponent(new StaticObjectIdentifier());

        return obj;
    }

    private BuildSpaceStation(spaceStation: ISpaceStation): GameObject
    {
        var obj = new GameObject(Vector2.FromPosition(spaceStation.Position), 0);

        switch (spaceStation.Type)
        {
            case "colony":
                obj.AddComponent(new SpriteDisplay("station_colony",
                    FlightState.LAYER_BACKGROUND,
                    HexColor.WHITE,
                    1));
                obj.AddComponent(new ConstantRotation(2));
                break;
            case "outpost":
                obj.AddComponent(new SpriteDisplay("station_outpost",
                    FlightState.LAYER_BACKGROUND,
                    HexColor.WHITE,
                    1));
                obj.AddComponent(new ConstantRotation(1));
                break;
            default:
                throw "Unknown space station type: " + spaceStation.Type;
        }

        obj.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_STATION));
        obj.AddComponent(new SpaceStationBehavior(spaceStation.Name));
        obj.AddComponent(new StaticObjectIdentifier());
        
        return obj;
    }
    
    private GetColorForAsteroidSubType(subtype: string): number
    {
        switch (subtype)
        {
            case "rock":
                return HexColor.WHITE;
            case "ice":
                return 0x99c7d7;
            default:
                throw "Unknown asteroid subtype: " + subtype;
        }
    }
    
    private BuildBackgroundObject(backgroundObject: IBackgroundObject): GameObject
    {
        var position = Vector2.FromPosition(backgroundObject.Position);

        var obj = new GameObject(position, 0);

        obj.AddComponent(new ParallaxSprite(
            backgroundObject.Type,
            FlightState.LAYER_DEEP_BACKGROUND,
            HexColor.WHITE,
            1,
            backgroundObject.ParallaxFactor));

        obj.AddComponent(new StaticObjectIdentifier());
        
        return obj;
    }

    private PlaceBackgroundDecorations(zone: Zone, zoneData: IZone): void
    {
        var container = new GameObject(Vector2.ZERO, 0);
        container.AddComponent(new StaticObjectIdentifier());

        var colors = [
            HexColor.WHITE,
            HexColor.CYAN,
            HexColor.RED,
            HexColor.ORANGE,
            HexColor.YELLOW
        ];

        for (var i = 0; i < 30; i++)
        {
            container.AddComponent(new CameraFixedSprite(
                "background_star",
                FlightState.LAYER_DEEP_BACKGROUND,
                Util.PickRandom(colors),
                1,
                new Vector2(Math.random(), Math.random())));
        }

        switch (zoneData.DustDensity)
        {
            case _2DLancer.Common.Data.DustDensity.None:
                break;
            case _2DLancer.Common.Data.DustDensity.Low:
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_BACKGROUND,
                    false,
                    1));
                break;
            case _2DLancer.Common.Data.DustDensity.Medium:
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_DEEP_BACKGROUND,
                    false,
                    0.05));
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_BACKGROUND,
                    false,
                    1));
                break;
            case _2DLancer.Common.Data.DustDensity.High:
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_DEEP_BACKGROUND,
                    true,
                    0.05));
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_BACKGROUND,
                    false,
                    1));
                break;
            case _2DLancer.Common.Data.DustDensity.VeryHigh:
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_DEEP_BACKGROUND,
                    true,
                    0.05));
                container.AddComponent(new DustCloudManager(
                    FlightState.LAYER_BACKGROUND,
                    true,
                    1));
                break;
            default:
                throw "Unknown dust density type: " + zoneData.DustDensity;
        }
        
        zone.GameObjects.push(container);
    }
}