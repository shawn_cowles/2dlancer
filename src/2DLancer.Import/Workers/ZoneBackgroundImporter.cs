﻿using System;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports background objects like planets and moons.
    /// </summary>
    public class ZoneBackgroundObjectImporter : AbstractWorker
    {
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Zone Background Object Importer"; }
        }
        
        /// <summary>
        /// Import celestials.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                foreach (var zoneName in importContext.Zones.Keys)
                {
                    var zone = importContext.Zones[zoneName];

                    foreach (var layer in importContext.ZoneMaps[zoneName].ObjectGroups)
                    {
                        foreach (var obj in layer.Objects.Where(o=>o.Type == ObjectTypes.BACKGROUND))
                        {
                            try
                            {
                                zone.BackgroundObjects.Add(new BackgroundObject
                                {
                                    Position = new Position(
                                        obj.X + obj.Width / 2,
                                        obj.Y + obj.Height / 2),

                                    Type = obj.Name,

                                    ParallaxFactor = GetDoubleProperty(obj, PropertyNames.BACKGROUND_PARALLAX_FACTOR)
                                });
                            }
                            catch (Exception ex)
                            {
                                throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}' in zone '{zone.Name}'", ex);
                            }
                        }
                    }
                }
                
                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }
    }
}

