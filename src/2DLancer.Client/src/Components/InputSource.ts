﻿/// <reference path="AbstractComponent.ts" />

// Base class for components that provide control input
class InputSource extends AbstractComponent
{
    public DesiredFacing: number;

    public TargetPoint: Vector2;
    public TargetObject: GameObject;

    public ActivatingModules: boolean[];

    public ThrustFoward: boolean;
    public ThrustPort: boolean;
    public ThrustStarboard: boolean;
    public ThrustAft: boolean;

    public HasTurretedWeapons: boolean;
    public ProjectileFlightSpeed: number;
    
    constructor()
    {
        super();

        this.DesiredFacing = 0;
        this.TargetPoint = Vector2.ZERO;
        
        this.ThrustFoward = false;
        this.ThrustPort = false;
        this.ThrustStarboard = false;
        this.ThrustAft = false;

        this.ActivatingModules = [false, false, false, false];

        this.HasTurretedWeapons = false;
        this.ProjectileFlightSpeed = null;
    }

    public DeactivateAllModules(): void
    {
        for (var i = 0; i < this.ActivatingModules.length; i++)
        {
            this.ActivatingModules[i] = false;
        }
    }
}