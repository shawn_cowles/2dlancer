﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages.Analytics
{
    /// <summary>
    /// An event for the player spending money.
    /// </summary>
    [TsClass(Module = "", Name = "IMoneySpentEvent")]
    [DataContract]
    public class MoneySpentEvent : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "MoneySpentEvent";

        /// <summary>
        /// The category of the item or service bought.
        /// </summary>
        [DataMember]
        public string Category { get; set; }

        /// <summary>
        /// The specific item or service bought.
        /// </summary>
        [DataMember]
        public string Item { get; set; }

        /// <summary>
        /// The amount of credits spent.
        /// </summary>
        [DataMember]
        public double Amount { get; set; }

        /// <summary>
        /// Construct a new MoneySpentEvent.
        /// </summary>
        public MoneySpentEvent() 
            : base(NAME)
        {
        }
    }
}
