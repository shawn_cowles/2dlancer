﻿/// <reference path="AbstractState.ts" />

// Handles loading of any assets
class LoadState extends AbstractState
{
    public static NAME = "state_load";

    private loadBar: Phaser.Sprite;
    private titleSplash: Phaser.Sprite;

    preload()
    {
        super.preload();

        // TODO title splash
        //this.titleSplash = this.add.sprite(this.game.width / 2, this.game.height / 2, "title_splash");
        //this.titleSplash.anchor.set(0.5, 0.5);

        this.loadBar = this.add.sprite(this.game.width / 2 - 100, this.game.height / 2 - 10, 'load_sprite');
        this.load.setPreloadSprite(this.loadBar);

        this.LoadShip("Talis");
        this.LoadShip("Pulsar");
        this.LoadShip("Cherubim");
        this.LoadShip("Canis");
        this.LoadShip("Junker");
        this.LoadShip("Lancer");
        this.LoadShip("Quasar");

        this.game.load.image("bullet", "images/bullet.png");
        this.game.load.image("small_blast", "images/small_blast.png");
        this.game.load.image("large_blast", "images/large_blast.png");
        this.game.load.image("huge_blast", "images/huge_blast.png");
        this.game.load.image("gun_shot_flash", "images/gun_shot_flash.png");
        this.game.load.image("laser_shot_flash", "images/laser_shot_flash.png");
        this.game.load.image("laser_charge", "images/laser_charge.png");
        this.game.load.image("thruster_plume", "images/thruster_plume.png");
        this.game.load.image("missile", "images/missile.png");
        this.game.load.image("mine", "images/mine.png");
        this.game.load.image("small_turret", "images/small_turret.png");
        this.game.load.image("emp_shockwave", "images/emp_shockwave.png");

        this.game.load.image("background_star", "images/background_star.png");
        this.game.load.image("dust_clouds", "images/dust_clouds.png");

        // Statics and Nav Bouys
        this.game.load.image("NavBouy", "images/statics/NavBouy.png");
        this.game.load.image("NavBouy_blink", "images/statics/NavBouy_blink.png");
        this.game.load.image("station_colony", "images/statics/station_colony.png");
        this.game.load.image("station_outpost", "images/statics/station_outpost.png");

        // Celestials
        this.game.load.image("asteroid_0", "images/celestials/asteroid_0.png");
        this.game.load.image("asteroid_1", "images/celestials/asteroid_1.png");
        this.game.load.image("cloud_1", "images/cloud_1.png");
        this.game.load.image("DS-01 1", "images/celestials/DS-01 1.png");
        this.game.load.image("DS-01 2", "images/celestials/DS-01 2.png");
        this.game.load.image("DS-04 1", "images/celestials/DS-04 1.png");
        this.game.load.image("DS-04 1 A", "images/celestials/DS-04 1 A.png");
        this.game.load.image("DS-04 1 B", "images/celestials/DS-04 1 B.png");
        this.game.load.image("DS-04 1 C", "images/celestials/DS-04 1 C.png");
        this.game.load.image("DS-04 2", "images/celestials/DS-04 2.png");

        // UI Sprites
        this.game.load.spritesheet("text_button", "images/ui/text_button.png", 200, 30, 2, null, 1);
        this.game.load.spritesheet("sliced_window", "images/ui/sliced_window.png",
            UiPanel.SLICE_SIZE, UiPanel.SLICE_SIZE);
        this.game.load.image("lead_indicator", "images/ui/lead_indicator.png");
        this.game.load.spritesheet("module_button", "images/ui/module_button.png", 50, 30, 3, null, 1);
        this.game.load.image("module_button_shade", "images/ui/module_button_shade.png");
        this.game.load.image("icon_engine_boost", "images/ui/icon_engine_boost.png");
        this.game.load.image("icon_missile_launcher", "images/ui/icon_missile_launcher.png");
        this.game.load.image("icon_mine_launcher", "images/ui/icon_mine_launcher.png");
        this.game.load.image("icon_shield_boost", "images/ui/icon_shield_boost.png");
        this.game.load.image("icon_emp", "images/ui/icon_emp.png");
        this.game.load.image("icon_autoturret", "images/ui/icon_autoturret.png");
        this.game.load.image("icon_autocannon", "images/ui/icon_autocannon.png");
        this.game.load.image("icon_lasercannon", "images/ui/icon_lasercannon.png");

        this.game.load.spritesheet("module_button_hotkey", "images/ui/module_button_hotkey.png",
            ModuleButton.WIDTH, ModuleButton.HEIGHT);

        // Audio Assets
        this.game.load.audio("ship_death", "audio/ship_death.mp3");
        this.game.load.audio("music", "audio/Ossuary 3 - Words.mp3");
        this.game.load.audio("jump_charge", "audio/jump_charge.mp3");
        this.game.load.audio("jump_leave", "audio/jump_leave.mp3");
        this.game.load.audio("jump_enter", "audio/jump_enter.mp3");
        this.game.load.audio("laser_charge", "audio/laser_charge.mp3");
        this.game.load.audio("little_burst", "audio/little_burst.mp3");
        this.game.load.audio("engine_base", "audio/engine_base.mp3");
        this.game.load.audio("shield_boost", "audio/shield_boost.mp3");
        this.game.load.audio("emp_blast", "audio/emp_blast.mp3");
        
        this.LoadSoundVariants("autocannon", 3);
        this.LoadSoundVariants("laser", 3);

        // Load Data
        this.game.load.json("zones", "data/zones.json");
        this.game.load.json("ships", "data/ships.json");
    }

    private LoadShip(shipName: string): void
    {
        this.game.load.image(shipName + "_base", "images/ships/" + shipName + "_base.png");
        this.game.load.image(shipName + "_paint", "images/ships/" + shipName + "_paint.png");
        this.game.load.image(shipName + "_debris_a", "images/ships/" + shipName + "_debris_a.png");
        this.game.load.image(shipName + "_debris_b", "images/ships/" + shipName + "_debris_b.png");
    }

    private LoadSoundVariants(baseName: string, variants: number): void
    {
        for (var i = 0; i < variants; i++)
        {
            var name = baseName + "_" + i
            this.game.load.audio(name, "audio/" + name + ".mp3");
        }
    }

    create()
    {
        // Fade progress bar and switch states
        var tween = this.add.tween(this.loadBar)
            .to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);

        tween.onComplete.add(() =>
        {
            // Load main menu
            this.game.state.start(FlightState.NAME, true, false);
        });
    }
}