﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// An engine mount on a ship.
    /// </summary>
    [TsClass(Module = "", Name = "IEngine")]
    public class Engine
    {
        /// <summary>
        /// The position offset of the engine on the ship.
        /// </summary>
        public Position Offset { get; set; }

        /// <summary>
        /// The width of the engine's trail.
        /// </summary>
        public double Width { get; set; }
    }
}
