﻿/// <reference path="AbstractComponent.ts" />

// ModuleSlots hold modules, which add special abilities to ships
class ModuleSlot extends AbstractComponent
{
    private inputSource: InputSource;

    private offsetMagnitudes: number[];
    private offsetDirections: number[];
    
    public Size: _2DLancer.Common.Data.ModuleSize;
    public Type: _2DLancer.Common.Data.ModuleType;
    public WeaponsTurreted: boolean;
    private module: AbstractModule;
    private moduleInitialized: boolean;

    constructor(
        positionOffsets: Vector2[],
        size: _2DLancer.Common.Data.ModuleSize,
        type: _2DLancer.Common.Data.ModuleType,
        weaponsTurreted: boolean)
    {
        super();

        this.Size = size;
        this.Type = type;
        this.WeaponsTurreted = weaponsTurreted;
        this.offsetDirections = [];
        this.offsetMagnitudes = [];
        this.moduleInitialized = false;
        this.module = null;

        for (var i = 0; i < positionOffsets.length; i++)
        {
            this.offsetDirections.push(positionOffsets[i].Direction());
            this.offsetMagnitudes.push(positionOffsets[i].Magnitude());
        }
    }

    // grab the local input source
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
    }
    
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (!this.moduleInitialized && this.IsModuleMounted())
        {
            this.moduleInitialized = true;

            this.module.Initialize(
                state,
                this.GameObject,
                this,
                this.offsetMagnitudes,
                this.offsetDirections);
        }

        if (this.IsModuleMounted())
        {
            var isActivating = this.inputSource.ActivatingModules[this.Type];

            this.module.Update(zone, state, this.GameObject, elapsedSeconds, isActivating);
        }
    }

    protected Uninitialize(): void
    {
        if (this.module != null)
        {
            this.module.UnInitialize(this.GameObject);
            this.moduleInitialized = false;
        }
    }
    
    public MountModule(module: AbstractModule): void
    {
        if (this.module != null)
        {
            this.module.UnInitialize(this.GameObject);
        }

        this.module = module;
        this.moduleInitialized = false;
    }

    public ModuleCooldownPercent(): number
    {
        if (this.module != null)
        {
            return this.module.CooldownPercent();
        }

        return 0;
    }

    public ModuleCanBeActivated(): boolean
    {
        if (this.module != null)
        {
            return this.module.CanBeActivated();
        }

        return false;
    }

    public IsModuleMounted(): boolean
    {
        return this.module != null;
    }
    
    public IconSprite(): string
    {
        if (this.module != null)
        {
            return this.module.IconSprite();
        }

        return null;
    }

    public ModuleName(): string
    {
        var name = "";

        switch (this.Size)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                name = "Small ";
                break;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                name = "Medium ";
                break;
            case _2DLancer.Common.Data.ModuleSize.Large:
                name = "Large ";
                break;
        }
        
        if (this.module != null)
        {
            name += this.module.Name();
        }
        else
        {
            switch (this.Type)
            {
                case _2DLancer.Common.Data.ModuleType.PrimaryWeapon:
                    name += "Weapon";
                    break;
                case _2DLancer.Common.Data.ModuleType.SecondaryWeapon:
                    name = "Secondary";
                    break;
                case _2DLancer.Common.Data.ModuleType.Bay:
                    name = "Bay";
                    break;
                case _2DLancer.Common.Data.ModuleType.Booster:
                    name = "Booster";
                    break;
            }
        }

        return name;
    }

    public SlotName(): string
    {
        var name = "";

        switch (this.Size)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                name = "Small ";
                break;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                name = "Medium ";
                break;
            case _2DLancer.Common.Data.ModuleSize.Large:
                name = "Large ";
                break;
        }
        
        switch (this.Type)
        {
            case _2DLancer.Common.Data.ModuleType.PrimaryWeapon:
                name += "Primary Weapon";
                break;
            case _2DLancer.Common.Data.ModuleType.SecondaryWeapon:
                name = "Secondary Weapon";
                break;
            case _2DLancer.Common.Data.ModuleType.Bay:
                name = "Bay";
                break;
            case _2DLancer.Common.Data.ModuleType.Booster:
                name = "Booster";
                break;
        }

        return name;
    }

    public GetMountedModule(): AbstractModule
    {
        return this.module;
    }
}