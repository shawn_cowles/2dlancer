﻿namespace _2DLancer.Import.Data
{
    /// <summary>
    /// Constant for object properties on import maps.
    /// </summary>
    public static class PropertyNames
    {
        /// <summary>
        /// The behavior type property on spawner objects.
        /// </summary>
        public const string SPAWNER_BEHAVIOR = "behavior";

        /// <summary>
        /// The ship class property on spawner objects.
        /// </summary>
        public const string SPAWNER_SHIP = "ship";

        /// <summary>
        /// The weapon class property on spawner objects.
        /// </summary>
        public const string SPAWNER_WEAPON = "weapon";

        /// <summary>
        /// The faction property on spawner objects.
        /// </summary>
        public const string SPAWNER_FACTION = "faction";
        
        /// <summary>
        /// The population property on spawner objects.
        /// </summary>
        public const string SPAWNER_POPULATION = "population";

        /// <summary>
        /// The spawn delay property on spawner objects.
        /// </summary>
        public const string SPAWNER_SPAWN_DELAY = "spawn_delay";

        /// <summary>
        /// The target zone of a nav bouy.
        /// </summary>
        public const string BOUY_TARGET_ZONE = "target_zone";

        /// <summary>
        /// The x coordinate of the target point of a nav bouy.
        /// </summary>
        public const string BOUY_TARGET_X = "target_x";

        /// <summary>
        /// The y coordinate of the target point of a nav bouy.
        /// </summary>
        public const string BOUY_TARGET_Y = "target_y";
        
        /// <summary>
        /// The subtype of a celestial object.
        /// </summary>
        public const string CELESTIAL_SUBTYPE = "subtype";

        /// <summary>
        /// The type of a space station.
        /// </summary>
        public const string SPACE_STATION_TYPE = "type";

        /// <summary>
        /// The dust density of a zone.
        /// </summary>
        public const string ZONE_DUST_DENSITY = "dust_density";

        /// <summary>
        /// The user friendly name of the zone.
        /// </summary>
        public const string ZONE_FRIENDLY_NAME = "friendly_name";

        /// <summary>
        /// The scale of the ship.
        /// </summary>
        public const string SHIP_SCALE = "scale";

        /// <summary>
        /// The width of an engine trail.
        /// </summary>
        public const string SHIP_ENGINE_TRAIL_WIDTH = "trail_width";

        /// <summary>
        /// The direction of the thruster.
        /// </summary>
        public const string SHIP_THRUSTER_DIRECTION = "direction";

        /// <summary>
        /// The max weapon class of the ship.
        /// </summary>
        public const string SHIP_WEAPON_CLASS = "weapon_class";

        /// <summary>
        /// The max engine class of the ship.
        /// </summary>
        public const string SHIP_ENGINE_CLASS = "engine_class";

        /// <summary>
        /// The max shield class of the ship.
        /// </summary>
        public const string SHIP_SHIELD_CLASS = "shield_class";

        /// <summary>
        /// The health of the ship.
        /// </summary>
        public const string SHIP_HEALTH = "health";

        /// <summary>
        /// The mass of the ship.
        /// </summary>
        public const string SHIP_MASS = "mass";

        /// <summary>
        /// The drag coefficient of the ship.
        /// </summary>
        public const string SHIP_DRAG = "drag";

        /// <summary>
        /// The drag coefficient of the ship.
        /// </summary>
        public const string DEBRIS_SPRITE = "sprite";

        /// <summary>
        /// The size of a module slot on a ship.
        /// </summary>
        public const string MODULE_SIZE = "size";

        /// <summary>
        /// The type of the module slot.
        /// </summary>
        public const string MODULE_TYPE = "type";

        /// <summary>
        /// Does the ship have turreted weapons?
        /// </summary>
        public const string SHIP_HAS_TURRETS = "has_turrets";

        /// <summary>
        /// The parallax scroll factor of the object.
        /// </summary>
        public const string BACKGROUND_PARALLAX_FACTOR = "parallax_factor";
    }
}
