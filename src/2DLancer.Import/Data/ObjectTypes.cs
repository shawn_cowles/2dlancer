﻿namespace _2DLancer.Import.Data
{
    /// <summary>
    /// Constants for object types in import maps.
    /// </summary>
    public static class ObjectTypes
    {
        /// <summary>
        /// A static object, like an asteroid.
        /// </summary>
        public const string STATIC = "static";

        /// <summary>
        /// A player spawner.
        /// </summary>
        public const string PLAYER_SPAWNER = "player_spawner";

        /// <summary>
        /// A bot spawner.
        /// </summary>
        public const string BOT_SPAWNER = "bot_spawner";

        /// <summary>
        /// A nav bouy that links to another zone.
        /// </summary>
        public const string NAV_BOUY = "nav_bouy";
        
        /// <summary>
        /// A waypoint used for pathfinding.
        /// </summary>
        public const string WAYPOINT = "waypoint";

        /// <summary>
        /// A space station.
        /// </summary>
        public const string SPACE_STATION = "space_station";

        /// <summary>
        /// Background objects like planets and moons.
        /// </summary>
        public const string BACKGROUND = "background";

        /// <summary>
        /// Engine objects on ships.
        /// </summary>
        public const string ENGINE = "engine";

        /// <summary>
        /// Thruster objects on ships.
        /// </summary>
        public const string THRUSTER = "thruster";

        /// <summary>
        /// Weapon barrel objects on ships.
        /// </summary>
        public const string WEAPON_BARREL = "weapon_barrel";

        /// <summary>
        /// Colliders on ships.
        /// </summary>
        public const string COLLIDER = "collider";

        /// <summary>
        /// Shield colliders on ships.
        /// </summary>
        public const string SHIELD = "shield";

        /// <summary>
        /// A piece of debris that the ship explodes into.
        /// </summary>
        public const string DEBRIS = "debris";

        /// <summary>
        /// A slot for modules on a ship.
        /// </summary>
        public const string MODULE_SLOT = "module_slot";
    }
}
