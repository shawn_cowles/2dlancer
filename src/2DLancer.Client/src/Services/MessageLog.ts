﻿// Receives, stores, and purges a log of messages for the player
class MessageLog
{
    private MAX_MESSAGES = 10;
    private MESSAGE_LIFETIME = 5;

    private cullTimer: number;

    private messages: string[];

    public Initialize(state: FlightState): void
    {
        this.cullTimer = 0;
        this.messages = [];
    }

    public Update(elapsedSeconds: number): void
    {
        this.cullTimer -= elapsedSeconds;
        
        if (this.messages.length > 0 && this.cullTimer < 0)
        {
            this.PopMessage();
        }
    }

    public LogMessage(message: string): void
    {
        if (this.messages.length == this.MAX_MESSAGES)
        {
            this.PopMessage();
        }

        if (this.messages.length == 0)
        {
            this.cullTimer = this.MESSAGE_LIFETIME;
        }

        this.messages.push(message);
    }

    public GetMessages(): string[]
    {
        return this.messages;
    }

    private PopMessage(): void
    {
        this.messages.splice(0, 1);
        this.cullTimer = this.MESSAGE_LIFETIME;
    }
}
