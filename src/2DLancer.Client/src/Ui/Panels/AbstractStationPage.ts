﻿// Base class for all station menu pages.
abstract class AbstractStationPage
{
    public Station: SpaceStationBehavior;

    private title: Phaser.Text;
    private buttons: TextButton[];
    private bodyContent: Phaser.Text[];

    protected BodyTextStyle: any;
    
    constructor()
    {
        this.buttons = [];
        this.bodyContent = [];

        this.BodyTextStyle = {
            font: "16px courier",
            fill: "#00FFFF",
            align: "left",
        };
    }
    
    public Initialize(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior): void
    {
        var titleStyle = {
            font: "24px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.title = state.game.add.text(0, 0, "Abstract Station Page", titleStyle);
        this.title.anchor.set(0.5, 0.5);
        
        this.BuildContent(
            panel,
            state,
            station,
            this.title,
            this.buttons,
            this.bodyContent);

        for (var i = 0; i < this.buttons.length - 1; i++)
        {
            this.buttons[i].anchor.set(0.5, 0.5);
        }

        for (var i = 0; i < this.bodyContent.length; i++)
        {
            this.bodyContent[i].anchor.set(0.5,0);
        }
    }

    public Update(state: FlightState, upperLeft: Phaser.Point)
    {
        this.title.position.set(
            upperLeft.x + SpaceStationInteractionPanel.WIDTH / 2,
            upperLeft.y + 20);

        var buttonSpacing = 40;
        var buttonX = upperLeft.x + 110;
        var buttonY = upperLeft.y + 60;
        for (var i = 0; i < this.buttons.length - 1; i++)
        {
            this.buttons[i].position.set(buttonX, buttonY);

            buttonY += buttonSpacing;
        }
        
        // Special handling for the last button
        this.buttons[this.buttons.length - 1].position.set(
            buttonX,
            upperLeft.y + SpaceStationInteractionPanel.HEIGHT - 25);


        var bodyX = buttonX - 20 + SpaceStationInteractionPanel.WIDTH / 2;
        var bodyY = upperLeft.y + 50; 
        for (var i = 0; i < this.bodyContent.length; i++)
        {
            this.bodyContent[i].position.set(bodyX, bodyY);

            bodyY += this.bodyContent[i].height + 15;
        }

        this.OnUpdate(state);
    }

    public Destroy(): void
    {
        this.title.destroy();

        for (var i = 0; i < this.buttons.length; i++)
        {
            this.buttons[i].destroy();
        }

        for (var i = 0; i < this.bodyContent.length; i++)
        {
            this.bodyContent[i].destroy();
        }
    }

    protected abstract BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void;

    protected abstract OnUpdate(state: FlightState): void;
}