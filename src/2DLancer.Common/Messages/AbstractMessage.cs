﻿using TypeLite;

namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// Any message that can be sent.
    /// </summary>
    [TsClass(Module = "", Name = "IAbstractMessage")]
    public abstract class AbstractMessage
    {
    }
}
