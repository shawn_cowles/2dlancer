﻿/// <reference path="AbstractComponent.ts" />

// ProjectileDamage lets a GameObject deal damage to another when they collide, then destroys the projectile
class ProjectileDamage extends AbstractComponent
{
    private damage: number;
    private shootingShip: GameObject;
    private largeBlast: boolean;

    constructor(damage: number, shootingShip: GameObject, largeBlast: boolean)
    {
        super();

        this.damage = damage;
        this.shootingShip = shootingShip;
        this.largeBlast = largeBlast;
    }

    // When colliding with another GameObject, deal damage and remove self
    public OnCollided(zone: Zone, state: FlightState, other: GameObject): void
    {
        super.OnCollided(zone, state, other);

        this.GameObject.ShouldBeRemoved = true;

        Mechanics.ResolveAttack(zone, this.GameObject.Position, other, this.damage, this.largeBlast);
        
        Mechanics.CheckForPlayerTag(state, this.shootingShip, other);
    }
}