﻿using System.Collections.Generic;
using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A navigation point used for pathfinding and linking zones together.
    /// </summary>
    [TsClass(Module = "", Name = "INavPoint")]
    public class NavPoint
    {
        /// <summary>
        /// The unique id of the NavPoint.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the NavPoint.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Is this a zone link?
        /// </summary>
        public bool IsZoneLink { get; set; }

        /// <summary>
        /// The position of the NavPoint.
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// The name of the zone the NavPoint leads to if this is a zone link.
        /// </summary>
        public string TargetZone { get; set; }

        /// <summary>
        /// The target position within the target zone if this is a zone link.
        /// </summary>
        public Position TargetPosition { get; set; }

        /// <summary>
        /// The ids of NavPoints that this NavPoint connects to within the same zone.
        /// </summary>
        public List<int> DirectLinks { get;set;}

        /// <summary>
        /// The ids of NavPoints that this NavPoint connects to within other zones.
        /// </summary>
        public List<int> JumpLinks { get; set; }

        /// <summary>
        /// Construct a new NavPoin.
        /// </summary>
        public NavPoint()
        {
            DirectLinks = new List<int>();
            JumpLinks = new List<int>();
        }
    }
}
