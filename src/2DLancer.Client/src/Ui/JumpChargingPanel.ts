﻿/// <reference path="UiPanel.ts" />

// A widget that displays a notification when the player's ship is charging for a jump.
class JumpChargingPanel extends UiPanel
{
    private static WIDTH = 300;
    private static HEIGHT = 100;

    private state: FlightState;

    private text: Phaser.Text;
    private cancelButton: TextButton;

    private proximityLink: ZoneLink;

    constructor()
    {
        super(new Phaser.Point(0, 0), JumpChargingPanel.WIDTH, JumpChargingPanel.HEIGHT);
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.text = state.game.add.text(0, 0, "", textStyle);
        this.text.anchor.set(0.5, 0.5);
        this.cancelButton = new TextButton(state.game, 0, 0, "Cancel",
            () =>
            {
                var sequencer = state.PlayerShip.GetComponent<JumpSequencer>(JumpSequencer);

                state.PlayerShip.RemoveComponent(null, sequencer);
            },
            this);
        this.cancelButton.anchor.set(0.5, 0.5);

        this.SetVisible(false);
        this.text.visible = false;
        this.cancelButton.visible = false;
    }

    // Update position and show/hide
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        if (state.PlayerShip == null)
        {
            return;
        }
        
        var jumpSequencer = state.PlayerShip.GetComponent<JumpSequencer>(JumpSequencer);

        if (jumpSequencer != null)
        {
            this.SetVisible(true);
            this.text.visible = true;
            this.cancelButton.visible = true;

            var xPosition = state.game.camera.width / 2 + state.game.camera.view.x;
            var yPosition = state.game.camera.height * 0.2 + state.game.camera.view.y;
            this.CenterPosition = new Phaser.Point(xPosition, yPosition);

            this.text.position.set(
                this.CenterPosition.x,
                this.CenterPosition.y - 40);

            this.cancelButton.position.set(
                this.CenterPosition.x,
                this.CenterPosition.y + 20);

            this.text.text = "Charging   " + jumpSequencer.CountdownTimer.toFixed(1);
        }
        else
        {
            this.SetVisible(false);
            this.text.visible = false;
            this.cancelButton.visible = false;
        }
    }

    public Destroy()
    {
        super.Destroy();

        this.text.destroy();
        this.cancelButton.destroy();
    }
}