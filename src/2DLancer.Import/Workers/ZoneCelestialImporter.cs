﻿using System;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports celestials for zones, things like asteroids, space stations, and the like.
    /// </summary>
    public class ZoneCelestialImporter : AbstractWorker
    {
        // Known good property values
        private string[] KNOWN_ASTEROID_TYPES = new[] { "asteroid_0", "asteroid_1" };

        private string[] KNOWN_ASTEROID_SUBTYPES = new[] { "rock", "ice" };

        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Zone Celestial Importer"; }
        }

        /// <summary>
        /// Import celestials.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                foreach (var zoneName in importContext.Zones.Keys)
                {
                    var zone = importContext.Zones[zoneName];

                    foreach (var layer in importContext.ZoneMaps[zoneName].ObjectGroups)
                    {
                        foreach (var obj in layer.Objects.Where(o => o.Type == ObjectTypes.STATIC))
                        {
                            try
                            {
                                if (!KNOWN_ASTEROID_TYPES.Contains(obj.Name))
                                {
                                    throw new ImportException($"Unknown asteroid type: '{obj.Name}'");
                                }

                                var subtype = GetProperty(obj, PropertyNames.CELESTIAL_SUBTYPE);

                                if (!KNOWN_ASTEROID_SUBTYPES.Contains(subtype))
                                {
                                    throw new ImportException($"Unknown asteroid subtype: '{subtype}'");
                                }

                                zone.Celestials.Add(new Celestial
                                {
                                    Position = new Position(
                                        obj.X + obj.Width / 2,
                                        obj.Y + obj.Height / 2),

                                    Type = obj.Name,

                                    Subtype = subtype
                                });
                            }
                            catch (Exception ex)
                            {
                                throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}' in zone '{zone.Name}'", ex);
                            }
                        }
                    }
                }

                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }
    }
}

