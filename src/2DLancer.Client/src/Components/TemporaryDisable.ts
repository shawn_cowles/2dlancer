﻿/// <reference path="AbstractComponent.ts" />

// A temporary disable debuff, such as from an EMP.
class TemporaryDisable extends AbstractComponent
{
    private duration: number;

    private inputSource: InputSource;

    constructor(duration: number)
    {
        super();

        this.duration = duration;
    }

    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
    }
    
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);
        
        this.inputSource.DesiredFacing = this.GameObject.Facing;
        this.inputSource.DeactivateAllModules();
        this.inputSource.ThrustAft = false;
        this.inputSource.ThrustFoward = false;
        this.inputSource.ThrustPort = false;
        this.inputSource.ThrustStarboard = false;

        this.duration -= elapsedSeconds;

        if (this.duration <= 0)
        {
            this.GameObject.RemoveComponent(zone, this);
        }
    }
}