﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Information on debris that a ship leaves behind on death.
    /// </summary>
    [TsClass(Module = "", Name = "IDebrisInfo")]
    public class DebrisInfo
    {
        /// <summary>
        /// The direction the debris launches into.
        /// </summary>
        public double LaunchDirection { get; set; }

        /// <summary>
        /// The name of the sprite for the debris.
        /// </summary>
        public string SpriteName { get; set; } 
    }
}
