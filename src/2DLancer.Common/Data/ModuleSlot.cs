﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A slot for modules on a ship.
    /// </summary>
    [TsClass(Module = "", Name = "IModuleSlot")]
    public class ModuleSlot
    {
        /// <summary>
        /// The position offsets of the slot on the ship.
        /// </summary>
        public Position[] Offsets { get; set; }

        /// <summary>
        /// The size of the module slot.
        /// </summary>
        public ModuleSize Size { get; set; }

        /// <summary>
        /// The type of this module slot.
        /// </summary>
        public ModuleType Type { get; set; }
    }
}
