﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Enumeration of possible module types
    /// </summary>
    [TsEnum(Module = "", Name = "ModuleType")]
    public enum ModuleType
    {
        /// <summary>
        /// Primary weapon module (i.e. the ship's main guns).
        /// </summary>
        PrimaryWeapon,

        /// <summary>
        /// Secondary weapon module (missile launcher or similar).
        /// </summary>
        SecondaryWeapon,

        /// <summary>
        /// Bay module, like a drone or mine launcher.
        /// </summary>
        Bay,

        /// <summary>
        /// Booster module, like an engine or shield booster.
        /// </summary>
        Booster
    }
}
