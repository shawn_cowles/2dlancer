﻿// Used for pathing across and in between zones
class Waypoint
{
    public Id: number;

    public ZoneName: string;

    public Position: Vector2;

    public DirectLinks: Waypoint[];

    public JumpLinks: Waypoint[];

    constructor(id: number, zoneName: string, position: Vector2)
    {
        this.Id = id;
        this.ZoneName = zoneName;
        this.Position = position;

        this.DirectLinks = [];
        this.JumpLinks = [];
    }
}