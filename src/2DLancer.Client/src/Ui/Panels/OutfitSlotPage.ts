﻿/// <reference path="AbstractStationPage.ts" />

// The station menu page for outfitting a module in a specific slot..
class OutfitSlotPage extends AbstractStationPage
{
    private moduleSlot: ModuleSlot;

    constructor(moduleSlot: ModuleSlot)
    {
        super();

        this.moduleSlot = moduleSlot;
    }

    public BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void
    {
        title.text = "Outfitting: " + this.moduleSlot.SlotName();
        
        if (this.moduleSlot.IsModuleMounted())
        {
            var sellPrice = this.moduleSlot
                .GetMountedModule()
                .GetListing()
                .CostForSize(this.moduleSlot.Size) / 2;

            buttons.push(new TextButton(state.game, 0, 0, "Sell - " + Util.FormatAsMoney(sellPrice),
                () =>
                {
                    this.moduleSlot.MountModule(null);
                    state.PlayerDataService.SetBalance(state.PlayerDataService.GetBalance() + sellPrice);
                    panel.SetPage(new OutfitSlotPage(this.moduleSlot));
                },
                this));
        }

        var moduleTypesAvailable = AbstractModuleListing.GetAllListings();

        for (var i = 0; i < moduleTypesAvailable.length; i++)
        {
            // restrict by mounting type
            if (this.moduleSlot.Type == moduleTypesAvailable[i].Type)
            {
                // becuase javascript scoping
                buttons.push(this.BuildModuleBuyButton(moduleTypesAvailable[i], state, panel));
            }
        }
        
        buttons.push(new TextButton(state.game, 0, 0, "Back",
            () =>
            {
                panel.SetPage(new OutfittingStationPage());
            },
            this));

        var descriptionText = "";

        if (this.moduleSlot.IsModuleMounted())
        {
            descriptionText = "The module slot currently holds a " + this.moduleSlot.ModuleName();
        }
        else
        {
            descriptionText = "The module slot is empty.";
        }

        var descriptionElement = new Phaser.Text(state.game, 0, 0,
            Util.WrapText(descriptionText, 55),
            this.BodyTextStyle);
        state.game.add.existing(descriptionElement);
        mainBodyContent.push(descriptionElement);
    }

    protected OnUpdate(state: FlightState): void
    {
    }

    private BuildModuleBuyButton(
        moduleListing: AbstractModuleListing,
        state: FlightState,
        panel: SpaceStationInteractionPanel): TextButton
    {
        var buttonName = moduleListing.Name;

        if (this.moduleSlot.IsModuleMounted() && this.moduleSlot.GetMountedModule().Name() == moduleListing.Name)
        {
            buttonName = "* " + buttonName + " *";
        }

        return new TextButton(state.game, 0, 0, buttonName,
            () =>
            {
                panel.SetPage(new ModuleBuyPage(moduleListing, this.moduleSlot));
            },
            this); 
    }
}