module _2DLancer.Common.Data {
	export const enum DustDensity {
		None = 0,
		Low = 1,
		Medium = 2,
		High = 3,
		VeryHigh = 4
	}
	export const enum ModuleSize {
		None = 0,
		Small = 1,
		Medium = 2,
		Large = 3
	}
	export const enum ModuleType {
		PrimaryWeapon = 0,
		SecondaryWeapon = 1,
		Bay = 2,
		Booster = 3
	}
}

