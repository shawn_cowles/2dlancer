﻿/// <reference path="AbstractComponent.ts" />

// StaticObjectIdentifier identifies a GameObject as one that should persist after a Zone moves to the background
class StaticObjectIdentifier extends AbstractComponent
{
}