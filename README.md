2D Lancer
============================================

Shawn Cowles, 2016

https://bitbucket.org/shawn_cowles/2dlancer

--------------------------------------------

A 2d homage to freelancer focusing on accessible and fun spaceflight and combat within a living, breathing, world.

--------------------------------------------

Code and assets are licensed under the MIT License. See LICENSE.txt for details.
Assets from third party sources are listed under "CREDITS.txt".
