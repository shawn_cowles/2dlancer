﻿
 
 

 

/// <reference path="Enums.ts" />

	interface IAbstractClientMessage extends IAbstractMessage {
		MessageName: string;
	}
	interface IAbstractIncomingClientMessage extends IAbstractClientMessage {
	}
	interface IAbstractMessage {
	}
	interface IAbstractOutgoingClientMessage extends IAbstractClientMessage {
	}
	interface IBackgroundObject {
		ParallaxFactor: number;
		Position: IPosition;
		Type: string;
	}
	interface ICelestial {
		Position: IPosition;
		Subtype: string;
		Type: string;
	}
	interface ICollider {
		Offset: IPosition;
		Radius: number;
	}
	interface IDebrisInfo {
		LaunchDirection: number;
		SpriteName: string;
	}
	interface IDockedAtStationEvent extends IAbstractIncomingClientMessage {
		StationName: string;
	}
	interface IEngine {
		Offset: IPosition;
		Width: number;
	}
	interface IModuleSlot {
		Offsets: IPosition[];
		Size: _2DLancer.Common.Data.ModuleSize;
		Type: _2DLancer.Common.Data.ModuleType;
	}
	interface IMoneyEarnedEvent extends IAbstractIncomingClientMessage {
		Amount: number;
		Category: string;
		Item: string;
	}
	interface IMoneySpentEvent extends IAbstractIncomingClientMessage {
		Amount: number;
		Category: string;
		Item: string;
	}
	interface INavPoint {
		DirectLinks: number[];
		Id: number;
		IsZoneLink: boolean;
		JumpLinks: number[];
		Name: string;
		Position: IPosition;
		TargetPosition: IPosition;
		TargetZone: string;
	}
	interface IPlayerSpawnedEvent extends IAbstractIncomingClientMessage {
	}
	interface IPosition {
		X: number;
		Y: number;
	}
	interface IShipData {
		BaseSprite: string;
		Colliders: ICollider[];
		Cost: number;
		Debris: IDebrisInfo[];
		Description: string;
		Drag: number;
		Engines: IEngine[];
		Health: number;
		Mass: number;
		MaxEngineClass: number;
		MaxShieldClass: number;
		ModuleSlots: IModuleSlot[];
		Name: string;
		PaintSprite: string;
		Scale: number;
		ShieldRadius: number;
		SpriteHeight: number;
		SpriteWidth: number;
		Thrusters: IThruster[];
		TurretedWeapons: boolean;
	}
	interface IShipRepairedEvent extends IAbstractIncomingClientMessage {
		StationName: string;
	}
	interface ISpaceStation {
		Name: string;
		Position: IPosition;
		Type: string;
	}
	interface ISpawner {
		Behavior: string;
		Color: number;
		Facing: number;
		Faction: string;
		IsPlayerSpawner: boolean;
		Population: number;
		Position: IPosition;
		ShipClass: string;
		SpawnDelay: number;
		WeaponClass: string;
	}
	interface IThruster {
		Direction: number;
		Offset: IPosition;
	}
	interface IUserConnectedMessage extends IAbstractIncomingClientMessage {
		UserId: string;
	}
	interface IZone {
		BackgroundObjects: IBackgroundObject[];
		Celestials: ICelestial[];
		DustDensity: _2DLancer.Common.Data.DustDensity;
		FriendlyName: string;
		Height: number;
		Name: string;
		NavPoints: INavPoint[];
		SpaceStations: ISpaceStation[];
		Spawners: ISpawner[];
		Width: number;
	}
	interface IZoneVisitedEvent extends IAbstractIncomingClientMessage {
		ZoneName: string;
	}


