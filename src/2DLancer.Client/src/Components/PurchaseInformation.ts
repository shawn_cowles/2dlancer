﻿/// <reference path="AbstractComponent.ts" />

// PurchaseInformation holds purchase information for a ship, useful for identifying the class of a purchased ship.
class PurchaseInformation extends AbstractComponent
{
    public ShipData: IShipData;

    constructor(shipData: IShipData)
    {
        super();

        this.ShipData = shipData;
    }
}