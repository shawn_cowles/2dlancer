﻿/// <reference path="AbstractModule.ts" />

// The Autoturret fires automatically on nearby ships.
class Autoturret extends AbstractModule
{
    public static COOLDOWN = 10;
    public static DURATION = 5;
    private static RANGE = 800;
    private static SHOT_DAMAGE = 3;

    private offsetMagnitude: number;
    private offsetDirection: number;
    
    private cooldownTimer: number;
    private shootingTimer: number;
    private nextShotTimer: number;

    private interShotCooldown: number;
    
    private target: GameObject;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.offsetDirection = offsetDirections[0];
        this.offsetMagnitude = offsetMagnitudes[0];

        var targetDps = Autoturret.DpsForModuleSize(slot.Size);
        this.interShotCooldown = Autoturret.SHOT_DAMAGE / targetDps;

        this.cooldownTimer = 0;
        this.shootingTimer = 0;
        this.nextShotTimer = 0;
    }
    
    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = Autoturret.COOLDOWN;
            this.shootingTimer = Autoturret.DURATION;
            this.nextShotTimer = 0;
        }

        if (this.shootingTimer > 0)
        {
            this.shootingTimer -= elapsedSeconds;
            this.nextShotTimer -= elapsedSeconds;

            if (this.target == null || this.target.ShouldBeRemoved)
            {
                this.target = null;
                this.PickTarget(parentShip, zone.GameObjects);
            }

            if (this.nextShotTimer < 0 && this.target != null)
            {
                this.nextShotTimer = this.interShotCooldown;

                this.ShootLaser(zone, state, parentShip);
            }
        }
    }
    
    public UnInitialize(parentShip: GameObject): void
    {
    }
    
    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / Autoturret.COOLDOWN, 1, 0);
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public IconSprite(): string
    {
        return "icon_autoturret";
    }

    public Name(): string
    {
        return "Autoturret";
    }

    public GetListing(): AbstractModuleListing
    {
        return new AutoturretListing();
    }

    private ShootLaser(zone: Zone, state: FlightState, parentShip: GameObject): void
    {
        Mechanics.ResolveAttack(
            zone,
            this.target.Position,
            this.target,
            Autoturret.SHOT_DAMAGE,
            false);

        Mechanics.CheckForPlayerTag(state, parentShip, this.target);

        var barrelPos = Vector2.FromPolarOffset(
            this.offsetMagnitude,
            this.offsetDirection,
            parentShip);

        var dirToTarget = barrelPos.DirectionTo(this.target.Position);

        var laserBlast = new GameObject(barrelPos, 0);
        laserBlast.AddComponent(new OffsetFollow(parentShip));
        laserBlast.AddComponent(new FadingLine(
            Vector2.ZERO,
            this.target.Position,
            this.target,
            HexColor.PURPLE,
            1,
            0.25,
            true));
        laserBlast.AddComponent(new TimedLife(1, false));
        zone.GameObjects.push(laserBlast);

        var flash = new GameObject(barrelPos, dirToTarget);
        flash.AddComponent(new OffsetFollow(parentShip));
        flash.AddComponent(new TimedLife(1, false));
        flash.AddComponent(new GrowingAndFadingSprite(
            "laser_shot_flash", FlightState.LAYER_MIDGROUND, HexColor.WHITE,
            0.25,
            0.5, 0.5, // scale
            1, 0)); // alpha
        flash.AddComponent(new MutatedSoundEffect("laser", 3, 0.1));
        zone.GameObjects.push(flash);
    }

    private PickTarget(self: GameObject, potentials: GameObject[]): void
    {
        var nearDist = <number>null;

        for (var i = 0; i < potentials.length; i++)
        {
            // only consider objects that can be damaged
            if (potentials[i].HasComponent(HullHealth))
            {
                // only target hostiles
                if (Mechanics.IsHostile(self, potentials[i]))
                {
                    var dist = self.Position.DistanceTo(potentials[i].Position);

                    if (dist < Autoturret.RANGE && (nearDist == null || dist < nearDist))
                    {
                        nearDist = dist;
                        this.target = potentials[i];
                    }
                }
            }
        }
    }

    public static DpsForModuleSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 2;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 4;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 8;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }
}

class AutoturretListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Autoturret";
        this.BaseCost = 200;
        this.Type = _2DLancer.Common.Data.ModuleType.Bay;
    }

    public BuildModule(): AbstractModule
    {
        return new Autoturret();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        var effectiveDps = Autoturret.DpsForModuleSize(moduleSize) * Autoturret.DURATION / Autoturret.COOLDOWN;

        return [
            "Duration: " + Autoturret.DURATION + "s"
            + "  Cooldown: " + Autoturret.COOLDOWN + "s"
            + "  Effective DPS: " + effectiveDps.toFixed(1),

            "The Autoturret is a small, automatically firing turret that shoots nearby enemies when activated. It can add a turret to any ship, regardless of primary weapon mounts."
        ];
    }
}