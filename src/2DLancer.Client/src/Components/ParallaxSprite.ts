﻿/// <reference path="AbstractComponent.ts" />

// A component that displays a sprite with a parallax effect.
class ParallaxSprite extends AbstractComponent
{
    protected spriteName: string;
    protected spriteLayer: number;
    protected tint: number;
    protected sprite: Phaser.Sprite;
    protected scale: number;
    protected moveScale: number;

    constructor(spriteName: string, spriteLayer: number, tint: number, scale: number, moveScale: number)
    {
        super();
        this.spriteName = spriteName;
        this.spriteLayer = spriteLayer;
        this.tint = tint;
        this.scale = scale;
        this.moveScale = moveScale;
    }

    // initialize the sprite display by creating the sprite in phaser
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.sprite = state.SpriteGroups[this.spriteLayer].create(0, 0, this.spriteName);
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = this.tint;
        this.sprite.scale = new Phaser.Point(this.scale, this.scale);
    }

    // Perform a simulation update
    public LateUpdate(zone: Zone, state: FlightState): void
    {
        super.LateUpdate(zone, state);

        if (this.sprite == null)
        {
            return;
        }

        var referencePos = <Vector2>null;
        
        // use the player ship if possible, otherwise the camera
        if (state.PlayerShip != null)
        {
            referencePos = state.PlayerShip.Position;
        }
        else
        {
            referencePos = new Vector2(
                state.camera.position.x + state.camera.width / 2,
                state.camera.position.y + state.camera.height / 2);
        }

        referencePos = new Vector2(
            state.camera.position.x + state.camera.width / 2,
            state.camera.position.y + state.camera.height / 2);

        var adjustedPosition = this.GameObject.Position
            .Subtract(referencePos)
            .Scale(this.moveScale)
            .Add(referencePos);

        // have the sprite follow the game object
        this.sprite.position.set(adjustedPosition.X, adjustedPosition.Y);
        this.sprite.rotation = this.GameObject.Facing;
    }

    // Clean up the sprite
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.sprite != null)
        {
            this.sprite.destroy();
            this.sprite = null;
        }
    }
}