﻿using _2DLancer.Server.Services;

namespace _2DLancer.Server.Interfaces
{
    /// <summary>
    /// A service that uses the message bus for inter-service communication.
    /// </summary>
    public interface IBusService
    {
        /// <summary>
        /// Start processing messages.
        /// </summary>
        /// /// <param name="bus">The message bus to use for sending and getting messages.</param>
        void Start(MessageBus bus);

        /// <summary>
        /// Stop processing messages.
        /// </summary>
        void Stop();
    }
}
