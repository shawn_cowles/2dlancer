﻿/// <reference path="AbstractComponent.ts" />

class MinimapDisplay extends AbstractComponent
{
    public static TYPE_PROJECTILE = 0;
    public static TYPE_SHIP = 1;
    public static TYPE_TERRAIN = 2;
    public static TYPE_NAVIGATION = 3;
    public static TYPE_STATION = 4;

    public Type: number;
    
    constructor(type: number)
    {
        super();

        this.Type = type;
    }
}