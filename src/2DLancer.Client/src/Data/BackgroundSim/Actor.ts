﻿// An abstract representation of an actor in the background simulation (such as a ship).
abstract class Actor
{
    public Position: Vector2;
    public Zone: Zone;
    public IsDead: boolean;

    constructor(position: Vector2, zone: Zone)
    {
        this.Position = position;
        this.Zone = zone;
        this.IsDead = false;
    }

    public abstract Update(state: FlightState, bgs: BackgroundSimulation, elapsedSeconds: number): void;

    public abstract BuildGameObject(state: FlightState): GameObject;
}