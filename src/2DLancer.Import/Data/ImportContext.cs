﻿using System.Collections.Generic;
using System.IO;
using _2DLancer.Common.Data;
using TiledSharp;

namespace _2DLancer.Import.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class ImportContext
    {
        /// <summary>
        /// The source path for the import.
        /// </summary>
        public string ImportPath { get; private set; }

        /// <summary>
        /// The source path for maps to import.
        /// </summary>
        public string MapPath
        {
            get { return Path.Combine(ImportPath, "Maps"); }
        }

        /// <summary>
        /// The source path for ships to import.
        /// </summary>
        public string ShipPath
        {
            get { return Path.Combine(ImportPath, "Ships"); }
        }

        /// <summary>
        /// The destination path for the import.
        /// </summary>
        public string ExportPath { get; private set; }

        /// <summary>
        /// The Zones being imported.
        /// </summary>
        public Dictionary<string, Zone> Zones { get; private set; }
        
        /// <summary>
        /// Imported TMX maps for the Zones.
        /// </summary>
        public Dictionary<string, TmxMap> ZoneMaps { get; private set; }

        /// <summary>
        /// Imported ship data.
        /// </summary>
        public List<ShipData> Ships { get; private set; }

        /// <summary>
        /// Construct a new ImportContext
        /// </summary>
        /// <param name="importPath">The source path for the import.</param>
        /// <param name="exportPath">The destination path for the import.</param>
        public ImportContext(string importPath, string exportPath)
        {
            ImportPath = importPath;
            ExportPath = exportPath;
            Zones = new Dictionary<string, Zone>();
            ZoneMaps = new Dictionary<string, TmxMap>();
            Ships = new List<ShipData>();
        }
    }
}
