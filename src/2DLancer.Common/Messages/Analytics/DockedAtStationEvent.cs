﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages.Analytics
{
    /// <summary>
    /// An event for the player docking at a station.
    /// </summary>
    [TsClass(Module = "", Name = "IDockedAtStationEvent")]
    [DataContract]
    public class DockedAtStationEvent : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "DockedAtStationEvent";

        /// <summary>
        /// The name of the station that the player docked at.
        /// </summary>
        [DataMember]
        public string StationName { get; set; }

        /// <summary>
        /// Construct a new DockedAtStationEvent.
        /// </summary>
        public DockedAtStationEvent() 
            : base(NAME)
        {
        }
    }
}
