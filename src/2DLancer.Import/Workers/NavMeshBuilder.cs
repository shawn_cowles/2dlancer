﻿using System;
using System.Collections.Generic;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Builds the NavMesh for AI to use for pathing.
    /// </summary>
    public class NavMeshBuilder : AbstractWorker
    {
        private const int NAV_HAZARD_DISTANCE = 400;
        
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "NavMesh Builder"; }
        }


        /// <summary>
        /// Build the NavMesh.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                foreach (var zone in importContext.Zones.Values)
                {
                    FormZoneMesh(zone);
                }

                foreach (var zone in importContext.Zones.Values)
                {
                    FormInterzoneLinks(zone, importContext.Zones.Values);
                }
                
                // TODO purge isolated sub-graphs, and error if zone links aren't all on same graph

                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }

        private void FormZoneMesh(Zone zone)
        {
            try
            {
                foreach(var point in zone.NavPoints)
                {
                    foreach(var otherPoint in zone.NavPoints.Where(p=>p!= point))
                    {
                        if(!point.DirectLinks.Contains(otherPoint.Id)
                            && !otherPoint.DirectLinks.Contains(point.Id)
                            && PathIsUnObstructed(point,otherPoint, zone))
                        {
                            point.DirectLinks.Add(otherPoint.Id);
                            otherPoint.DirectLinks.Add(point.Id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ImportException($"Failure when building the NavMesh for zone '{zone.Name}'", ex);
            }
        }

        private bool PathIsUnObstructed(NavPoint point, NavPoint otherPoint, Zone zone)
        {
            var distBetweenPoints = point.Position.DistanceTo(otherPoint.Position);
            
            var dx = (otherPoint.Position.X - point.Position.X) / distBetweenPoints;
            var dy = (otherPoint.Position.Y - point.Position.Y) / distBetweenPoints;
            
            for (var i = 0; i < distBetweenPoints; i += NAV_HAZARD_DISTANCE / 2)
            {
                var p = new Position(
                    point.Position.X + dx * i,
                    point.Position.Y + dy * i);

                foreach (var hazard in zone.Celestials)
                {
                    if(hazard.Position.DistanceTo(p) < NAV_HAZARD_DISTANCE)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void FormInterzoneLinks(Zone zone, IEnumerable<Zone> zones)
        {
            try
            {
                foreach (var point in zone.NavPoints.Where(p=>p.IsZoneLink))
                {
                    var targetZone = zones.FirstOrDefault(z => z.Name == point.TargetZone);

                    if(targetZone == null)
                    {
                        throw new ImportException($"NavPoint targeting zone that cannot be found: '{point.TargetZone}'");
                    }

                    var dstPoint = targetZone.NavPoints
                        .OrderBy(p => p.Position.DistanceTo(point.TargetPosition))
                        .FirstOrDefault();

                    if (dstPoint == null)
                    {
                        throw new ImportException($"NavPoint targeting zone cannot find destination waypoint in: '{point.TargetZone}'");
                    }

                    point.JumpLinks.Add(dstPoint.Id);
                }
            }
            catch (Exception ex)
            {
                throw new ImportException($"Failure when building the NavMesh for zone '{zone.Name}'", ex);
            }
        }
    }
}

