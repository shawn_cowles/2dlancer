﻿// Handles storing and persisting player data
class PlayerDataService
{
    private INSURANCE_RATE = 0.50;

    private playerId: string;
    private playerBalance: number;
    private currentLoadout: ShipLoadout;

    // Enemy ships the player has "tagged" by damaging
    private playerTaggedShips: GameObject[];
    
    private analytics: AnalyticsService;
    private messageLog: MessageLog;

    private localStorageAvailable: boolean;
    private shipInsurancePaidFor: GameObject;
    
    constructor()
    {
        this.playerTaggedShips = [];
        this.shipInsurancePaidFor = null;
    }

    public Initialize(state: FlightState): void
    {
        this.analytics = state.Analytics;
        this.messageLog = state.MessageLog;

        // !! to turn truthyness into a bool
        try
        {
            this.localStorageAvailable = !!localStorage;
        }
        catch (e)
        {
            this.localStorageAvailable = false;
        }

        if (!this.localStorageAvailable)
        {
            console.log("Local storage not available.");
        }

        if (this.localStorageAvailable && localStorage.getItem("player_id") != null)
        {
            this.playerId = localStorage.getItem("player_id");
        }
        else
        {
            this.playerId = "guest" + Math.random();

            if (this.localStorageAvailable)
            {
                localStorage.setItem("player_id", this.playerId);
            }
        }

        if (this.localStorageAvailable
            && localStorage.getItem("player_balance") != null)
        {
            this.playerBalance = parseInt(localStorage.getItem("player_balance"));
        }
        else
        {
            this.playerBalance = 0;
        }

        // TODO this is just to make testing easy
        this.playerBalance += 20000;

        if (this.localStorageAvailable
            && localStorage.getItem("player_loadout") != null)
        {
            try
            {
                this.currentLoadout = JSON.parse(localStorage.getItem("player_loadout"));

                // gracefully handle adding modules to the ship loadout
                if (this.currentLoadout.MountedModules == null)
                {
                    this.currentLoadout.MountedModules = [];
                }
            }
            catch(exception) // if the format has changed, just reset the loadout
            {
                this.currentLoadout = null;
            }
        }
        else
        {
            this.currentLoadout = null;
        }

        if (this.currentLoadout == null)
        {
            this.SetDefaultLoadout();
        }
    }

    public PlayerId(): string
    {
        return this.playerId;
    }

    public GetBalance(): number
    {
        return this.playerBalance;
    }

    public SetBalance(newBalance: number): void
    {
        this.playerBalance = newBalance;

        if (this.localStorageAvailable)
        {
            localStorage.setItem("player_balance", newBalance.toString());
        }
    }

    public SetCurrentLoadout(loadout: ShipLoadout): void
    {
        this.currentLoadout = loadout;

        if (this.localStorageAvailable)
        {
            localStorage.setItem("player_loadout", JSON.stringify(loadout));
        }
    }

    public GetCurrentLoadout(): ShipLoadout
    {
        return this.currentLoadout;
    }

    public Update(state: FlightState): void
    {
        for (var i = 0; i < this.playerTaggedShips.length; i++)
        {
            if (this.playerTaggedShips[i].ShouldBeRemoved)
            {
                var bounty = 200;

                this.messageLog.LogMessage(Util.FormatAsMoney(bounty)
                    + " bounty from killing a pirate.");
                this.SetBalance(this.GetBalance() + bounty);
                this.analytics.MoneyEarned("bounty", "pirate", bounty);
                
                var bountyPopup = new GameObject(this.playerTaggedShips[i].Position, 0);
                bountyPopup.AddComponent(new FadingText("+ $200", 3));
                bountyPopup.AddComponent(new TimedLife(3, false));
                this.playerTaggedShips[i].Zone.GameObjects.push(bountyPopup);

                this.playerTaggedShips.splice(i, 1);
                i -= 1;
            }
        }

        // Player has died, reset their ship
        if (state.PlayerShip != null && state.PlayerShip.ShouldBeRemoved)
        {
            if (state.PlayerShip != null && state.PlayerShip != this.shipInsurancePaidFor)
            {
                this.shipInsurancePaidFor = state.PlayerShip;

                var insurancePayout = this.CalculateInsurancePayoutFor(state.PlayerShip)

                this.messageLog.LogMessage("Ship insurance pays out " 
                    + Util.FormatAsMoney(insurancePayout));
                this.SetBalance(this.GetBalance() + insurancePayout);
                this.analytics.MoneyEarned("insurance", "ship", insurancePayout);
            }

            this.SetDefaultLoadout();
        }
    }

    public TagIfNeeded(ship: GameObject): void
    {
        if (this.playerTaggedShips.indexOf(ship) < 0)
        {
            this.playerTaggedShips.push(ship);
        }
    }

    private SetDefaultLoadout(): void
    {
        this.SetCurrentLoadout(new ShipLoadout(
            ShipBuilder.CLASS_LANCER,
            null,
            null,
            false,
            [new AutocannonListing().Name]));
    }

    private CalculateInsurancePayoutFor(ship: GameObject): number
    {
        var shipData = ship.GetComponent<PurchaseInformation>(PurchaseInformation).ShipData;

        var baseCost = shipData.Cost;

        // ignore cost of the free starter ship
        if (shipData.Name == ShipBuilder.CLASS_LANCER)
        {
            baseCost = 0;
        }
        
        var moduleCost = 0;

        var slots = ship.GetComponents<ModuleSlot>(ModuleSlot);

        for (var i = 0; i < slots.length; i++)
        {
            if (slots[i].IsModuleMounted())
            {
                moduleCost += slots[i].GetMountedModule()
                    .GetListing()
                    .CostForSize(slots[i].Size);
            }
        }

        return (baseCost + moduleCost) * this.INSURANCE_RATE;
    }
}
