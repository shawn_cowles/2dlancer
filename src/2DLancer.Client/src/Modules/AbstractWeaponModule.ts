﻿/// <reference path="AbstractModule.ts" />

// Abstract base class for all primary weapon modules.
abstract class AbstractWeaponModule extends AbstractModule
{
    protected inputSource: InputSource;

    protected firingArc: number;

    protected isTurreted: boolean;
    private turretSprites: Phaser.Sprite[];
    protected turretFacing: number;

    protected cooldownTimer: number;

    protected offsetMagnitudes: number[];
    protected offsetDirections: number[];

    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.inputSource = parentShip.GetComponent<InputSource>(InputSource);
        this.isTurreted = slot.WeaponsTurreted;

        this.firingArc = DesignConstants.GimballedWeaponArc;
        this.cooldownTimer = 0;

        this.offsetDirections = offsetDirections;
        this.offsetMagnitudes = offsetMagnitudes;

        if (this.isTurreted)
        {
            this.turretFacing = parentShip.Facing;

            this.turretSprites = null;
        }
    }

    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (this.isTurreted)
        {
            if (this.turretSprites == null)
            {
                this.turretSprites = new Array<Phaser.Sprite>();

                for (var i = 0; i < this.offsetDirections.length; i++)
                {
                    var sprite = new Phaser.Sprite(state.game, 0, 0, "small_turret");
                    sprite.anchor.set(0.5, 0.5);
                    this.turretSprites.push(sprite);
                    state.SpriteGroups[FlightState.LAYER_OVERLAY].add(sprite);
                }
            }

            this.TurnAndPositionTurrets(parentShip, elapsedSeconds);
        }
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public UnInitialize(parentShip: GameObject): void
    {
        if (this.turretSprites != null)
        {
            for (var i = 0; i < this.turretSprites.length; i++)
            {
                this.turretSprites[i].destroy();
            }

            this.turretSprites = null;
        }
    }

    private TurnAndPositionTurrets(parentShip: GameObject, elapsedSeconds: number): void
    {
        var maxTurn = DesignConstants.TurretTurnRate * elapsedSeconds;
        var facing = Util.CorrectAngle(this.turretFacing);
        var delta = this.inputSource.DesiredFacing - facing;
        var correctedDelta = Util.CorrectAngle(delta);
        var turn = Util.Clamp(correctedDelta, maxTurn, -maxTurn);
        this.turretFacing = Util.CorrectAngle(facing + turn);

        var barrelPositions = new Array<Vector2>();
        for (var i = 0; i < this.turretSprites.length; i++)
        {
            var turretPos = Vector2.FromPolarOffset(
                this.offsetMagnitudes[i],
                this.offsetDirections[i],
                parentShip);

            this.turretSprites[i].position.set(turretPos.X, turretPos.Y);
            this.turretSprites[i].rotation = this.turretFacing;

            var barrelPos = Vector2.FromPolar(17, this.turretFacing)
                .Add(turretPos);

            barrelPositions.push(barrelPos);
        }
    }

    protected AdjustBarrelForTurret(raw: Vector2): Vector2
    {
        return raw.Add(Vector2.FromPolar(17, this.turretFacing));
    }
}