﻿using System;
using System.Runtime.Serialization;
using _2DLancer.Common.Messages.Analytics;
using Newtonsoft.Json;
using TypeLite;

namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// A message coming into the backend from a client.
    /// </summary>
    [TsClass(Module = "", Name = "IAbstractIncomingClientMessage")]
    [DataContract]
    public abstract class AbstractIncomingClientMessage : AbstractClientMessage
    {
        /// <summary>
        /// The id of the client that sent the message. Set after the message is deserialized.
        /// </summary>
        [TsIgnore]
        public string ClientId { get; set; }

        /// <summary>
        /// Construct a new AbstractIncomingClientMessage.
        /// </summary>
        /// <param name="messageName">The name of the message.</param>
        protected AbstractIncomingClientMessage(string messageName)
            : base(messageName)
        {
        }
        
        /// <summary>
        /// Deserialize a JSON string into a message object. Throws an <see cref="ArgumentException"/> if the message type is not recognized.
        /// </summary>
        /// <param name="messageType">The name of the message type.</param>
        /// <param name="json">The JSON string to deserialize.</param>
        /// <param name="clientId">The id of the client that sent the message.</param>
        /// <returns>The deserialized message object.</returns>
        public static AbstractIncomingClientMessage Deserialize(string messageType, string json, string clientId)
        {
            AbstractIncomingClientMessage message;

            switch (messageType)
            {
                case DockedAtStationEvent.NAME:
                    message = JsonConvert.DeserializeObject<DockedAtStationEvent>(json);
                    break;
                case MoneyEarnedEvent.NAME:
                    message = JsonConvert.DeserializeObject<MoneyEarnedEvent>(json);
                    break;
                case MoneySpentEvent.NAME:
                    message = JsonConvert.DeserializeObject<MoneySpentEvent>(json);
                    break;
                case PlayerSpawnedEvent.NAME:
                    message = JsonConvert.DeserializeObject<PlayerSpawnedEvent>(json);
                    break;
                case ShipRepairedEvent.NAME:
                    message = JsonConvert.DeserializeObject<ShipRepairedEvent>(json);
                    break;
                case ZoneVisitedEvent.NAME:
                    message = JsonConvert.DeserializeObject<ZoneVisitedEvent>(json);
                    break;
                case UserConnectedMessage.NAME:
                    message = JsonConvert.DeserializeObject<UserConnectedMessage>(json);
                    break;
                default:
                    throw new ArgumentException("Unexpected message type: " + messageType);
            }

            message.ClientId = clientId;

            return message;
        }
    }
}