﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Enumeration of possible module sizes
    /// </summary>
    [TsEnum(Module = "", Name = "ModuleSize")]
    public enum ModuleSize
    {
        /// <summary>
        /// Module slot does not exist on the ship.
        /// </summary>
        None,

        /// <summary>
        /// Small sized module.
        /// </summary>
        Small,

        /// <summary>
        /// Medium sized module.
        /// </summary>
        Medium,

        /// <summary>
        /// Large sized module.
        /// </summary>
        Large
    }
}
