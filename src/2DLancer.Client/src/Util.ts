﻿// A container for utility methods
class Util
{
    // Bring an angle to the range of PI to -PI, facilitates angle calculations
    public static CorrectAngle(inputAngle: number): number
    {
        while (inputAngle > Math.PI)
        {
            inputAngle -= 2 * Math.PI;
        }

        while (inputAngle < -Math.PI)
        {
            inputAngle += 2 * Math.PI;
        }

        return inputAngle;
    }

    // Clamp a number to a range
    public static Clamp(input: number, max: number, min: number): number
    {
        if (input < min)
        {
            return min;
        }

        if (input > max)
        {
            return max;
        }
        
        return input;
    }

    // Clamp a number to a range
    public static DegreesBetween(facingA: number, facingB: number): number
    {
        return Math.abs(Util.CorrectAngle(facingB - facingA));
    }

    // Pick a random item from an array
    public static PickRandom<T>(items: T[]): T
    {
        var i = Math.floor(Math.random() * items.length);

        return items[i];
    }

    // Format a string as money
    public static FormatAsMoney(amount: number): string
    {
        return "$" + amount
            .toFixed(2)
            .replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    // Wrap text to be under a specific line length
    public static WrapText(text: string, lineLength: number): string
    {
        var out = "";

        var words = text.split(" ");

        var line = "";

        for (var i = 0; i < words.length; i++)
        {
            var nextWord = words[i];

            if (nextWord.length + line.length > lineLength)
            {
                out += line + "\n";
                line = nextWord + " ";
            }
            else if (nextWord == "\n")
            {
                out += line + "\n";
                line = "";
            }
            else
            {
                line += nextWord + " ";
            }
        }

        return out + line;
    }
}