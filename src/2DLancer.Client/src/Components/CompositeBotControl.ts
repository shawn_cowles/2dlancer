﻿/// <reference path="AbstractComponent.ts" />

// A component that makes a ship follow a set of behaviors.
class CompositeBotControl extends AbstractComponent
{
    private behaviors: AbstractAiBehavior[];
    private inputSource: InputSource;

    constructor(behaviors: AbstractAiBehavior[])
    {
        super();

        this.behaviors = behaviors;
    }

    // grab the local input source and initialize behaviors
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);

        for (var i = 0; i < this.behaviors.length; i++)
        {
            this.behaviors[i].Initialize(this, zone);
        }
    }
    
    // Call each behavior to find one that applies
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.GameObject.HasControlOverride())
        {
            return;
        }

        var behaviorApplied = false;

        for (var i = 0; i < this.behaviors.length && !behaviorApplied; i++)
        {
            behaviorApplied = this.behaviors[i].Update(
                this,
                state,
                zone,
                this.inputSource,
                elapsedSeconds);
        }
    }

    public ControlShipActor(ship: Ship, state: FlightState, elapsedSeconds: number): void
    {
        var behaviorApplied = false;

        for (var i = 0; i < this.behaviors.length && !behaviorApplied; i++)
        {
            behaviorApplied = this.behaviors[i].ControlShipActor(ship, state, elapsedSeconds);
        }
    }
}