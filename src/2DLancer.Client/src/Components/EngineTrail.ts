﻿/// <reference path="AbstractComponent.ts" />

// Creates a colored trail that follows the game object, useful for showing ship motion and
// representing engine exhaust
class EngineTrail extends AbstractComponent
{
    public OffsetMagnitude: number;
    public OffsetDirection: number;
    private trailPoints: Vector2[];
    private nextPointCounter: number;

    public LingerTime: number;
    public PointsPerSecond: number;
    public Width: number;
    public MinAlpha: number;
    public MaxAlpha: number;
    public Tint: number;

    constructor(offsetPoint: Vector2, width: number, tint: number)
    {
        super();
        
        // Store the offset point in polar coordinates so it can track the game object properly as it turns
        this.OffsetMagnitude = Math.sqrt(offsetPoint.X * offsetPoint.X + offsetPoint.Y * offsetPoint.Y);
        this.OffsetDirection = Math.atan2(offsetPoint.Y, offsetPoint.X);
        this.trailPoints = [];
        this.nextPointCounter = 0;

        this.LingerTime = 2;
        this.PointsPerSecond = 10;
        this.Width = width;
        this.MinAlpha = 0;
        this.MaxAlpha = 0.5;
        this.Tint = tint;
    }

    // Clear any old trail points.
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.trailPoints = [];
    }

    // Draw engine trails.
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.nextPointCounter += elapsedSeconds;

        // Add new points
        if (this.nextPointCounter > 1 / this.PointsPerSecond)
        {
            this.nextPointCounter = 0;

            var point = Vector2.FromPolarOffset(
                this.OffsetMagnitude,
                this.OffsetDirection,
                this.GameObject);

            this.trailPoints.push(point);
        }

        // Remove old points
        if (this.trailPoints.length > this.LingerTime * this.PointsPerSecond)
        {
            this.trailPoints.splice(0, 1);
        }

        // Render the trail
        if (this.trailPoints.length > 0)
        {
            var alphaPerStep = (this.MaxAlpha - this.MinAlpha) / this.trailPoints.length;
            var alpha = this.MinAlpha;
            var color = this.Tint;

            state.Graphics.lineStyle(this.Width, color, alpha);

            state.Graphics.moveTo(this.trailPoints[0].X, this.trailPoints[0].Y);

            this.trailPoints.forEach((p) =>
            {
                state.Graphics.lineTo(p.X, p.Y);
                alpha += alphaPerStep;
                state.Graphics.lineStyle(this.Width, color, alpha);
                state.Graphics.moveTo(p.X, p.Y);

                return true;
            });

            // Because phaser acts weirdly when drawing a line between two points that are close,
            // finish the line a little "ahead" of where it should be, this will be hidden
            // underneath the ship's sprite, and make a smooth connection to the engines.
            var adjNextX = this.GameObject.Position.X + 1 * Math.cos(this.GameObject.Facing);
            var adjNextY = this.GameObject.Position.Y + 1 * Math.sin(this.GameObject.Facing);
            
            state.Graphics.lineTo(
                adjNextX + this.OffsetMagnitude * Math.cos(this.OffsetDirection + this.GameObject.Facing),
                adjNextY + this.OffsetMagnitude * Math.sin(this.OffsetDirection + this.GameObject.Facing));
        }
    }
}