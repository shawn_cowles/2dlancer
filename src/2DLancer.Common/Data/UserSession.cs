﻿using System;
using System.Collections.Generic;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Stores information on an active user session.
    /// </summary>
    public class UserSession
    {
        /// <summary>
        /// The client's user ID, sent from the client.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The date the session started.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Total spawn count for the session.
        /// </summary>
        public int SpawnCount { get; set; }

        /// <summary>
        /// Total income for the session.
        /// </summary>
        public double TotalIncome { get; set; }

        /// <summary>
        /// Total spending for the session.
        /// </summary>
        public double TotalSpending { get; set; }

        /// <summary>
        /// The date the session ended.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// How many times the player has docked this session.
        /// </summary>
        public int DockCount { get; set; }

        /// <summary>
        /// How many times the player has repaired this session.
        /// </summary>
        public int RepairCount { get; set; }

        /// <summary>
        /// How many times the player has transitioned between zones this session.
        /// </summary>
        public int ZoneTransitions { get; set; }

        /// <summary>
        /// All items bought during the session.
        /// </summary>
        public List<string> ItemsBought { get; set; }

        /// <summary>
        /// All zones visited during the session.
        /// </summary>
        public List<string> ZonesVisited { get; set; }

        /// <summary>
        /// Construct a new UserSession.
        /// </summary>
        public UserSession()
        {
            ItemsBought = new List<string>();
            ZonesVisited = new List<string>();
        }
    }
}
