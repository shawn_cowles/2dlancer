﻿using System;
using System.IO;
using System.Linq;
using _2DLancer.Import.Data;
using Newtonsoft.Json;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Worker that exports import data to JSON files.
    /// </summary>
    public class DataExportWorker : AbstractWorker
    {
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Data Export Worker"; }
        }

        /// <summary>
        /// Read all loaded data from the context and write them to JSON files.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var zones = importContext.Zones.Values
                    .ToArray();

                var zonesFile = Path.Combine(importContext.ExportPath, "zones.json");

                using (var fileStream = new FileStream(zonesFile, FileMode.Create))
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(zones));
                }


                var shipsFile = Path.Combine(importContext.ExportPath, "ships.json");

                using (var fileStream = new FileStream(shipsFile, FileMode.Create))
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(importContext.Ships));
                }

                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }
    }
}
