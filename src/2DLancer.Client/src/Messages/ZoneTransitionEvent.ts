﻿class ZoneVisitedEvent implements IZoneVisitedEvent
{
    public MessageName = "ZoneVisitedEvent";

    public ZoneName: string;

    constructor(zoneName: string)
    {
        this.ZoneName = zoneName;
    }
}