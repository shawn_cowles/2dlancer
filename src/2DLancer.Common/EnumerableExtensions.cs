﻿using System.Collections.Generic;

namespace _2DLancer.Common
{
    /// <summary>
    /// Holds useful extensions relating to <see cref="IEnumerable{T}"/>.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Create an <see cref="IEnumerable{T}"/> from a single item. If the item is null
        /// the <see cref="IEnumerable{T}"/> will be empty instead.
        /// </summary>
        /// <typeparam name="T">The type of the item.</typeparam>
        /// <param name="item">The item to put into the enumerable.</param>
        /// <returns>An <see cref="IEnumerable{T}"/> containing <paramref name="item"/>.</returns>
        public static IEnumerable<T> Yield<T>(this T item)
        {
            if (item == null)
            {
                yield break;
            }

            yield return item;
        }
    }
}
