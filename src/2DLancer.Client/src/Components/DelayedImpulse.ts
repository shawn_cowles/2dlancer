﻿/// <reference path="AbstractComponent.ts" />

// DelayedImpulse adds an impulse to the GameObject's velocity after a delay.
class DelayedImpulse extends AbstractComponent
{
    private delay: number;
    private impulse: Vector2;
    private impulseApplied: boolean;

    constructor(delay: number, impulse: Vector2)
    {
        super();

        this.delay = delay;
        this.impulse = impulse;
        this.impulseApplied = false;
    }

    // Count down the timer and apply the impulse
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.impulseApplied)
        {
            return;
        }
        
        this.delay -= elapsedSeconds;

        if (this.delay <= 0)
        {
            this.impulseApplied = true;
            this.GameObject.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity.Add(this.impulse);
        }
    }
}