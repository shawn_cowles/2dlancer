﻿using System;
using _2DLancer.Server.Interfaces;
using log4net;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// A log4net based logger.
    /// </summary>
    internal class Log4NetLogger : ILogger
    {
        private ILog _internalLog;
        
        internal Log4NetLogger(ILog internalLog)
        {
            _internalLog = internalLog;
        }

        /// <summary>
        /// Log a debug message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Debug(string message)
        {
            _internalLog.Debug(message);
        }

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Info(string message)
        {
            _internalLog.Info(message);
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Warn(string message)
        {
            _internalLog.Warn(message);
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        public void Error(string message)
        {
            _internalLog.Error(message);
        }

        /// <summary>
        /// Log an error message and an exception.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception.</param>
        public void Error(string message, Exception ex)
        {
            _internalLog.Error(message, ex);
        }

        /// <summary>
        /// Log an exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public void Error(Exception ex)
        {
            _internalLog.Error("", ex);
        }
    }
}
