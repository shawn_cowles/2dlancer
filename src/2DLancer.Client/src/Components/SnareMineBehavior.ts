﻿/// <reference path="AbstractComponent.ts" />

// SnareMineBehavior attaches to enemy ships and slows them down.
class SnareMineBehavior extends AbstractComponent
{
    public static SNARE_PENALTY = 0.3;

    private SNARE_RANGE = 500;

    private target: GameObject;
    private cableLength: number;
    
    constructor()
    {
        super();

        this.target = null;
    }
    
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.target == null || this.target.ShouldBeRemoved || this.target.Zone != zone)
        {
            if (this.target != null)
            {
                this.target.GetComponent<Thrusters>(Thrusters).RemoveSnare(this);
            }

            this.target = null;

            this.FindTarget(zone.GameObjects);

            if (this.target != null)
            {
                this.cableLength = this.GameObject.Position.DistanceTo(this.target.Position);
                this.target.GetComponent<Thrusters>(Thrusters).AddSnare(this);
            }
        }

        if (this.target != null)
        {
            var toTarget = this.GameObject.Position.Subtract(this.target.Position);

            this.GameObject.Facing = toTarget.Direction();

            if (toTarget.Magnitude() > this.cableLength)
            {
                // follow target
                this.GameObject.Position = Vector2.FromPolar(
                    -this.cableLength,
                    this.GameObject.Position.DirectionTo(this.target.Position))
                    .Add(this.target.Position);
            }
            
            // draw the snare
            state.OverlayGraphics.lineStyle(2, 0x666666);
            state.OverlayGraphics.moveTo(
                this.GameObject.Position.X,
                this.GameObject.Position.Y);
            state.OverlayGraphics.lineTo(
                this.target.Position.X,
                this.target.Position.Y);
        }
    }


    public OnRemoved(zone: Zone): void
    {
        super.OnRemoved(zone);

        if (this.target != null)
        {
            this.target.GetComponent<Thrusters>(Thrusters).RemoveSnare(this);
        }
    }
    
    private FindTarget(potentials: GameObject[]): void
    {
        var nearDist = <number>null;
        this.target = null;

        for (var i = 0; i < potentials.length; i++)
        {
            // only consider objects that can be snared
            if (potentials[i].HasComponent(Thrusters))
            {
                // only target hostiles
                if (Mechanics.IsHostile(this.GameObject, potentials[i]))
                {
                    var dist = this.GameObject.Position.DistanceTo(potentials[i].Position);

                    if (dist < this.SNARE_RANGE && (nearDist == null || dist < nearDist))
                    {
                        nearDist = dist;
                        this.target = potentials[i];
                    }
                }
            }
        }
    }
}