﻿using System;
using System.Xml.Linq;
using _2DLancer.Import.Data;
using TiledSharp;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Abstract base class for a worker that validates or imports.
    /// </summary>
    public abstract class AbstractWorker
    {
        /// <summary>
        /// Construct a new AbstractWorker.
        /// </summary>
        protected AbstractWorker()
        {
        }

        /// <summary>
        /// The name of the worker.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Run the AbstractWorker.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public abstract WorkResult DoWork(ImportContext importContext);

        /// <summary>
        /// Return a WorkResult for an importer that encountered an exception.
        /// </summary>
        /// <param name="ex">The encountered exception.</param>
        /// <returns>A WorkResult containing the details of the exception.</returns>
        protected WorkResult ReportOnException(Exception ex)
        {
            var message = "Exception:\n";
            var currentException = ex;

            while (currentException != null)
            {
                message += $"{currentException.Message}\n";

                if(currentException.InnerException == null)
                {
                    message += $"{currentException.StackTrace}\n";
                }

                currentException = currentException.InnerException;
            }

            return new WorkResult(Name, false, message);
        }

        /// <summary>
        /// Get a property from a TmxObject, or throw a helpful exception if it does not exist.
        /// </summary>
        /// <param name="obj">The object to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the object.</returns>
        protected string GetProperty(TmxObject obj, string propertyName)
        {
            if (!obj.Properties.ContainsKey(propertyName))
            {
                throw new ImportException($"Object '{obj.Name}' is missing property '{propertyName}'");
            }

            return obj.Properties[propertyName];
        }

        /// <summary>
        /// Get a property from a TmxObject as an int, or throw a helpful exception if it does not exist or does 
        /// not parse into an int.
        /// </summary>
        /// <param name="obj">The object to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the object as an int.</returns>
        protected int GetIntProperty(TmxObject obj, string propertyName)
        {
            var val = GetProperty(obj, propertyName);

            int i;

            if (int.TryParse(val, out i))
            {
                return i;
            }

            throw new ImportException($"Property '{propertyName}' on object '{obj.Name}' is not an int.");
        }

        /// <summary>
        /// Get a property from a TmxObject as an double, or throw a helpful exception if it does not exist or does 
        /// not parse into an double.
        /// </summary>
        /// <param name="obj">The object to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the object as an double.</returns>
        protected double GetDoubleProperty(TmxObject obj, string propertyName)
        {
            var val = GetProperty(obj, propertyName);

            double d;

            if (double.TryParse(val, out d))
            {
                return d;
            }

            throw new ImportException($"Property '{propertyName}' on object '{obj.Name}' is not a double.");
        }
        
        /// <summary>
        /// Get a property from a TmxObject as an enum, or throw a helpful exception if it does not
        /// exist or does not parse into an enum.
        /// </summary>
        /// <typeparam name="T">The enum type to parse the property as.</typeparam>
        /// <param name="obj">The object to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the object as an enum.</returns>
        protected T GetEnumProperty<T>(TmxObject obj, string propertyName) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("Provided type must be an enumerated type.");
            }
            
            var val = GetProperty(obj, propertyName);

            T res;

            if (Enum.TryParse<T>(val, out res))
            {
                return res;
            }

            throw new ImportException($"Property '{propertyName}' on object '{obj.Name}' is not a {typeof(T).Name}.");
        }
        
        /// <summary>
        /// Get a property from a TmxObject as a boolean, or throw a helpful exception if it does 
        /// not exist or does not parse into an boolean.
        /// </summary>
        /// <param name="obj">The object to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the object as a boolean.</returns>
        protected bool GetBoolProperty(TmxObject obj, string propertyName)
        {
            var val = GetProperty(obj, propertyName);

            bool b;

            if (bool.TryParse(val, out b))
            {
                return b;
            }

            throw new ImportException($"Property '{propertyName}' on object '{obj.Name}' is not a bool.");
        }
        
        /// <summary>
        /// Get a property from a TmxMap, or throw a helpful exception if it does not exist.
        /// </summary>
        /// <param name="map">The map to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the map.</returns>
        protected string GetProperty(TmxMap map, string propertyName)
        {
            if (!map.Properties.ContainsKey(propertyName))
            {
                throw new ImportException($"Map is missing property '{propertyName}'");
            }

            return map.Properties[propertyName];
        }

        /// <summary>
        /// Get a property from a TmxMap as an int, or throw a helpful exception if it does not exist or does 
        /// not parse into an int.
        /// </summary>
        /// <param name="map">The map to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the map as an int.</returns>
        protected int GetIntProperty(TmxMap map, string propertyName)
        {
            var val = GetProperty(map, propertyName);

            int i;

            if (int.TryParse(val, out i))
            {
                return i;
            }

            throw new ImportException($"Property '{propertyName}' on map is not an int.");
        }

        /// <summary>
        /// Get a property from a TmxMap as a double, or throw a helpful exception if it does not exist or does 
        /// not parse into an double.
        /// </summary>
        /// <param name="map">The map to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the object as an double.</returns>
        protected double GetDoubleProperty(TmxMap map, string propertyName)
        {
            var val = GetProperty(map, propertyName);

            double d;

            if (double.TryParse(val, out d))
            {
                return d;
            }

            throw new ImportException($"Property '{propertyName}' on map is not a double.");
        }


        /// <summary>
        /// Get a property from a TmxMap as an enum, or throw a helpful exception if it does not
        /// exist or does not parse into an enum.
        /// </summary>
        /// <typeparam name="T">The enum type to parse the property as.</typeparam>
        /// <param name="map">The map to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the map as an enum.</returns>
        protected T GetEnumProperty<T>(TmxMap map, string propertyName) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("Provided type must be an enumerated type.");
            }

            var val = GetProperty(map, propertyName);

            T res;

            if (Enum.TryParse<T>(val, out res))
            {
                return res;
            }

            throw new ImportException($"Property '{propertyName}' on map is not a {typeof(T).Name}.");
        }


        /// <summary>
        /// Get a property from a TmxMap as a boolean, or throw a helpful exception if it does 
        /// not exist or does not parse into an boolean.
        /// </summary>
        /// <param name="map">The map to read the property from.</param>
        /// <param name="propertyName">The name of the property to get.</param>
        /// <returns>The value of the property on the map as a boolean.</returns>
        protected bool GetBoolProperty(TmxMap map, string propertyName)
        {
            var val = GetProperty(map, propertyName);

            bool b;

            if (bool.TryParse(val, out b))
            {
                return b;
            }

            throw new ImportException($"Property '{propertyName}' on map is not a bool.");
        }



        /// <summary>
        /// Get an attribute an XElement, or throw a helpful exception if it does not exist.
        /// </summary>
        /// <param name="element">The XElement to read the attribute from.</param>
        /// <param name="attributeName">The name of the attribute to get.</param>
        /// <returns>The value of the attribute on the XElement.</returns>
        protected string GetAttribute(XElement element, string attributeName)
        {
            var attribute = element.Attribute(attributeName);

            if (attribute == null)
            {
                throw new ImportException($"XElement '{element.Name}' is missing attribute '{attributeName}'");
            }

            return attribute.Value;
        }
        
        /// <summary>
        /// Get an attribute from an XElement as an double, or throw a helpful exception if it does
        /// not exist or does not parse into an double.
        /// </summary>
        /// <param name="element">The XElement to read the attribute from.</param>
        /// <param name="attributeName">The name of the attribute to get.</param>
        /// <returns>The value of the property on the XElement as an double.</returns>
        protected double GetDoubleAttribute(XElement element, string attributeName)
        {
            var val = GetAttribute(element, attributeName);

            double d;

            if (double.TryParse(val, out d))
            {
                return d;
            }

            throw new ImportException($"Property '{attributeName}' on element '{element.Name}' is not a double.");
        }
    }
}
