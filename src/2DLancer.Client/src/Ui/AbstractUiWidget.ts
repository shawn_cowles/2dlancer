﻿// The base class for any UI elements.
abstract class AbstractUiWidget
{
    private isInitialized: boolean;
    
    constructor()
    {
        this.isInitialized = false;
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
    }
    
    // Update the widget for the latest simulation update
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        if (!this.isInitialized)
        {
            this.isInitialized = true;
            this.Initialize(state);
        }
    }

    // Determine if a point is over a UI element, used to suppress shooting when the player
    // is using the ui
    public abstract PointIsInPanel(point: Vector2): boolean;

    // Perform any needed cleanup
    public Destroy():void
    {
    }
}