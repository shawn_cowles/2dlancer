﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// A message leaving the backend going to one or more clients.
    /// </summary>
    [TsClass(Module = "", Name = "IAbstractOutgoingClientMessage")]
    [DataContract]
    public abstract class AbstractOutgoingClientMessage : AbstractClientMessage
    {
        /// <summary>
        /// The ids of the clients to send to.
        /// </summary>
        [TsIgnore]
        public string[] ClientIds { get; }

        /// <summary>
        /// Construct a new IncomingClientMessage.
        /// </summary>
        /// <param name="clientIds">The ids of the clients to send to.</param>
        /// <param name="messageName">The name of the message.</param>
        protected AbstractOutgoingClientMessage(string messageName, string[] clientIds)
            :base(messageName)
        {
            ClientIds = clientIds;
        }
    }
}
