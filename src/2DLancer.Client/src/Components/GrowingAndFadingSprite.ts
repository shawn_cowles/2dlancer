﻿/// <reference path="SpriteDisplay.ts" />

// A component that changes the size and alpha of a sprite over time. Useful for explosions and
// muzzle flashes.
class GrowingAndFadingSprite extends SpriteDisplay
{
    private currentScale: number;
    private currentAlpha: number;
    private scaleDelta: number;
    private alphaDelta: number;
    private duration: number;

    constructor(spriteName: string, spriteLayer: number, tint: number,
        duration: number, startScale: number, endScale: number, startAlpha: number, endAlpha: number)
    {
        super(spriteName, spriteLayer, tint, 0);

        this.duration = duration;
        this.currentScale = startScale;
        this.currentAlpha = startAlpha;

        this.scaleDelta = (endScale - startScale) / duration;
        this.alphaDelta = (endAlpha - startAlpha) / duration;
    }
    
    // Grow the sprite
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.duration > 0)
        {
            this.duration -= elapsedSeconds;
            
            this.currentScale += this.scaleDelta * elapsedSeconds;
            this.currentAlpha += this.alphaDelta * elapsedSeconds;
            this.sprite.scale = new Phaser.Point(this.currentScale, this.currentScale);
            this.sprite.alpha = this.currentAlpha;
        }
    }
}