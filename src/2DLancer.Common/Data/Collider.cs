﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A collision circle of a ship.
    /// </summary>
    [TsClass(Module = "", Name = "ICollider")]
    public class Collider
    {
        /// <summary>
        /// The position offset of the collider on the ship.
        /// </summary>
        public Position Offset { get; set; }

        /// <summary>
        /// The radius of the collider.
        /// </summary>
        public double Radius { get; set; }
    }
}
