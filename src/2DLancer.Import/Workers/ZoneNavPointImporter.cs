﻿using System;
using System.Collections.Generic;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;
using TiledSharp;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports navigation points like nav bouys, waypoints and the like.
    /// </summary>
    public class ZoneNavPointImporter : AbstractWorker
    {
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "NavPoint Importer"; }
        }


        /// <summary>
        /// Import navigation points.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            var nextId = 0;

            try
            {
                foreach (var zoneName in importContext.Zones.Keys)
                {
                    var zone = importContext.Zones[zoneName];

                    foreach (var layer in importContext.ZoneMaps[zoneName].ObjectGroups)
                    {
                        foreach (var obj in layer.Objects)
                        {
                            try
                            {
                                if (obj.Type == ObjectTypes.NAV_BOUY)
                                {
                                    ImportNavBouy(nextId, zone, obj, importContext.Zones.Keys);
                                    nextId += 1;
                                }

                                if(obj.Type == ObjectTypes.WAYPOINT)
                                {
                                    ImportWaypoint(nextId, zone, obj);
                                    nextId += 1;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}' in zone '{zone.Name}'", ex);
                            }
                        }
                    }
                }
                
                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }

        private void ImportNavBouy(int id, Zone zone, TmxObject obj, IEnumerable<string> allZoneNames)
        {
            var targetZone = GetProperty(obj, PropertyNames.BOUY_TARGET_ZONE);

            if (!allZoneNames.Contains(targetZone))
            {
                throw new ImportException($"Target zone '{targetZone}' does not exist as a map.");
            }

            var targetX = GetDoubleProperty(obj, PropertyNames.BOUY_TARGET_X);
            var targetY = GetDoubleProperty(obj, PropertyNames.BOUY_TARGET_Y);

            zone.NavPoints.Add(new NavPoint
            {
                Id = id,
                Name = obj.Name,
                Position = new Position(obj.X + obj.Width / 2, obj.Y + obj.Height / 2),
                IsZoneLink = true,
                TargetZone = targetZone,
                TargetPosition = new Position(targetX, targetY)
            });
        }

        private void ImportWaypoint(int id, Zone zone, TmxObject obj)
        {
            zone.NavPoints.Add(new NavPoint
            {
                Id = id,
                Name = obj.Name,
                Position = new Position(obj.X + obj.Width / 2, obj.Y + obj.Height / 2),
                IsZoneLink = false
            });
        }
    }
}

