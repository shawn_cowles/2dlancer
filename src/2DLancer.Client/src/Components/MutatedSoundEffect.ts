﻿/// <reference path="SoundEffect.ts" />

// Plays audio files that have multiple versions, choosing a random version.
class MutatedSoundEffect extends SoundEffect
{
    constructor(soundName: string, versionCount: number, volume?: number)
    {
        super(
            soundName + "_" + Math.floor(Math.random() * (versionCount)),
            volume);
    }
}