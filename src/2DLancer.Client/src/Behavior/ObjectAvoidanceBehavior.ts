﻿/// <reference path="AbstractAiBehavior.ts" />

// A behavior that avoids collisions with other objects.
class ObjectAvoidanceBehavior extends AbstractAiBehavior
{
    // Initialize the behavior
    public Initialize(control: CompositeBotControl, zone: Zone): void
    {
    }

    // Perform an update, returns true if the behavior applies, false otherwise
    public Update(
        control: CompositeBotControl,
        state: FlightState,
        zone: Zone,
        inputSource: InputSource,
        elapsedSeconds: number): boolean
    {
        for (var i = 0; i < zone.GameObjects.length; i++)
        {
            var other = zone.GameObjects[i];
            var bearing = control.GameObject.Position.BearingTo(control.GameObject.Facing, other.Position);


            if (other != control.GameObject && Math.abs(bearing) < Math.PI / 4)
            {
                var collider = other.GetComponent<Collider>(Collider);

                if (collider != null)
                {
                    var distance = other.Position.DistanceTo(control.GameObject.Position);

                    if (distance <= collider.MaximumRadius * 4)
                    {
                        inputSource.DesiredFacing = other.Position.DirectionTo(control.GameObject.Position);
                        
                        inputSource.ThrustFoward = true;
                        return true;
                    }
                }
            }
        }

        return false;
    }



    public ControlShipActor(
        ship: Ship,
        state: FlightState,
        elapsedSeconds: number): boolean
    {
        // skip object avoidance and collisions in background simulation
        return false;
    }
}