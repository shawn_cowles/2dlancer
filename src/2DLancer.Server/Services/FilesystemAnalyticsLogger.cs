﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using _2DLancer.Common.Data;
using _2DLancer.Common.Messages;
using _2DLancer.Server.Interfaces;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// Writes analytics logs to the filesystem.
    /// </summary>
    public class FileSystemAnalyticsLogger : AbstractBusService, IInitializableService, IDisposable
    {
        /// <summary>
        /// The millisecond delay between each check for producing a daily summary.
        /// </summary>
        private const int DAILY_SUMMARY_TICK = 3600000; // an hour

        private readonly ISettingsProvider _settingsProvider;
        private readonly ILogger _logger;

        private StreamWriter _sessionOutputStream;
        private StreamWriter _summaryOutputStream;
        private List<UserSession> _finishedSessions;
        private Timer _summaryTimer;
        private DateTime _lastSummaryDate;

        /// <summary>
        /// Construct a new FileSystemAnalyticsLogger
        /// </summary>
        /// <param name="settingsProvider">The settings provider to use.</param>
        /// <param name="loggerProvider">The logger provider to use.</param>
        public FileSystemAnalyticsLogger(
            ISettingsProvider settingsProvider,
            ILoggerProvider loggerProvider)
            :base(MatchPredicate)
        {
            _settingsProvider = settingsProvider;
            _logger = loggerProvider.GetLoggerFor(typeof(FileSystemAnalyticsLogger));

            _finishedSessions = new List<UserSession>();
            _lastSummaryDate = DateTime.UtcNow;
        }

        private static bool MatchPredicate(AbstractMessage obj)
        {
            return obj is SessionEndedMessage;
        }

        /// <summary>
        /// Initialize the FileSystemAnalyticsLogger.
        /// </summary>
        public void Initialize()
        {
            try
            {
                Directory.CreateDirectory(_settingsProvider.GetDataDirectory);

                _summaryTimer = new Timer(TestForDailySummary, null, DAILY_SUMMARY_TICK, DAILY_SUMMARY_TICK);
                
                _sessionOutputStream = new StreamWriter(
                    Path.Combine(
                        _settingsProvider.GetDataDirectory,
                        "sessions.txt"), 
                    true);

                _summaryOutputStream = new StreamWriter(
                    Path.Combine(
                        _settingsProvider.GetDataDirectory,
                        "daily_summaries.txt"),
                    true);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
        }


        /// <summary>
        /// Handle messages from the bus.
        /// </summary>
        /// <param name="messages">The incoming messages.</param>
        protected override void HandleMessages(AbstractMessage[] messages)
        {
            try
            {
                foreach (var message in messages.OfType<SessionEndedMessage>())
                {
                    var itemsBought = string.Join("\n", message.Session.ItemsBought.Distinct());
                    var zonesVisited = string.Join("\n", message.Session.ZonesVisited.Distinct());

                    _sessionOutputStream.WriteLine("===== SESSION =====");
                    _sessionOutputStream.WriteLine($"started: {message.Session.StartDate}");
                    _sessionOutputStream.WriteLine($"duration: {(message.Session.EndDate - message.Session.StartDate).TotalSeconds} s");
                    _sessionOutputStream.WriteLine($"total income: {message.Session.TotalIncome}");
                    _sessionOutputStream.WriteLine($"total spent: {message.Session.TotalSpending}");
                    _sessionOutputStream.WriteLine($"spawns: {message.Session.SpawnCount}");
                    _sessionOutputStream.WriteLine($"zone transitions: {message.Session.ZoneTransitions}");
                    _sessionOutputStream.WriteLine($"repair count: {message.Session.RepairCount}");
                    _sessionOutputStream.WriteLine($"dock count: {message.Session.DockCount}");
                    _sessionOutputStream.WriteLine($"items bought:\n{itemsBought}");
                    _sessionOutputStream.WriteLine($"zones visited:\n{zonesVisited}");

                    _sessionOutputStream.WriteLine();

                    _finishedSessions.Add(message.Session);
                }

                _sessionOutputStream.Flush();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private void TestForDailySummary(object state)
        {
            try
            {
                if (_lastSummaryDate.Day != DateTime.UtcNow.Day)
                {
                    _lastSummaryDate = DateTime.UtcNow;

                    LogDailySummary();

                    _finishedSessions.Clear();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        
        private void LogDailySummary()
        {
            try
            {
                var totalCount = _finishedSessions.Count;

                var avgLength = 0.0;
                var avgSpawns = 0.0;
                var avgDocks = 0.0;

                if (_finishedSessions.Any())
                {
                    avgLength = _finishedSessions
                        .Select(s => (s.EndDate - s.StartDate).TotalSeconds)
                        .Average();

                    avgSpawns = _finishedSessions
                        .Select(s => s.SpawnCount)
                        .Average();

                    avgDocks = _finishedSessions
                        .Select(s => s.DockCount)
                        .Average();
                }

                var allItemsBought = _finishedSessions
                    .SelectMany(s => s.ItemsBought)
                    .ToArray();

                var allZonesVisited = _finishedSessions
                    .SelectMany(s => s.ZonesVisited.Distinct())
                    .ToArray();

                _summaryOutputStream.WriteLine($"===== DAILY SUMMARY FOR {DateTime.UtcNow} =====");
                _summaryOutputStream.WriteLine($"total sessions: {totalCount}");
                _summaryOutputStream.WriteLine($"average session length: {avgLength} s");
                _summaryOutputStream.WriteLine($"average spawns: {avgSpawns}");
                _summaryOutputStream.WriteLine($"average docks: {avgDocks}");
                _summaryOutputStream.WriteLine($"Total Purchases");
                foreach (var item in allItemsBought.Distinct())
                {
                    var qty = allItemsBought.Count(i => i == item);

                    _summaryOutputStream.WriteLine($" {item}: {qty}");
                }
                _summaryOutputStream.WriteLine($"Zone Visits");
                foreach (var zone in allZonesVisited.Distinct())
                {
                    var qty = allZonesVisited.Count(z => z == zone);

                    _summaryOutputStream.WriteLine($" {zone}: {qty}");
                }

                _summaryOutputStream.WriteLine();
                _summaryOutputStream.Flush();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Stop the daily summary timer, and dump the current summary to disk.
        /// </summary>
        public override void Stop()
        {
            base.Stop();
            
            LogDailySummary();

            _summaryTimer.Dispose();
        }
        
        /// <summary>
        /// Dispose of the FileSystemAnalyticsLogger.
        /// </summary>
        public void Dispose()
        {
            if (_sessionOutputStream != null)
            {
                _sessionOutputStream.Flush();
                _sessionOutputStream.Close();

                _summaryOutputStream.Flush();
                _summaryOutputStream.Close();
            }
        }
    }
}
