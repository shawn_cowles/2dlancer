﻿/// <reference path="AbstractComponent.ts" />

// OffsetFollow moves a GameObject to follow another GameObject, appearing as if it were attached.
class OffsetFollow extends AbstractComponent
{
    private target: GameObject;
    private offsetDirection: number;
    private offsetMagnitude: number;

    constructor(target: GameObject)
    {
        super();

        this.target = target;
    }
    
    // set the initial offsets
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);
        
        // subtract current target facing to rotate the offset into the standard object-facing-left
        // space used for engine and weapon offsets
        var offset = this.GameObject.Position.Subtract(this.target.Position);
        this.offsetDirection = offset.Direction() - this.target.Facing;
        this.offsetMagnitude = offset.Magnitude();
    }

    // Move this GameObject to follow target
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.target != null && !this.target.ShouldBeRemoved)
        {
            this.GameObject.Position = Vector2.FromPolarOffset(
                this.offsetMagnitude,
                this.offsetDirection,
                this.target);
        }
    }
}