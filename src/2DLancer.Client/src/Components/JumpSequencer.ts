﻿/// <reference path="AbstractComponent.ts" />

// Handles jumping a ship to a different zone.
class JumpSequencer extends AbstractComponent
{
    private inputSource: InputSource;
    public CountdownTimer: number;
    private destinationZone: string;
    private destinationPosition: Vector2;
    private soundEffect: SoundEffect;

    constructor(destinationZone: string, position: Vector2)
    {
        super();

        this.destinationZone = destinationZone;
        this.destinationPosition = position;
        this.CountdownTimer = 2;
        this.IsControlOverride = true;
    }
    
    // Initialize the component for active sim mode.
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
        this.soundEffect = new SoundEffect("jump_charge", 0.5);
        this.GameObject.AddComponent(this.soundEffect);
    }
    
    // Countdown and jump
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.CountdownTimer -= elapsedSeconds;

        this.inputSource.DesiredFacing = zone.ZoneCenter.DirectionTo(this.GameObject.Position);
        this.inputSource.DeactivateAllModules();
        this.inputSource.ThrustAft = false;
        this.inputSource.ThrustFoward = false;
        this.inputSource.ThrustPort = false;
        this.inputSource.ThrustStarboard = false;

        if (this.CountdownTimer <= 0)
        {
            // place the ship near the target, but not on top of it
            var actualDestination = Vector2.FromPolar(
                150,
                Math.random() * 2 * Math.PI)
                .Add(this.destinationPosition);

            // Uninit all of the ship components, so they can properly init in the new zone
            // this will hide the ship sprites while jumping and reset engine trails
            this.GameObject.ForceUninitialize();

            // Remove the jumping ship
            zone.GameObjects.splice(zone.GameObjects.indexOf(this.GameObject), 1);
            
            // add the jump out effect
            var jumpOutEffect = ShipBuilder.BuildJumpOutEffect(this.GameObject);
            zone.GameObjects.push(jumpOutEffect);
            
            // Special steps for the player
            if (this.GameObject == state.PlayerShip)
            {
                var jumpInEffect = ShipBuilder.BuildJumpInEffect(actualDestination, this.GameObject.Facing, this.GameObject);
                var jumpInSpawner = new GameObject(actualDestination, this.GameObject.Facing);
                jumpInSpawner.AddComponent(new DelayedSpawn(0.3, jumpInEffect));
                jumpInSpawner.AddComponent(new TimedLife(1, false));

                jumpOutEffect.AddComponent(new DelayedAction(0.6,
                    (z: Zone, s: FlightState) =>
                    {
                        this.GameObject.Position = actualDestination; // for camera focus when spawning
                        s.SetActiveZone(this.destinationZone);
                        s.GetActiveZone().GameObjects.push(jumpInSpawner);
                    }));
            }
            else
            {
                this.GameObject.GetComponent<ActorLink>(ActorLink)
                    .Actor
                    .TransitionToZone(
                        actualDestination,
                        state.GetZone(this.destinationZone));
            }
            

            // Remove the jump sequencer, its job is done
            this.GameObject.RemoveComponent(zone, this);
            if (this.soundEffect != null)
            {
                this.GameObject.RemoveComponent(zone, this.soundEffect);
            }
        }
    }
    
    public OnRemoved(zone: Zone): void
    {
        super.OnRemoved(zone);

        if (this.soundEffect != null)
        {
            this.GameObject.RemoveComponent(zone, this.soundEffect);
        }
    }
}