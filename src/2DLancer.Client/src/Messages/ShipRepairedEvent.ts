﻿class ShipRepairedEvent implements IShipRepairedEvent
{
    public MessageName = "ShipRepairedEvent";

    public StationName: string;

    constructor(stationName: string)
    {
        this.StationName = stationName;
    }
}