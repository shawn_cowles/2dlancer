﻿// All ships, projectiles, stations, etc in the game are GameObjects. GameObjects have components that define their specific behavior.
class GameObject
{
    // The position of the GameObject within its zone
    public Position: Vector2;

    // The angle (in radians) the GameObject is facing, 0 is to the right
    public Facing: number;

    // This flags the GameObject for removal at the end of the update cycle
    public ShouldBeRemoved: boolean;
    
    // Any components this GameObject contains
    private components: AbstractComponent[];

    // Cached for quick access.
    public PhysicsBody: PhysicsBody;

    // The current zone of the GameObject
    public Zone: Zone;
    
    constructor(position: Vector2, facing: number)
    {
        this.Position = position;
        this.Facing = facing;
        this.ShouldBeRemoved = false;
        this.components = [];
    }

    // Add a component to this GameObject
    public AddComponent(component: AbstractComponent): void
    {
        this.components.push(component);
        component.GameObject = this;
        this.CacheComponents();
    }

    // Get a component by its type
    public GetComponent<T extends AbstractComponent>(type: Function): T
    {
        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i] instanceof type)
            {
                return <T>this.components[i];
            }
        }

        return null;
    }

    // Get all components of the provided type
    public GetComponents<T extends AbstractComponent>(type: Function): T[]
    {
        var found = new Array<T>();

        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i] instanceof type)
            {
                found.push(<T>this.components[i]);
            }
        }

        return found;
    }

    // Determine if this GameObject has a component of the provided type
    public HasComponent(type: Function): boolean
    {
        return this.GetComponent<any>(type) != null;
    }

    // Remove any components of the provided type
    public RemoveComponentsOfType<T extends AbstractComponent>(zone: Zone, type: Function): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i] instanceof type)
            {
                this.components[i].OnRemoved(zone);
                this.components.splice(i, 1);
            }
        }

        this.CacheComponents();
    }

    // Remove a specific component
    public RemoveComponent(zone: Zone, component: AbstractComponent): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i] == component)
            {
                this.components[i].OnRemoved(zone);
                this.components.splice(i, 1);
            }
        }

        this.CacheComponents();
    }

    // Perform a simulation update
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        this.Zone = zone;

        for (var i = 0; i < this.components.length; i++)
        {
            this.components[i].Update(zone, state, elapsedSeconds);
        }
    }

    // Perform a late update, only done on the active zone after the camera has moved
    public LateUpdate(zone: Zone, state: FlightState): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            this.components[i].LateUpdate(zone, state);
        }
    }

    // Called when the GameObject is removed from its zone
    public OnRemoved(zone: Zone): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            this.components[i].OnRemoved(zone);
        }
    }

    // Called when this GameObject has collided with another
    public OnCollided(zone: Zone, state: FlightState, other: GameObject): void
    {
        if (other != this)
        {
            for (var i = 0; i < this.components.length; i++)
            {
                this.components[i].OnCollided(zone, state, other);
            }
        }
    }

    // Broadcast a message to all components on this GameObject
    public BroadcastMessage(zone: Zone, message: string): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            this.components[i].OnMessage(zone, message);
        }
    }

    // Cache commonly used components for streamlined access
    private CacheComponents(): void
    {
        this.PhysicsBody = this.GetComponent<PhysicsBody>(PhysicsBody);
    }

    // Force unintialize an object, useful for temporarily removing an object from a zone
    public ForceUninitialize(): void
    {
        for (var i = 0; i < this.components.length; i++)
        {
            this.components[i].ForceUninitialize();
        }
    }

    // Check for the presence of a control override component
    public HasControlOverride(): boolean
    {
        for (var i = 0; i < this.components.length; i++)
        {
            if (this.components[i].IsControlOverride)
            {
                return true;
            }
        }

        return false;
    }
}