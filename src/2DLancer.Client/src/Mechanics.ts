﻿// Utility class for game mechanics calculations
class Mechanics
{
    // Resolve an attack, a laser shot or a projectile hit
    public static ResolveAttack(
        zone: Zone,
        impactPoint: Vector2,
        target: GameObject,
        damage: number,
        largeBlast: boolean): void
    {
        if (impactPoint != null)
        {
            var blastSpriteName = "small_blast";

            if (largeBlast)
            {
                blastSpriteName = "large_blast";
            }

            var blast = new GameObject(impactPoint, 0);
            blast.AddComponent(new TimedLife(0.5, false));
            blast.AddComponent(new GrowingAndFadingSprite(
                blastSpriteName, FlightState.LAYER_MIDGROUND, HexColor.WHITE, 0.5,
                0, 2, // scale
                1, 0)); // alpha
            blast.AddComponent(new OffsetFollow(target));
            blast.AddComponent(new SoundEffect("little_burst", 0.5));
            zone.GameObjects.push(blast);
        }

        var shieldGenerator = target.GetComponent<ShieldGenerator>(ShieldGenerator);
        var hullHealth = target.GetComponent<HullHealth>(HullHealth);

        if (shieldGenerator != null && shieldGenerator.ShieldsUp())
        {
            shieldGenerator.DealDamage(damage);
        }
        else
        {
            if (hullHealth != null)
            {
                hullHealth.DealDamage(damage);
            }
        }
    }

    // Determine if a GameObject should be hostile to another game object
    public static IsHostile(self: GameObject, other: GameObject): boolean
    {
        var ownFaction = self.GetComponent<FactionIdentifier>(FactionIdentifier);
        var otherFaction = other.GetComponent<FactionIdentifier>(FactionIdentifier);

        return ownFaction == null
            || otherFaction == null
            || otherFaction.FactionId != ownFaction.FactionId;
    }

    // Calculate a lead position for shooting at a target
    public static LeadTarget(shooter: GameObject, target: GameObject, projectileSpeed: number): Vector2
    {
        var shooterToTarget = target.Position.Subtract(shooter.Position);

        //var relativeVelocity = target.PhysicsBody.Velocity
        //    .Subtract(shooter.PhysicsBody.Velocity);

        //var closingVelocity = relativeVelocity.Dot(shooterToTarget.Normalize());
        
        var projectileFlightTime = shooterToTarget.Magnitude() / (projectileSpeed);// + closingVelocity);

        //console.log(closingVelocity);

        //relativeVelocity.Add(Vector2.FromPolar(closingVelocity, shooterToTarget.Direction()));
        
        return target.Position
            .Add(target.PhysicsBody.Velocity.Scale(projectileFlightTime));
    }

    // Check for and apply player "tag" if they hit a ship
    public static CheckForPlayerTag(state: FlightState, shooter: GameObject, target: GameObject): void
    {
        if (shooter == state.PlayerShip
            && target != state.PlayerShip
            && Mechanics.IsHostile(shooter, target))
        {
            state.PlayerDataService.TagIfNeeded(target);
        }
    }
}