﻿/// <reference path="AbstractComponent.ts" />

// SpaceStationBehavior handles the economic and military behavior of a space station. Heavily 
// used by the background simulation.
class SpaceStationBehavior extends AbstractComponent
{
    public Name: string;

    // Set during BackgroundSimulation initialization
    public NearestWaypoint: Waypoint;

    constructor(name: string)
    {
        super();

        this.Name = name;
    }
}