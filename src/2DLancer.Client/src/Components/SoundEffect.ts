﻿/// <reference path="AbstractComponent.ts" />

// Plays audio files.
class SoundEffect extends AbstractComponent
{
    private sound: Phaser.Sound;
    private soundName: string;
    private currentBaseVolume: number;
    private targetBaseVolume: number;
    private loop: boolean;
    private distanceMod: number;

    public AutoPlay: boolean;

    constructor(soundName: string,
        volume?: number,
        loop?: boolean,
        distanceMod?: number)
    {
        super();

        this.soundName = soundName;
        this.AutoPlay = true;

        if (volume != null)
        {
            this.currentBaseVolume = volume;
        }
        else
        {
            this.currentBaseVolume = 1;
        }

        this.targetBaseVolume = this.currentBaseVolume;

        // convert possible null into boolean
        this.loop = !!loop;

        if (distanceMod != null)
        {
            this.distanceMod = distanceMod;
        }
        else
        {
            this.distanceMod = 1;
        }
    }

    // Create the sound object
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.sound = state.game.add.sound(this.soundName, this.currentBaseVolume, this.loop);

        if (this.AutoPlay)
        {
            this.sound.play();
        }
    }

    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        var volumeDelta = this.targetBaseVolume - this.currentBaseVolume;
        this.currentBaseVolume += Util.Clamp(volumeDelta, elapsedSeconds, -elapsedSeconds);

        var distToPlayer = this.GameObject.Position.DistanceTo(
            Vector2.FromPoint(state.camera.position)
                .Add(new Vector2(state.camera.width / 2, state.camera.height / 2)));
        var screenRadius = Math.max(state.camera.width, state.camera.height) / 2;

        var distanceFactor = Util.Clamp(
            1 - distToPlayer * this.distanceMod / screenRadius + 0.2,
            1,
            0);

        this.sound.volume = this.currentBaseVolume * distanceFactor;
    }

    // Destroy the sound
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.sound != null)
        {
            this.sound.destroy();
            this.sound = null;
        }
    }
    
    public FadeToVolume(newBaseVolume: number): void
    {
        this.targetBaseVolume = newBaseVolume;
    }

    public PlayOnce(): void
    {
        if (this.sound != null)
        {
            this.sound.play();
        }
    }
}