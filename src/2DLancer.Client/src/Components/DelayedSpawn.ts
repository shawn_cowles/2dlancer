﻿/// <reference path="AbstractComponent.ts" />

// DelayedSpawn adds another GameObject to the world after a delay.
class DelayedSpawn extends AbstractComponent
{
    private delay: number;
    private objectToAdd: GameObject;
    private spawned: boolean;
    private spawnVelocity: number;

    constructor(delay: number, objectToAdd: GameObject, velocity?: number)
    {
        super();

        this.delay = delay;
        this.objectToAdd = objectToAdd;
        this.spawned = false;
        this.spawnVelocity = velocity;

        if (this.spawnVelocity == null)
        {
            this.spawnVelocity = 0;
        }
    }
    
    // Spawn the object when the timer reaches 0
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.spawned)
        {
            return;
        }
        
        this.delay -= elapsedSeconds;

        if (this.delay <= 0)
        {
            this.spawned = true;
            this.objectToAdd.Position = this.GameObject.Position;
            if (this.objectToAdd.PhysicsBody != null && this.GameObject.PhysicsBody != null)
            {
                this.objectToAdd.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity;
            }
            this.objectToAdd.Facing = this.GameObject.Facing;
            zone.GameObjects.push(this.objectToAdd);

            if (this.objectToAdd.PhysicsBody != null)
            {
                this.objectToAdd.PhysicsBody.Velocity = Vector2.FromPolar(this.spawnVelocity, this.GameObject.Facing)
            }
        }
    }
}