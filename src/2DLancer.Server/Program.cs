﻿using System;
using System.Collections.Generic;
using _2DLancer.Server.Interfaces;
using _2DLancer.Server.Services;
using Autofac;

namespace _2DLancer.Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var container = BuildIoCContainer();
            
            var logger = container.Resolve<ILoggerProvider>().GetLoggerFor(typeof(Program));
            
            try
            {
                logger.Info("Beginning Server Startup");

                foreach (var initializable in container.Resolve<IEnumerable<IInitializableService>>())
                {
                    initializable.Initialize();
                }
                
#if DEBUG
                var service = container.Resolve<Service>();
                
                service.DebugStart();


                logger.Info("2DLancer Server Started");

                while (Console.ReadKey().KeyChar != 'q')
                {
                    Console.WriteLine();
                    continue;
                }

                service.DebugStop();
#else
                System.ServiceProcess.ServiceBase.Run(container.Resolve<Service>());
#endif
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private static IContainer BuildIoCContainer()
        {
            var builder = new ContainerBuilder();

            // Singleton Services
            builder.RegisterType<AppConfigSettingsProvider>()
                .AsImplementedInterfaces()
                .SingleInstance();
            builder.RegisterType<WebSocketConnectionService>()
                .AsImplementedInterfaces()
                .SingleInstance();
            builder.RegisterType<Log4NetLoggerProvider>()
                .AsImplementedInterfaces()
                .SingleInstance();
            builder.RegisterType<FileSystemAnalyticsLogger>()
                .AsImplementedInterfaces()
                .SingleInstance();

            // Basic Services
            builder.RegisterType<AnalyticsHandler>()
                .AsImplementedInterfaces();
            
            //The Service Itself
            builder.RegisterType<Service>().As<Service>();

            return builder.Build();
        }
    }
}
