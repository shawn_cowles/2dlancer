﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A background object, like a planet or a moon.
    /// </summary>
    [TsClass(Module ="", Name = "IBackgroundObject")]
    public class BackgroundObject
    {
        /// <summary>
        /// The position of the object.
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// The type of the object.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The parallax scroll factor of the object.
        /// </summary>
        public double ParallaxFactor { get; set; }
    }
}
