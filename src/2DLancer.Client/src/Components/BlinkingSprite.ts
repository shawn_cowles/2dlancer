﻿/// <reference path="SpriteDisplay.ts" />

// A component that causes a sprite to blink on and off over time.
class BlinkingSprite extends SpriteDisplay
{
    private blinkOnTime: number;
    private blinkOffTime: number;

    private blinkTimer: number;

    constructor(spriteName: string, spriteLayer: number, tint: number, scale: number,
        blinkOnTime: number, blinkOffTime: number)
    {
        super(spriteName, spriteLayer, tint, scale);

        this.blinkOnTime = blinkOnTime;
        this.blinkOffTime = blinkOffTime;
        this.blinkTimer = blinkOnTime;
    }

    // Initialize the component for active sim mode.
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.blinkTimer = 0;
    }
    
    // Update timers and visibility
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.blinkTimer -= elapsedSeconds;

        if (this.blinkTimer <= 0)
        {
            this.sprite.visible = !this.sprite.visible;

            if (this.sprite.visible)
            {
                this.blinkTimer = this.blinkOnTime;
            }
            else
            {
                this.blinkTimer = this.blinkOffTime;
            }
        }
    }
}