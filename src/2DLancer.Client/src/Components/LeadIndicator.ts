﻿/// <reference path="AbstractComponent.ts" />

// Creates a lead indicator in front of the GameObject, positioned for the player to shoot at
// and colored by relation to the player.
class LeadIndicator extends AbstractComponent
{
    private indicatorSprite: Phaser.Sprite;

    constructor()
    {
        super();
    }

    // Create sprites and setup
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.indicatorSprite = state.SpriteGroups[FlightState.LAYER_OVERLAY]
            .create(0, 0, "lead_indicator");
        this.indicatorSprite.tint = HexColor.RED;
        this.indicatorSprite.anchor.set(0.5, 0.5);
    }

    // Clean up sprites
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.indicatorSprite != null)
        {
            this.indicatorSprite.destroy();
            this.indicatorSprite = null;
        }
    }

    // Update position of the mouseover
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (state.PlayerShip == null
            || state.PlayerShip.ShouldBeRemoved
            || this.GameObject == state.PlayerShip
            || !Mechanics.IsHostile(state.PlayerShip, this.GameObject))
        {
            this.indicatorSprite.visible = false;
            return;
        }
        
        this.indicatorSprite.visible = true;

        var inputSource = state.PlayerShip.GetComponent<InputSource>(InputSource);

        // lead the target if the player has guns
        if (inputSource.ProjectileFlightSpeed != null)
        {
            var leadPoint = Mechanics.LeadTarget(
                state.PlayerShip,
                this.GameObject,
                inputSource.ProjectileFlightSpeed);

            this.indicatorSprite.position.set(
                leadPoint.X,
                leadPoint.Y);

            state.OverlayGraphics.lineStyle(3, HexColor.RED, 0.5);

            state.OverlayGraphics.moveTo(
                this.GameObject.Position.X,
                this.GameObject.Position.Y);
            
            var selfToIndicator = leadPoint.Subtract(this.GameObject.Position);
            var totalDist = Math.max(1, selfToIndicator.Magnitude());
            var lineOn = false;
            
            for (var i = 0; i < totalDist; i += 20)
            {
                var t = this.GameObject.Position
                    .Add(selfToIndicator.Scale(i / totalDist))

                if (lineOn)
                {
                    state.OverlayGraphics.lineTo(t.X, t.Y);
                }
                else
                {
                    state.OverlayGraphics.moveTo(t.X, t.Y);
                }

                lineOn = !lineOn;
            }
        }
        else
        {
            this.indicatorSprite.position.set(
                this.GameObject.Position.X,
                this.GameObject.Position.Y);
        }
    }
}