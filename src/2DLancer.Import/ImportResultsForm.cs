﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using _2DLancer.Import.Data;
using _2DLancer.Import.Workers;

namespace _2DLancer.Import
{
    /// <summary>
    /// A GUI Form for running import and validation workers and displaying the results.
    /// </summary>
    public partial class ImportResultsForm : Form
    {
        delegate void SetStatusCallback(string status, Color color);

        delegate void AddWorkResultCallback(WorkResult result);

        private Dictionary<string, string> _resultDetails;

        /// <summary>
        /// Construct a new ImportResultsForm.
        /// </summary>
        /// <param name="importPath">The source file for the import.</param>
        /// <param name="outputPath">The destination path for the import.</param>
        public ImportResultsForm(string importPath, string outputPath)
        {
            InitializeComponent();

            _resultDetails = new Dictionary<string, string>();

            SetStatus("Initializing...", Color.Black);
            
            ImportAndValidate(importPath, outputPath);
        }

        private async void ImportAndValidate(string importPath, string outputPath)
        {
            var workers = new List<AbstractWorker>();

            workers.Add(new ZoneCreationWorker());
            workers.Add(new ZoneCelestialImporter());
            workers.Add(new ZoneBackgroundObjectImporter());
            workers.Add(new ZoneSpawnerImporter());
            workers.Add(new ZoneNavPointImporter());
            workers.Add(new ZoneSpaceStationImporter());
            workers.Add(new NavMeshBuilder());
            workers.Add(new ShipDataImporter());
            workers.Add(new ShipDetailsImporter());
            

            workers.Add(new DataExportWorker());
            
            SetStatus("Importing...", Color.Black);

            var results = new List<WorkResult>();

            var context = new ImportContext(importPath, outputPath);

            foreach (var worker in workers)
            {
                var result = await Task.Run(() => worker.DoWork(context));

                results.Add(result);
                AddWorkResult(result);

                if(!result.Success)
                {
                    break;
                }
            }

            if (results.All(r => r.Success))
            {
                SetStatus("Import Successful", Color.Green);
            }
            else
            {
                SetStatus("Import Failed!", Color.Red);
            }

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void SetStatus(string status, Color color)
        {
            if (statusLabel.InvokeRequired)
            {
                Invoke(new SetStatusCallback(SetStatus), new object[] { status, color });
            }
            else
            {
                statusLabel.Text = status;
                statusLabel.ForeColor = color;
            }
        }

        private void AddWorkResult(WorkResult result)
        {
            if (resultsList.InvokeRequired)
            {
                Invoke(new AddWorkResultCallback(AddWorkResult), new object[] { result });
            }
            else
            {
                resultsList.Items.Add(result);
            }
        }

        private void resultsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = resultsList.SelectedItem as WorkResult;

            if (item != null)
            {
                resultsDetailArea.Text = item.Message.Replace("\n", "\r\n");
            }
            else
            {
                resultsDetailArea.Text = "";
            }
        }
    }
}
