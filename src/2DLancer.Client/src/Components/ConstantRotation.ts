﻿/// <reference path="AbstractComponent.ts" />

// ConstantRotation constantly rotates a GameObject, useful for rotating stations.
class ConstantRotation extends AbstractComponent
{
    private rpm: number;
    
    constructor(rpm: number)
    {
        super();

        this.rpm = rpm;
        
    }

    // Rotate the GameObject
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.GameObject.Facing += this.rpm / 60 * elapsedSeconds;
    }
}