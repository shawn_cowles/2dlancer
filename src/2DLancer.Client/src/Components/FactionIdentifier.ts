﻿/// <reference path="AbstractComponent.ts" />

// Identifies a GameObject as being part of a faction.
class FactionIdentifier extends AbstractComponent
{
    public FactionId: string;

    constructor(factionId: string)
    {
        super();

        this.FactionId = factionId;
    }
}