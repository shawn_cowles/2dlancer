﻿/// <reference path="AbstractComponent.ts" />

// HomingProjectileBehavior tracks in on enemy ships.
class HomingProjectileBehavior extends AbstractComponent
{
    private stopWhenIdle: boolean;
    private inputSource: InputSource;
    private target: GameObject;
    private lockRange: number;

    constructor(lockRange: number, stopWhenIdle: boolean)
    {
        super();

        this.lockRange = lockRange;
        this.stopWhenIdle = stopWhenIdle;
        this.target = null;
    }
    
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
    }
    
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.target == null || this.target.ShouldBeRemoved)
        {
            this.FindTarget(zone.GameObjects);
        }

        if (this.target != null)
        {
            this.inputSource.DesiredFacing = this.GameObject.Position.DirectionTo(this.target.Position);
            this.inputSource.ThrustFoward = true;
        }
        else
        {
            this.inputSource.DesiredFacing = this.GameObject.Facing;

            if (this.stopWhenIdle)
            {
                this.inputSource.ThrustFoward = false;
            }
            else
            {
                this.inputSource.ThrustFoward = true;
            }
        }
    }

    private FindTarget(potentials: GameObject[]): void
    {
        var nearDist = <number>null;
        this.target = null;

        for (var i = 0; i < potentials.length; i++)
        {
            // only consider objects that can be damaged
            if (potentials[i].HasComponent(HullHealth))
            {
                // only target hostiles
                if (Mechanics.IsHostile(this.GameObject, potentials[i]))
                {
                    var dist = this.GameObject.Position.DistanceTo(potentials[i].Position);

                    if (dist < this.lockRange && (nearDist == null || dist < nearDist))
                    {
                        nearDist = dist;
                        this.target = potentials[i];
                    }
                }
            }
        }
    }

}