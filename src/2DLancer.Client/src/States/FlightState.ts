﻿/// <reference path="AbstractState.ts" />

// The state for all of the actual flying around
class FlightState extends AbstractState
{
    public static NAME = "state_flight";
    public static LAYER_DEEP_BACKGROUND = 0;
    public static LAYER_BACKGROUND = 1;
    public static LAYER_MIDGROUND = 2;
    public static LAYER_FOREGROUND = 3;
    public static LAYER_OVERLAY = 4;

    private serverConnection: ServerConnectionService;
    private collisionSystem: CollisionSystem;
    public PlayerDataService: PlayerDataService;
    public Navigation: NavigationSystem;
    public BackgroundSim: BackgroundSimulation;
    public Analytics: AnalyticsService;
    public ShipBuilder: ShipBuilder;
    public MessageLog: MessageLog;

    public SpriteGroups: Phaser.Group[];
    public Graphics: Phaser.Graphics;
    public OverlayGraphics: Phaser.Graphics;

    private activeZoneIndex: number;
    public Zones: Zone[];
    
    public Widgets: AbstractUiWidget[];

    public PlayerSpawner: PlayerSpawner;
    public PlayerShip: GameObject;

    create()
    {
        // Phaser Setup
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.renderer.renderSession.roundPixels = false;
        this.game.add.sound("music", .25, true).play();

        // Setup Sprite Groups
        this.SpriteGroups = new Array<Phaser.Group>();
        this.SpriteGroups.push(this.add.group()); //Deep Background
        this.SpriteGroups.push(this.add.group()); //Background
        this.Graphics = this.add.graphics(0, 0);
        this.SpriteGroups.push(this.add.group()); //Midground
        this.SpriteGroups.push(this.add.group()); //Foreground
        this.OverlayGraphics = this.add.graphics(0, 0);
        this.SpriteGroups.push(this.add.group()); //Overlay
        
        // Build Services
        this.serverConnection = new ServerConnectionService();
        this.collisionSystem = new CollisionSystem();
        this.Navigation = new NavigationSystem();
        this.BackgroundSim = new BackgroundSimulation();
        this.Analytics = new AnalyticsService(this.serverConnection);
        this.PlayerDataService = new PlayerDataService();
        this.ShipBuilder = new ShipBuilder();
        this.MessageLog = new MessageLog();
        
        // Build UI
        this.Widgets = [];
        this.SetupFlightUI();

        // Build the universe
        var zoneBuilder = new ZoneBuilder(this.game);
        this.Zones = zoneBuilder.BuildZones();
        
        // Initialize services
        this.PlayerDataService.Initialize(this);
        this.Navigation.Initialize(this);
        this.BackgroundSim.Initialize(this);
        this.Analytics.Initialize(this);
        this.ShipBuilder.Initalize(this);
        this.MessageLog.Initialize(this);

        for (var i = 0; i < this.BackgroundSim.Actors.length; i++) // Find the player spawner
        {
            if (this.BackgroundSim.Actors[i] instanceof PlayerSpawner)
            {
                this.PlayerSpawner = <PlayerSpawner>this.BackgroundSim.Actors[i];
                this.SetActiveZone(this.PlayerSpawner.Zone.Name);
                this.camera.focusOnXY(
                    this.PlayerSpawner.Position.X,
                    this.PlayerSpawner.Position.Y);
            }
        }
    }
    
    update()
    {
        this.Graphics.clear();
        this.OverlayGraphics.clear();

        var elapsedSeconds = this.LancerGame.time.elapsed / 1000;
        
        // if elapsed seconds is very large then we can assume the game has been paused for awhile, suppress an actual update
        if (elapsedSeconds > 1)
        {
            return;
        }

        this.HandlePlayerInput(elapsedSeconds);

        // Active Zone Update
        this.Zones[this.activeZoneIndex].Update(this);
        this.collisionSystem.ProcessCollisions(this.Zones[this.activeZoneIndex], this);
        
        
        if (this.PlayerShip != null)
        {
            this.CenterCameraOn(this.PlayerShip);
        }

        this.Zones[this.activeZoneIndex].LateUpdate(this);

        // UI Updates
        for (var i = 0; i < this.Widgets.length; i++)
        {
            this.Widgets[i].Update(this, elapsedSeconds);
        }

        this.PlayerDataService.Update(this);
        this.serverConnection.Update();
        this.MessageLog.Update(elapsedSeconds);
        this.BackgroundSim.Update(this, elapsedSeconds);
    }

    private CenterCameraOn(target: GameObject): void
    {
        this.camera.focusOnXY(
            target.Position.X,
            target.Position.Y);
    }

    private HandlePlayerInput(elapsedSeconds: number): void
    {
        var pointerPos = new Vector2(
            this.input.activePointer.worldX,
            this.input.activePointer.worldY);

        var mouseInUi = false;

        for (var i = 0; i < this.Widgets.length; i++)
        {
            if (this.Widgets[i].PointIsInPanel(pointerPos))
            {
                mouseInUi = true;
            }
        }

        if (this.PlayerShip != null)
        {
            var playerInput = this.PlayerShip.GetComponent<InputSource>(InputSource);

            if (playerInput != null)
            {
                if (this.PlayerShip.HasControlOverride())
                {
                    return;
                }

                playerInput.DesiredFacing = Util.CorrectAngle(this.PlayerShip.Position.DirectionTo(pointerPos));
                playerInput.TargetPoint = pointerPos;

                // !! converts values to boolean, because IsDown sometimes returns null, because javascript
                playerInput.ThrustFoward = !!this.input.keyboard.isDown(Phaser.KeyCode.W);
                playerInput.ThrustAft = !!this.input.keyboard.isDown(Phaser.KeyCode.S);
                playerInput.ThrustPort = !!this.input.keyboard.isDown(Phaser.KeyCode.A);
                playerInput.ThrustStarboard = !!this.input.keyboard.isDown(Phaser.KeyCode.D);

                playerInput.DeactivateAllModules();

                if (!mouseInUi)
                {
                    if (this.input.activePointer.leftButton.isDown)
                    {
                        playerInput.ActivatingModules[_2DLancer.Common.Data.ModuleType.PrimaryWeapon]
                            = true;
                    }

                    if (this.input.activePointer.rightButton.isDown)
                    {
                        playerInput.ActivatingModules[_2DLancer.Common.Data.ModuleType.SecondaryWeapon]
                            = true;
                    }
                }

                if (this.input.keyboard.isDown(Phaser.KeyCode.Q))
                {
                    playerInput.ActivatingModules[_2DLancer.Common.Data.ModuleType.Bay]
                        = true;
                }

                if (this.input.keyboard.isDown(Phaser.KeyCode.E))
                {
                    playerInput.ActivatingModules[_2DLancer.Common.Data.ModuleType.Booster]
                        = true;
                }
            }
        }
    }
    
    // Set the active zone (i.e. where the player is)
    public SetActiveZone(zoneName: string): void
    {
        if (this.activeZoneIndex != null)
        {
            // set current zone to summary mode
            this.Zones[this.activeZoneIndex].CleanForBackground();
            this.activeZoneIndex = null;
        }

        for (var i = 0; i < this.Zones.length; i++)
        {
            if (this.Zones[i].Name == zoneName)
            {
                this.activeZoneIndex = i;
                this.Zones[i].BuildForActive(this);
                this.world.setBounds(0, 0, this.Zones[i].Width, this.Zones[i].Height);
                this.Analytics.ZoneVisited(this.Zones[i].Name);
            }
        }
        
        if (this.activeZoneIndex == null)
        {
            throw "Unknown zone name: " + zoneName;
        }
    }

    // Get the active zone (i.e. where the player is)
    public GetActiveZone(): Zone
    {
        if (this.activeZoneIndex != null)
        {
            return this.Zones[this.activeZoneIndex];
        }

        return null;
    }

    // Set the active zone (i.e. where the player is)
    public GetZone(zoneName: string): Zone
    {
        for (var i = 0; i < this.Zones.length; i++)
        {
            if (this.Zones[i].Name == zoneName)
            {
                return this.Zones[i];
            }
        }

        throw "Unknown zone name: " + zoneName;
    }

    public SetupFlightUI(): void
    {
        for (var i = 0; i < this.Widgets.length; i++)
        {
            this.Widgets[i].Destroy();
        }

        this.Widgets = [];
        this.Widgets.push(new CurrentLocationLabel());
        this.Widgets.push(new MiniMap(this, "blue"));
        this.Widgets.push(new ShipStatusPanel());
        this.Widgets.push(new PlayerStatusPanel());
        this.Widgets.push(new MessageLogDisplay());
        this.Widgets.push(new NavBouyPanel());
        this.Widgets.push(new JumpChargingPanel());
        this.Widgets.push(new SpaceStationDockPanel());
        //this.Widgets.push(new NavMeshDebugDisplay());
    }

    public SetupDockedUI(station: SpaceStationBehavior): void
    {
        for (var i = 0; i < this.Widgets.length; i++)
        {
            this.Widgets[i].Destroy();
        }

        this.Widgets = [];
        this.Widgets.push(new CurrentLocationLabel());
        this.Widgets.push(new MiniMap(this, "blue"));
        this.Widgets.push(new SpaceStationInteractionPanel(station));
        this.Widgets.push(new PlayerStatusPanel());
        this.Widgets.push(new MessageLogDisplay());
    }
}