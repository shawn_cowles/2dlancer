﻿/// <reference path="AbstractComponent.ts" />

// ActorLink links a GameObject to an Actor in the background simulation.
class ActorLink extends AbstractComponent
{
    public Actor: Ship;

    private health: HullHealth;

    constructor(actor: Ship)
    {
        super();

        this.Actor = actor;
    }
    
    protected Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.health = this.GameObject.GetComponent<HullHealth>(HullHealth);
    }

    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.UpdateActor();
    }

    public OnRemoved(zone: Zone): void
    {
        super.OnRemoved(zone);

        this.UpdateActor();
    }

    private UpdateActor(): void
    {
        if (this.GameObject.ShouldBeRemoved)
        {
            this.Actor.IsDead = true;
        }

        this.Actor.Position = this.GameObject.Position;
        this.Actor.Health = this.health.GetHealth();
    }
}