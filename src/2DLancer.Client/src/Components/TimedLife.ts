﻿/// <reference path="AbstractComponent.ts" />

// This component will flag its parent for removal after a set time period. Useful for transitory objects like bullets or explosions.
class TimedLife extends AbstractComponent
{
    private timeToLive: number;
    private destroyOnTimeout: boolean;

    constructor(timeToLive: number, destroyOnTimeout: boolean)
    {
        super();

        this.timeToLive = timeToLive;
        this.destroyOnTimeout = destroyOnTimeout;
    }

    // count down time to live
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.timeToLive -= elapsedSeconds;

        if (this.timeToLive < 0)
        {
            this.GameObject.ShouldBeRemoved = true;

            if (this.destroyOnTimeout)
            {
                this.GameObject.BroadcastMessage(zone, Messages.DESTROYED);
            }
        }
    }
}