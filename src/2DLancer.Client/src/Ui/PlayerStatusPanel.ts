﻿/// <reference path="UiPanel.ts" />

// A widget that displays ship status information.
class PlayerStatusPanel extends UiPanel
{
    public static WIDTH = 180;
    public static HEIGHT = 25;

    private state: FlightState;

    private moneyText: Phaser.Text;

    constructor()
    {
        super(new Phaser.Point(0, 0), PlayerStatusPanel.WIDTH, PlayerStatusPanel.HEIGHT);
    }
    
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.moneyText = state.game.add.text(0, 0, "", textStyle);
        this.moneyText.anchor.set(0.5, 0.5);
    }
    
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        var xPosition = state.game.camera.width - PlayerStatusPanel.WIDTH / 2 + state.game.camera.view.x - 10;
        var yPosition = state.game.camera.view.y
            + CurrentLocationLabel.HEIGHT
            + MiniMap.MINIMAP_SIZE
            + PlayerStatusPanel.HEIGHT / 2 + 15;

        this.CenterPosition = new Phaser.Point(xPosition, yPosition);

        // Call update after we find the center, so UI elements are arrayed correctly
        super.Update(state, elapsedSeconds);

        this.moneyText.position.set(
            xPosition,
            yPosition);

        this.moneyText.text = Util.FormatAsMoney(state.PlayerDataService.GetBalance());
    }

    public Destroy()
    {
        super.Destroy();

        this.moneyText.destroy();
    }
}