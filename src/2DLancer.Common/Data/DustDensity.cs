﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// An enum of possible dust densities in zones.
    /// </summary>
    [TsEnum(Module ="", Name = "DustDensity")]
    public enum DustDensity
    {
        /// <summary>
        /// No dust.
        /// </summary>
        None,

        /// <summary>
        /// Only light foreground dust
        /// </summary>
        Low,

        /// <summary>
        /// Light foreground dust, and light background dust
        /// </summary>
        Medium,

        /// <summary>
        /// Light foreground dust and heavy background dust
        /// </summary>
        High,

        /// <summary>
        /// Heavy foreground dust and heavy background dust
        /// </summary>
        VeryHigh
    }
}
