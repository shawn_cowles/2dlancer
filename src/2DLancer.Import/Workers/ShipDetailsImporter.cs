﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports sumplementaty details about ships from xml files.
    /// </summary>
    public class ShipDetailsImporter : AbstractWorker
    {
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Ship Details Importer"; }
        }

        /// <summary>
        /// Import ships.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var shipElements = LoadShipElements(importContext.ShipPath);

                AssertShipDesignAndDetails(importContext, shipElements);
                
                foreach(var name in shipElements.Keys)
                {
                    ImportShipData(
                        shipElements[name],
                        importContext.Ships.First(s => s.Name == name));
                }
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }

            return new WorkResult(Name, true, string.Empty);
        }

        private Dictionary<string, XElement> LoadShipElements(string shipPath)
        {
            var shipElements = new Dictionary<string, XElement>();

            var xmlFiles = Directory.EnumerateFiles(shipPath)
                .Where(f => Path.GetExtension(f) == ".xml")
                .ToArray();

            foreach (var xmlFile in xmlFiles)
            {
                try
                {
                    var doc = XDocument.Load(xmlFile);

                    foreach (var shipElement in doc.Descendants(XmlNames.SHIP_DETAILS_SHIP_ELEMENT))
                    {
                        var shipName = GetAttribute(shipElement, XmlNames.SHIP_DETAILS_SHIP_NAME);

                        if (shipElements.ContainsKey(shipName))
                        {
                            throw new ImportException($"Ship details element for {shipName} has been found in multiple files.");
                        }

                        shipElements.Add(
                            shipName,
                            shipElement);
                    }
                }
                catch (Exception ex)
                {
                    throw new ImportException($"Failed to parse xml file '{xmlFile}'", ex);
                }
            }

            return shipElements;
        }

        private void AssertShipDesignAndDetails(ImportContext importContext, Dictionary<string, XElement> shipElements)
        {
            foreach (var name in shipElements.Keys)
            {
                if (!importContext.Ships.Any(s => s.Name == name))
                {
                    throw new ImportException($"Found ship details for {name} with no corresponding design (tmx) file.");
                }
            }

            foreach (var ship in importContext.Ships)
            {
                if (!shipElements.ContainsKey(ship.Name))
                {
                    throw new ImportException($"Missing ship details for {ship.Name}.");
                }
            }
        }

        private void ImportShipData(XElement shipElement, ShipData shipData)
        {
            try
            {
                shipData.Description = GetAttribute(shipElement, XmlNames.SHIP_DETAILS_SHIP_DESCIPTION);
                shipData.Cost = GetDoubleAttribute(shipElement, XmlNames.SHIP_DETAILS_SHIP_COST);
            }
            catch (Exception ex)
            {
                throw new ImportException($"Failed reading ship details for '{shipData.Name}'", ex);
            }
        }
    }
}
