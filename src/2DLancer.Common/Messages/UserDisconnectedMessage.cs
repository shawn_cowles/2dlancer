﻿namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// This message is sent by the WebSocketConnectionService whenever a client disconnects.
    /// </summary>
    public class UserDisconnectedMessage : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "UserDisconnectedMessage";

        /// <summary>
        /// Construct a new UserDisconnectedMessage.
        /// </summary>
        public UserDisconnectedMessage(string sessionId)
            : base(NAME)
        {
            ClientId = sessionId;
        }
    }
}
