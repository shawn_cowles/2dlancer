﻿/// <reference path="Actor.ts" />

// Spawns and respawns the player.
class PlayerSpawner extends Actor
{
    public Color: number;
    public FactionId: string;
    public Facing: number;

    private spawnDelay: number;
    private spawnTimer: number;

    constructor(
        position: Vector2,
        zone: Zone,
        color: number,
        factionId: string,
        spawnDelay: number,
        facing: number)
    {
        super(position, zone);

        this.Color = color;
        this.FactionId = factionId;
        this.spawnDelay = spawnDelay;
        this.spawnTimer = 0;
        this.Facing = facing;
    }

    // If the player is dead, count to respawn
    public Update(state: FlightState, bgs: BackgroundSimulation, elapsedSeconds: number): void
    {
        if (state.PlayerShip == null || state.PlayerShip.ShouldBeRemoved)
        {
            this.spawnTimer -= elapsedSeconds;

            if (this.spawnTimer <= 0)
            {
                state.Analytics.PlayerSpawned();

                this.spawnTimer = this.spawnDelay;

                var loadout = state.PlayerDataService.GetCurrentLoadout();
                loadout.Color = this.Color;
                loadout.FactionId = this.FactionId;
                loadout.ShowSpawnEffect = false;

                state.PlayerShip = state.ShipBuilder.BuildShip(
                    this.Position,
                    this.Facing,
                    loadout,
                    null);
                // prevent the player from being cleaned up by zone changes
                state.PlayerShip.AddComponent(new StaticObjectIdentifier());

                this.Zone.GameObjects.push(state.PlayerShip);
                state.SetActiveZone(this.Zone.Name);
            }
        }
    }

    // No GameObject representation
    public BuildGameObject(state: FlightState): GameObject
    {
        return null;
    }
}