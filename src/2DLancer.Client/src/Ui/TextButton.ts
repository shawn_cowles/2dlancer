﻿class TextButton extends Phaser.Button
{
    private textObject: Phaser.Text;
    public Text: string;

    public position: Phaser.Point;

    constructor(game: Phaser.Game, xPos: number, yPos: number, text: string, callback: Function, callbackContext: any)
    {
        super(game, xPos, yPos, "text_button", callback, callbackContext, 1, 0, 1, 0);

        game.add.existing(this);
        this.Text = text;
        this.anchor.set(0.5, 0.5);
    }

    public update()
    {
        super.update();

        // delayed creation of text, to get layering right
        if (this.textObject == null)
        {
            var textStyle = { font: "16px courier", fill: "#00FFFF" };
            this.textObject = this.game.add.text(this.position.x, this.position.y, this.Text, textStyle);
            this.textObject.anchor.set(0.5, 0.5);
        }

        this.textObject.position = this.position;
        this.textObject.visible = this.visible;
        this.textObject.text = this.Text;
    }

    public destroy()
    {
        super.destroy();

        if (this.textObject)
        {
            this.textObject.destroy();
        }
    }
}