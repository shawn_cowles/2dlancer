﻿/// <reference path="AbstractComponent.ts" />

// Holds physics simulation information for a GameObject
class PhysicsBody extends AbstractComponent
{
    // The velocity of the GameObject
    public Velocity: Vector2;

    // The mass of the GameObject (in arbitrary units)
    public Mass: number;
    
    constructor(mass: number)
    {
        super();

        this.Mass = mass;
        this.Velocity = new Vector2(0, 0);
    }
    
    // Update position based on velocity, only simulate physics when active
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.GameObject.Position = this.GameObject.Position.Add(this.Velocity.Scale(elapsedSeconds));
    }
}