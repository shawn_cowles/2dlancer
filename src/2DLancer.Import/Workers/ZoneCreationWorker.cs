﻿using System;
using System.IO;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;
using TiledSharp;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Worker that creates a skeleton for all of the zones to import and imports all of the TMX maps.
    /// </summary>
    public class ZoneCreationWorker : AbstractWorker
    {
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Zone Creation Worker"; }
        }

        /// <summary>
        /// Read all loaded data from the context and write them to JSON files.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var zoneFiles = Directory.EnumerateFiles(importContext.MapPath)
                    .Where(f => Path.GetExtension(f) == ".tmx")
                    .ToArray();
                
                foreach (var zoneFile in zoneFiles)
                {
                    try
                    {
                        var map = new TmxMap(zoneFile);

                        var dustDensity = GetEnumProperty<DustDensity>(map, PropertyNames.ZONE_DUST_DENSITY);

                        var friendlyName = GetProperty(map, PropertyNames.ZONE_FRIENDLY_NAME);

                        var zone = new Zone(
                            Path.GetFileNameWithoutExtension(zoneFile),
                            friendlyName,
                            map.Width * map.TileWidth,
                            map.Height * map.TileHeight,
                            dustDensity);

                        importContext.ZoneMaps.Add(zone.Name, map);
                        importContext.Zones.Add(zone.Name, zone);
                    }
                    catch(Exception ex)
                    {
                        throw new ImportException($"Failed to import zone '{zoneFile}'", ex);
                    }
                }
                
                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }
    }
}
