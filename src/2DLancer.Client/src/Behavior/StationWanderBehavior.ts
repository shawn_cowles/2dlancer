﻿/// <reference path="AbstractAiBehavior.ts" />

// A behavior that randomly wanders between stations.
class StationWanderBehavior extends AbstractAiBehavior
{
    private WAYPOINT_PROX_RANGE = 300;
    
    private path: Waypoint[];

    constructor()
    {
        super();

        this.path = [];
    }

    // Initialize the behavior
    public Initialize(control: CompositeBotControl, zone: Zone): void
    {
    }

    // Perform an update, returns true if the behavior applies, false otherwise
    public Update(
        control: CompositeBotControl,
        state: FlightState,
        zone: Zone,
        inputSource: InputSource,
        elapsedSeconds: number): boolean
    {
        if (this.path.length < 1)
        {
            var waypoint = this.SelectDestination(state.BackgroundSim);

            this.path = state.Navigation.Path(
                control.GameObject.Position,
                zone,
                waypoint);
        }

        if (this.path[0].ZoneName != zone.Name && !control.GameObject.HasComponent(JumpSequencer))
        {
            control.GameObject.AddComponent(new JumpSequencer(
                this.path[0].ZoneName,
                this.path[0].Position));
        }
        
        var distToWaypoint = control.GameObject.Position.DistanceTo(this.path[0].Position);

        var bearingTo = control.GameObject.Position.BearingTo(control.GameObject.Facing, this.path[0].Position);

        inputSource.ThrustFoward = Math.abs(bearingTo) < 0.01;
        inputSource.DesiredFacing = control.GameObject.Position.DirectionTo(this.path[0].Position);
        inputSource.DeactivateAllModules();

        if (distToWaypoint < this.WAYPOINT_PROX_RANGE)
        {
            this.path.splice(0, 1);
        }

        return true;
    }

    public ControlShipActor(
        ship: Ship,
        state: FlightState,
        elapsedSeconds: number): boolean
    {
        if (this.path.length < 1)
        {
            var waypoint = this.SelectDestination(state.BackgroundSim);

            this.path = state.Navigation.Path(
                ship.Position,
                ship.Zone,
                waypoint);
        }

        if (this.path[0].ZoneName != ship.Zone.Name)
        {
            ship.TransitionToZone(this.path[0].Position, state.GetZone(this.path[0].ZoneName));
        }
        else
        {
            ship.MoveTo(this.path[0].Position, elapsedSeconds);

            if (ship.Position.DistanceTo(this.path[0].Position) < this.WAYPOINT_PROX_RANGE)
            {
                this.path.splice(0, 1);
            }
        }

        return true;
    }

    // Randomly pick a waypoint
    private SelectDestination(backgroundSim: BackgroundSimulation): Waypoint
    {
        var i = Math.floor(Math.random() * backgroundSim.Stations.length);

        return backgroundSim.Stations[i].NearestWaypoint;
    }
}