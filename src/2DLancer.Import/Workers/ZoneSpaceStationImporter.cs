﻿using System;
using System.Collections.Generic;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;
using TiledSharp;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports space stations.
    /// </summary>
    public class ZoneSpaceStationImporter : AbstractWorker
    {
        private static readonly string[] KNOWN_TYPES = new[] { "colony", "outpost" };

        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Space Station Importer"; }
        }
        
        /// <summary>
        /// Import space stations.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                foreach (var zoneName in importContext.Zones.Keys)
                {
                    var zone = importContext.Zones[zoneName];

                    foreach (var layer in importContext.ZoneMaps[zoneName].ObjectGroups)
                    {
                        foreach (var obj in layer.Objects.Where(o => o.Type == ObjectTypes.SPACE_STATION))
                        {
                            try
                            {
                                var type = GetProperty(obj, PropertyNames.SPACE_STATION_TYPE);

                                if(!KNOWN_TYPES.Contains(type))
                                {
                                    throw new ImportException($"Unknown space station type: '{type}'");
                                }

                                zone.SpaceStations.Add(new SpaceStation
                                {
                                    Position = new Position(obj.X + obj.Width / 2, obj.Y + obj.Height / 2),
                                    Name = obj.Name,
                                    Type = type
                                });
                            }
                            catch (Exception ex)
                            {
                                throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}' in zone '{zone.Name}'", ex);
                            }
                        }
                    }
                }
                
                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }
    }
}

