﻿namespace _2DLancer.Import.Data
{
    /// <summary>
    /// Hold the result of running a worker.
    /// </summary>
    public class WorkResult
    {
        /// <summary>
        /// Was the operation successful?
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// The name of the worker.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// A message explaining what happened if the operation failed.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Construct a new WorkResult.
        /// </summary>
        /// <param name="name">Was the operation successful?</param>
        /// <param name="success">The name of the worker.</param>
        /// <param name="message">A message explaining what happened if the operation failed.</param>
        public WorkResult(string name, bool success, string message)
        {
            Name = name;
            Success = success;
            Message = message;
        }

        /// <summary>
        /// A string representation of the WorkResult, contains name and success status.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Success)
            {
                return "Pass: " + Name;
            }

            return "FAILED: " + Name;
        }
    }
}
