﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages.Analytics
{
    /// <summary>
    /// An event for the player repairing ship damage at a station.
    /// </summary>
    [TsClass(Module = "", Name = "IShipRepairedEvent")]
    [DataContract]
    public class ShipRepairedEvent : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "ShipRepairedEvent";

        /// <summary>
        /// The name of the station that the player repaired at.
        /// </summary>
        [DataMember]
        public string StationName { get; set; }

        /// <summary>
        /// Construct a new ShipRepairedEvent.
        /// </summary>
        public ShipRepairedEvent() 
            : base(NAME)
        {
        }
    }
}
