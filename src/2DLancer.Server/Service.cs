﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using _2DLancer.Server.Interfaces;
using _2DLancer.Server.Services;

namespace _2DLancer.Server
{
    /// <summary>
    /// The main service that listens  
    /// </summary>
    public partial class Service : ServiceBase
    {
        private readonly ILogger _logger;
        private readonly IBusService[] _busServices;

        private MessageBus _messageBus;

        /// <summary>
        /// Construct a new Service.
        /// </summary>
        /// <param name="loggerProvider">The logger provider to use.</param>
        /// <param name="busServices">All of the services that will listen to the message bus.</param>
        public Service(
            ILoggerProvider loggerProvider,
            IEnumerable<IBusService> busServices)
        {
            _logger = loggerProvider.GetLoggerFor(typeof(Service));
            _busServices = busServices.ToArray();
            _messageBus = new MessageBus();
            
            InitializeComponent();
        }
        
        /// <summary>
        /// Called when the service starts.
        /// </summary>
        /// <param name="args">Service start arguments.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                base.OnStart(args);

                _logger.Info("SSG Service Starting");

                foreach(var service in _busServices)
                {
                    service.Start(_messageBus);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Called when the service stops.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                base.OnStop();

                _logger.Info("SSG Service Stopping");

                foreach (var service in _busServices)
                {
                    service.Stop();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Start the service when debugging.
        /// </summary>
        public void DebugStart()
        {
            OnStart(new string[0]);
        }

        /// <summary>
        /// Stop the service when debugging.
        /// </summary>
        public void DebugStop()
        {
            OnStop();
        }
    }
}
