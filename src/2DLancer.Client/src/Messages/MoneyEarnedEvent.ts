﻿class MoneyEarnedEvent implements IMoneyEarnedEvent
{
    public MessageName = "MoneyEarnedEvent";
    
    public Amount: number;
    public Category: string;
    public Item: string;

    constructor(category: string, item: string, amount: number)
    {
        this.Category = category;
        this.Item = item;
        this.Amount = amount;
    }
}