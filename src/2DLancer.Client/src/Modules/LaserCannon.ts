﻿/// <reference path="AbstractWeaponModule.ts" />

// The LaserCannon shoot pulsed lasers that directly hit targets, ignoring intervening objects.
class LaserCannon extends AbstractWeaponModule
{
    private static MAX_CHARGE_TIME = 1.5;

    private shotCooldown: number;
    private shotDamage: number;
    
    private nextBarrel: number;

    private targetDps: number;

    private chargeCounter: number;

    private chargeSprites: Phaser.Sprite[];
    private chargeSound: SoundEffect;

    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        super.Initialize(state, parentShip, slot, offsetMagnitudes, offsetDirections);

        var targetDps = Autocannon.DpsForModuleSize(slot.Size);
        this.shotDamage = 1.5;
        this.shotCooldown = 0.5;
        this.chargeCounter = 0;
        this.chargeSprites = null;
        this.targetDps = LaserCannon.DpsForModuleSize(slot.Size);

        this.offsetDirections = offsetDirections;
        this.offsetMagnitudes = offsetMagnitudes;
        this.nextBarrel = 0;
    }

    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        super.Update(zone, state, parentShip, elapsedSeconds, isActivating);

        // create sprites if needed
        if (this.chargeSprites == null)
        {
            this.chargeSprites = [];

            for (var i = 0; i < this.offsetDirections.length; i++)
            {
                var sprite = state.SpriteGroups[FlightState.LAYER_OVERLAY].create(0, 0, "laser_charge");
                sprite.anchor.set(0.5, 0.5);
                sprite.scale.set(0.5, 0.5);
                this.chargeSprites.push(sprite);
            }
        }

        // charge the laser
        if (isActivating && this.cooldownTimer <= 0)
        {
            if (this.chargeSound == null)
            {
                this.chargeSound = new SoundEffect("laser_charge", 0.5);
                parentShip.AddComponent(this.chargeSound);
            }

            this.chargeCounter = Math.min(this.chargeCounter + elapsedSeconds, LaserCannon.MAX_CHARGE_TIME);
        }

        var weaponFacing = parentShip.Facing;

        if (this.isTurreted)
        {
            weaponFacing = this.turretFacing;
        }
        
        // Update charging effects
        for (var i = 0; i < this.chargeSprites.length; i++)
        {
            var barrelPos = Vector2.FromPolarOffset(
                this.offsetMagnitudes[i],
                this.offsetDirections[i],
                parentShip);

            if (this.isTurreted)
            {
                barrelPos = this.AdjustBarrelForTurret(barrelPos);
            }

            this.chargeSprites[i].alpha = this.chargeCounter / LaserCannon.MAX_CHARGE_TIME;
            this.chargeSprites[i].position.set(
                barrelPos.X,
                barrelPos.Y);
            this.chargeSprites[i].rotation = weaponFacing;
        }

        // Fire the laser
        if (!isActivating && this.chargeCounter > 0)
        {
            this.FireLaserBlast(zone, state, parentShip, weaponFacing);

            this.cooldownTimer = this.shotCooldown;
            this.chargeCounter = 0;
            console.log("pew!");

            if (this.chargeSound != null)
            {
                parentShip.RemoveComponent(zone, this.chargeSound);
                this.chargeSound = null;
            }
        }
    }

    public UnInitialize(parentShip: GameObject): void
    {
        super.UnInitialize(parentShip);

        if (this.chargeSprites != null)
        {
            for (var i = 0; i < this.chargeSprites.length; i++)
            {
                this.chargeSprites[i].destroy();
            }
            this.chargeSprites = null;

            if (this.chargeSound != null)
            {
                parentShip.RemoveComponent(null, this.chargeSound);
                this.chargeSound = null;
            }
        }
    }

    public IconSprite(): string
    {
        return "icon_lasercannon";
    }

    public Name(): string
    {
        return "Laser";
    }

    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / this.shotCooldown, 1, 0);
    }

    public GetListing(): AbstractModuleListing
    {
        return new LaserCannonListing();
    }

    private FireLaserBlast(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        weaponFacing: number): void
    {
        var shotDamage = (0.4 + this.chargeCounter * 1.5) * this.targetDps;
        var shotScale = (this.chargeCounter + 1);

        var targetPoint = this.ClampHitPoint(this.inputSource.TargetPoint, parentShip, weaponFacing);

        var hitTarget = this.DetermineTargetHit(zone, targetPoint);

        if (hitTarget != null)
        {
            Mechanics.ResolveAttack(
                zone,
                targetPoint,
                hitTarget,
                shotDamage,
                false);

            Mechanics.CheckForPlayerTag(state, parentShip, hitTarget);
        }

        for (var i = 0; i < this.offsetMagnitudes.length; i++)
        {
            var barrelPos = Vector2.FromPolarOffset(
                this.offsetMagnitudes[i],
                this.offsetDirections[i],
                parentShip);

            if (this.isTurreted)
            {
                barrelPos = this.AdjustBarrelForTurret(barrelPos);
            }

            var dirToTarget = barrelPos.DirectionTo(targetPoint);

            var laserBlast = new GameObject(barrelPos, 0);
            laserBlast.AddComponent(new OffsetFollow(parentShip));
            laserBlast.AddComponent(new FadingLine(
                Vector2.ZERO,
                targetPoint,
                hitTarget != null ? hitTarget : parentShip,
                HexColor.PURPLE,
                shotScale,
                0.25,
                true));
            laserBlast.AddComponent(new TimedLife(1, false));
            zone.GameObjects.push(laserBlast);

            var flash = new GameObject(barrelPos, dirToTarget);
            flash.AddComponent(new OffsetFollow(parentShip));
            flash.AddComponent(new TimedLife(1, false));
            flash.AddComponent(new GrowingAndFadingSprite(
                "laser_shot_flash", FlightState.LAYER_MIDGROUND, HexColor.WHITE,
                0.25,
                shotScale / 2, shotScale / 2, // scale
                1, 0)); // alpha
            flash.AddComponent(new MutatedSoundEffect("laser", 3, 0.2 * shotScale));
            zone.GameObjects.push(flash);
        }
    }

    private ClampHitPoint(targetPoint: Vector2, parentShip: GameObject, weaponFacing: number): Vector2
    {
        var arcToTarget = parentShip.Position.BearingTo(
            weaponFacing,
            targetPoint);

        var clampedArc = Util.Clamp(arcToTarget, this.firingArc, -this.firingArc);

        var clampedPoint = Vector2.FromPolar(
            parentShip.Position.DistanceTo(targetPoint),
            clampedArc + weaponFacing)
            .Add(parentShip.Position);

        return clampedPoint;
    }

    private DetermineTargetHit(zone: Zone, targetPoint: Vector2): GameObject
    {
        for (var i = 0; i < zone.GameObjects.length; i++)
        {
            var colliders = zone.GameObjects[i].GetComponents<Collider>(Collider);

            for (var j = 0; j < colliders.length; j++)
            {
                if (colliders[j].ContainsPoint(targetPoint))
                {
                    return zone.GameObjects[i];
                }
            }
        }

        return null;
    }

    public static DpsForModuleSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 1 * DesignConstants.WeaponDpsPerSize;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 2 * DesignConstants.WeaponDpsPerSize;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 3 * DesignConstants.WeaponDpsPerSize;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }
}

class LaserCannonListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Laser";
        this.BaseCost = 50;
        this.Type = _2DLancer.Common.Data.ModuleType.PrimaryWeapon;
    }

    public BuildModule(): AbstractModule
    {
        return new LaserCannon();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        return [
            "DPS: " + LaserCannon.DpsForModuleSize(moduleSize) + "",

            "Laser cannons focus light into powerful beams. Laser cannons can be charge, increasing the beam damage the longer the trigger is held. \n \n Lasers are a precise weapon, hitting the target point directly and ignoring any intervening objects."
        ];
    }
}