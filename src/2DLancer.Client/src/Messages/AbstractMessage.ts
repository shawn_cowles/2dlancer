﻿class AbstractMessage implements IAbstractClientMessage
{
    public MessageName: string;

    constructor(messageName: string)
    {
        this.MessageName = messageName;
    }
}