﻿class ModuleButton extends Phaser.Button
{
    public static WIDTH = 50;
    public static HEIGHT = 30;

    private iconSprite: Phaser.Sprite;
    private shadeSprite: Phaser.Sprite;
    private tooltipSprite: Phaser.Sprite;
    
    private moduleSlot: ModuleSlot;
    
    public position: Phaser.Point;

    private state: FlightState;

    constructor(game: Phaser.Game, xPos: number, yPos: number, module: ModuleSlot, state: FlightState)
    {
        super(game, xPos, yPos, "module_button", () => this.OnClick(), null, 1, 0, 1, 0);

        game.add.existing(this);
        this.anchor.set(0.5, 0.5);
        
        this.moduleSlot = module;
        this.state = state;
    }

    public update()
    {
        super.update();

        // delayed creation of icon and text, to get layering right
        if (this.iconSprite == null)
        {
            this.iconSprite = this.game.add.sprite(this.position.x, this.position.y, this.moduleSlot.IconSprite());
            this.iconSprite.anchor.set(0.5, 0.5);
        }
        if (this.shadeSprite == null)
        {
            this.shadeSprite = this.game.add.sprite(this.position.x, this.position.y, "module_button_shade");
            this.shadeSprite.anchor.set(0.5, 0.5);
        }
        if (this.tooltipSprite == null)
        {
            var frame = this.moduleSlot.Type;
            
            this.tooltipSprite = this.game.add.sprite(
                this.position.x, this.position.y,
                "module_button_hotkey", frame);

            this.tooltipSprite.anchor.set(0.5, 0.5);
        }

        if (this.input.enabled && !this.moduleSlot.ModuleCanBeActivated())
        {
            this.input.enabled = false;
            this.frame = 2;
        }

        if (!this.input.enabled && this.moduleSlot.ModuleCanBeActivated())
        {
            this.input.enabled = true;
            this.frame = 0;
        }
        
        this.iconSprite.position = this.position;
        this.shadeSprite.position = this.position;
        // scale the shade sprite to illustrate cooldown
        this.shadeSprite.height = this.iconSprite.height * this.moduleSlot.ModuleCooldownPercent();
        this.tooltipSprite.position = this.position;
        
        this.iconSprite.visible = this.visible;
        this.tooltipSprite.visible = this.visible;
    }

    public destroy()
    {
        super.destroy();

        if (this.iconSprite != null)
        {
            this.iconSprite.destroy();
            this.iconSprite = null;
        }

        if (this.tooltipSprite != null)
        {
            this.tooltipSprite.destroy();
            this.tooltipSprite = null;
        }

        if (this.shadeSprite != null)
        {
            this.shadeSprite.destroy();
            this.shadeSprite = null;
        }
    }

    public OnClick(): void
    {
        this.state.PlayerShip
            .GetComponent<InputSource>(InputSource)
            .ActivatingModules[this.type] = true;
    }
}