﻿/// <reference path="AbstractWeaponModule.ts" />

// The Autocannon is a basic direct fire weapon.
class Autocannon extends AbstractWeaponModule
{
    private shotCooldown: number;
    private shotDamage: number;
    
    private nextBarrel: number;

    public LaunchVelocity: number;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        super.Initialize(state, parentShip, slot, offsetMagnitudes, offsetDirections);

        var targetDps = Autocannon.DpsForModuleSize(slot.Size);
        this.shotDamage = 1.5;
        this.shotCooldown = this.shotDamage / targetDps;

        this.LaunchVelocity = 1000;
        
        this.offsetDirections = offsetDirections;
        this.offsetMagnitudes = offsetMagnitudes;
        this.nextBarrel = 0;

        this.inputSource.HasTurretedWeapons = slot.WeaponsTurreted;
        this.inputSource.ProjectileFlightSpeed = this.LaunchVelocity;
    }

    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        super.Update(zone, state, parentShip, elapsedSeconds, isActivating);

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = this.shotCooldown;

            var weaponFacing = parentShip.Facing;

            if (this.isTurreted)
            {
                weaponFacing = this.turretFacing;
            }

            this.FireBullet(zone, parentShip, weaponFacing);
        }
    }
    
    public IconSprite(): string
    {
        return "icon_autocannon";
    }

    public Name(): string
    {
        return "Autocannon";
    }

    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / this.shotCooldown, 1, 0);
    }

    public GetListing(): AbstractModuleListing
    {
        return new AutocannonListing();
    }

    private FireBullet(zone: Zone, parentShip: GameObject, weaponFacing: number): void
    {
        var barrelPos = Vector2.FromPolarOffset(
            this.offsetMagnitudes[this.nextBarrel],
            this.offsetDirections[this.nextBarrel],
            parentShip);

        if (this.isTurreted)
        {
            barrelPos = this.AdjustBarrelForTurret(barrelPos);
        }
        
        this.nextBarrel = (this.nextBarrel + 1) % this.offsetDirections.length;
        
        var arcToTarget = barrelPos.BearingTo(
            weaponFacing,
            this.inputSource.TargetPoint);

        var clampedArc = Util.Clamp(arcToTarget, this.firingArc, -this.firingArc);

        var dirToTarget = Util.CorrectAngle(clampedArc + weaponFacing);

        var bullet = new GameObject(barrelPos, dirToTarget);
        bullet.AddComponent(new PhysicsBody(1));
        bullet.PhysicsBody.Velocity = parentShip.PhysicsBody.Velocity
            .Add(Vector2.FromPolar(this.LaunchVelocity, bullet.Facing));

        bullet.AddComponent(new SpriteDisplay(
            "bullet",
            FlightState.LAYER_MIDGROUND,
            HexColor.WHITE,
            1));
        bullet.AddComponent(new Collider(
            [new CollisionCircle(Vector2.ZERO, 2)],
            true,
            parentShip));
        bullet.AddComponent(new TimedLife(3, false));
        bullet.AddComponent(new ProjectileDamage(this.shotDamage, parentShip, false));
        zone.GameObjects.push(bullet);

        var flash = new GameObject(barrelPos, dirToTarget);
        flash.AddComponent(new OffsetFollow(parentShip));
        flash.AddComponent(new TimedLife(0.5, false));
        flash.AddComponent(new GrowingAndFadingSprite(
            "gun_shot_flash", FlightState.LAYER_MIDGROUND, HexColor.WHITE, 0.1,
            1, 1, // scale
            1, 0)); // alpha
        flash.AddComponent(new MutatedSoundEffect("autocannon", 3, 0.3));
        zone.GameObjects.push(flash);
    }

    public static DpsForModuleSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 1 * DesignConstants.WeaponDpsPerSize;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 2 * DesignConstants.WeaponDpsPerSize;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 3 * DesignConstants.WeaponDpsPerSize;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }
}

class AutocannonListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Autocannon";
        this.BaseCost = 50;
        this.Type = _2DLancer.Common.Data.ModuleType.PrimaryWeapon;
    }

    public BuildModule(): AbstractModule
    {
        return new Autocannon();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        return [
            "DPS: " + Autocannon.DpsForModuleSize(moduleSize) + "",

            "Autocannons are a simple and uncomplicated weapon. Massive electromagnets propel solid metal slugs at high velocities, dealing damage through kinetic energy alone. Autocannons are standard equipment on most newly constructed ships. \n \n Autocannons have to lead the target, and will hit the first object they collide with."
        ];
    }
}