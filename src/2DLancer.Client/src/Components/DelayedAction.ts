﻿/// <reference path="AbstractComponent.ts" />

// DelayedAction performs an arbitrary action after a delay.
class DelayedAction extends AbstractComponent
{
    private action: (z: Zone, s: FlightState) => void;
    private actionPerformed: boolean;
    private delay: number;

    constructor(delay: number, action: (z: Zone, s: FlightState) => void)
    {
        super();

        this.delay = delay;
        this.action= action;
        this.actionPerformed = false;
    }
    
    // Perform the action when the timer reaches 0
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.actionPerformed)
        {
            return;
        }
        
        this.delay -= elapsedSeconds;

        if (this.delay <= 0)
        {
            this.actionPerformed = true;

            this.action(zone, state);
        }
    }
}