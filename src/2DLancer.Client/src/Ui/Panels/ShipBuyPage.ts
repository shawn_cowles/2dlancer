﻿/// <reference path="AbstractStationPage.ts" />

// The station menu page for buying a ship.
class ShipBuyPage extends AbstractStationPage
{
    private shipData: IShipData;

    constructor(shipData: IShipData)
    {
        super();

        this.shipData = shipData;
    }

    public BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void
    {
        title.text = "Buy a " + this.shipData.Name;

        var currentShip = state.PlayerShip
            .GetComponent<PurchaseInformation>(PurchaseInformation)
            .ShipData;

        var purchaseCost = this.shipData.Cost - currentShip.Cost / 2;
        var balance = state.PlayerDataService.GetBalance();

        if (balance >= purchaseCost && this.shipData.Name != currentShip.Name)
        {
            buttons.push(new TextButton(state.game, 0, 0, "Purchase",
                () =>
                {
                    state.Analytics.MoneySpent("Ship", this.shipData.Name, purchaseCost);

                    state.PlayerDataService.SetBalance(balance - purchaseCost);
                    var loadout = state.PlayerDataService.GetCurrentLoadout();
                    loadout.ShipClass = this.shipData.Name;
                    loadout.MountedModules = [new AutocannonListing().Name];
                    state.PlayerDataService.SetCurrentLoadout(loadout);

                    state.PlayerShip = state.ShipBuilder.BuildShip(
                        state.PlayerShip.Position,
                        state.PlayerShip.Facing,
                        new ShipLoadout(
                            loadout.ShipClass,
                            state.PlayerSpawner.Color,
                            state.PlayerSpawner.FactionId,
                            false,
                            loadout.MountedModules),
                        null);

                    panel.SetPage(new MainStationPage());
                },
                this));
        }

        buttons.push(new TextButton(state.game, 0, 0, "Back",
            () =>
            {
                panel.SetPage(new ShipyardStationPage());
            },
            this));
        
        var shipSprite = new Phaser.Sprite(state.game, 0, 0,
            this.shipData.BaseSprite);
        shipSprite.scale.set(0.5, 0.5);
        state.game.add.existing(shipSprite);
        mainBodyContent.push(shipSprite);

        var statBlockElement = new Phaser.Text(state.game, 0, 0,
            Util.WrapText(this.MakeStatBlock(), 55),
            this.BodyTextStyle);
        state.game.add.existing(statBlockElement );
        mainBodyContent.push(statBlockElement);
        
        var bodyText = this.shipData.Description;
        if (this.shipData.Name != currentShip.Name)
        {
            bodyText = bodyText
                + " \n \n "
                + Util.FormatAsMoney(purchaseCost);

            if (balance < purchaseCost)
            {
                bodyText += " (insufficient funds)";
            }
        }
        else
        {
            bodyText = bodyText
                + " \n \n "
                + "You already own this ship.";
        }

        var descriptionElement = new Phaser.Text(state.game, 0, 0,
            Util.WrapText(bodyText, 55),
            this.BodyTextStyle);
        state.game.add.existing(descriptionElement);
        mainBodyContent.push(descriptionElement);
    }

    protected OnUpdate(state: FlightState): void
    {
    }

    private MakeStatBlock(): string
    {
        return "Health: " + this.shipData.Health + " hp"
            + "  Shields: " + DesignConstants.ShieldHealthPerSize * this.shipData.MaxShieldClass + " hp"
            + "  Top Speed: " + ((this.shipData.MaxEngineClass * DesignConstants.EngineThrustPerSize / this.shipData.Mass) / this.shipData.Drag).toFixed(0) + " m/s"
            + " \n Turn Rate: " + ((this.shipData.MaxEngineClass * DesignConstants.EngineTorquePerSize / this.shipData.Mass) * 180 / Math.PI).toFixed(0) + " deg/sec"
            + " \n Primary Weapon: " + this.SizeToString(this.shipData.ModuleSlots[_2DLancer.Common.Data.ModuleType.PrimaryWeapon].Size)
            + "  Secondary Weapon: " + this.SizeToString(this.shipData.ModuleSlots[_2DLancer.Common.Data.ModuleType.SecondaryWeapon].Size)
            + " \n Bay: " + this.SizeToString(this.shipData.ModuleSlots[_2DLancer.Common.Data.ModuleType.Bay].Size)
            + "  Booster: " + this.SizeToString(this.shipData.ModuleSlots[_2DLancer.Common.Data.ModuleType.Booster].Size);
    }

    private SizeToString(moduleSize: _2DLancer.Common.Data.ModuleSize): string
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return "Small";
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return "Medium";
            case _2DLancer.Common.Data.ModuleSize.Large:
                return "Large";
            case _2DLancer.Common.Data.ModuleSize.None:
                return "n/a";
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }   
}