﻿/// <reference path="AbstractComponent.ts" />

// ExplodeIntoDebris replaces a GameObject with debris when 
class ExplodeIntoDebris extends AbstractComponent
{
    private debrisSprites: string[];
    private debrisLaunchDirections: number[];
    private explodeVelocity: number;
    private burstCount: number;
    private explodeAreaWidth: number;
    private explodeAreaHeight: number;
    private debrisScale: number;

    constructor(
        debrisSprites: string[],
        debrisScale: number,
        debrisLaunchDirections: number[],
        explodeVelocity: number,
        burstCount: number,
        explodeAreaWidth: number,
        explodeAreaHeight: number)
    {
        super();

        this.debrisSprites = debrisSprites;
        this.debrisScale = debrisScale;
        this.debrisLaunchDirections = debrisLaunchDirections;
        this.explodeVelocity = explodeVelocity;
        this.burstCount = burstCount;
        this.explodeAreaWidth = explodeAreaWidth;
        this.explodeAreaHeight = explodeAreaHeight;
    }

    public OnMessage(zone: Zone, message: string): void
    {
        super.OnMessage(zone, message);

        if (message == Messages.DESTROYED)
        {
            var primaryDebris = <GameObject>null;

            for (var i = 0; i < this.debrisSprites.length; i++)
            {
                var debris = new GameObject(
                    this.GameObject.Position,
                    this.GameObject.Facing);
                debris.AddComponent(new PhysicsBody(this.GameObject.PhysicsBody.Mass / this.debrisSprites.length));
                debris.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity;
                debris.AddComponent(new DelayedImpulse(0.25,
                    Vector2.FromPolar(
                        this.explodeVelocity,
                        this.debrisLaunchDirections[i] + this.GameObject.Facing)));
                debris.AddComponent(new SpriteDisplay(
                    this.debrisSprites[i],
                    FlightState.LAYER_MIDGROUND,
                    HexColor.WHITE,
                    this.debrisScale));
                debris.AddComponent(new TimedLife(5, true));
                debris.AddComponent(new ExplodeOnDeath());
                debris.AddComponent(new DelayedSpinning(0.25, (Math.random() - 0.5) * 2));
                
                if (primaryDebris == null)
                {
                    primaryDebris = debris;

                    primaryDebris.AddComponent(new SoundEffect("ship_death", 0.5));
                }

                zone.GameObjects.push(debris);
            }
            
            for (var i = 0; i < this.burstCount; i++)
            {
                var x = (Math.random() - 0.5) * this.explodeAreaWidth;
                x = x * Math.cos(this.GameObject.Facing) + this.GameObject.Position.X;
                var y = (Math.random() - 0.5) * this.explodeAreaHeight;
                y = y * Math.sin(this.GameObject.Facing) + this.GameObject.Position.Y;

                var blast = new GameObject(new Vector2(x, y), this.GameObject.Facing);
                blast.AddComponent(new TimedLife(0.5, false));
                blast.AddComponent(new GrowingAndFadingSprite(
                    "large_blast", FlightState.LAYER_MIDGROUND, HexColor.WHITE, 0.5,
                    0, 1, // scale
                    1, 0)); // alpha
                blast.AddComponent(new OffsetFollow(primaryDebris));

                var blastSpawner = new GameObject(new Vector2(x, y), this.GameObject.Facing);
                blastSpawner.AddComponent(new DelayedSpawn(Math.random() * 0.5, blast));
                blastSpawner.AddComponent(new TimedLife(0.5, false));
                blastSpawner.AddComponent(new OffsetFollow(primaryDebris));
                zone.GameObjects.push(blastSpawner);
            }
        }
    }
}