﻿/// <reference path="AbstractComponent.ts" />

// Creates the visual effect of an Engine firing.
class EngineJet extends AbstractComponent
{
    protected sprite: Phaser.Sprite;
    private offsetMagnitude: number;
    private offsetDirection: number;
    private tint: number;

    private inputSource: InputSource;
    private thrusters: Thrusters;

    constructor(offsetPoint: Vector2, tint: number)
    {
        super();

        // Store the offset point in polar coordinates so it can track the game object properly as it turns
        this.offsetMagnitude = offsetPoint.Magnitude();
        this.offsetDirection = offsetPoint.Direction();
        this.tint = tint;
    }

    // create the sprite in phaser.
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone,state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
        this.thrusters = this.GameObject.GetComponent<Thrusters>(Thrusters);

        this.sprite = state.SpriteGroups[FlightState.LAYER_MIDGROUND].create(0, 0, "thruster_plume");
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = this.tint;
        this.sprite.visible = false;
    }

    // Handle visibility changes based on current throttle
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        var point = Vector2.FromPolarOffset(this.offsetMagnitude, this.offsetDirection, this.GameObject);

        this.sprite.position.set(point.X, point.Y);
        this.sprite.rotation = this.GameObject.Facing;

        this.sprite.visible = this.inputSource.ThrustFoward;

        if (this.thrusters.IsBoosting())
        {
            this.sprite.scale.set(1.7, 1.7);
        }
        else
        {
            this.sprite.scale.set(1, 1);
        }
    }
    
    // To uninitialize, destroy the sprite
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.sprite != null)
        {
            this.sprite.destroy();
            this.sprite = null;
        }
    }
}