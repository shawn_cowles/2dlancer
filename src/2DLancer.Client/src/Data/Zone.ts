﻿// A discrete section of the universe, simulated independently. 
class Zone
{
    public Name: string;

    public FriendlyName: string;

    public GameObjects: GameObject[];
    
    public ZoneLinks: ZoneLink[];

    public Width: number;
    public Height: number;
    public ZoneCenter: Vector2;

    public Actors: Actor[];
    
    constructor(name: string, friendlyName: string, width:number,height:number)
    {
        this.Name = name;
        this.FriendlyName = friendlyName;
        this.GameObjects = [];
        this.ZoneLinks = [];
        this.Actors = [];
        this.Width = width;
        this.Height = height;
        this.ZoneCenter = new Vector2(
            this.Width / 2,
            this.Height / 2);
    }

    // Perform an update on the zone
    public Update(state: FlightState): void
    {
        var elapsedSeconds = state.time.elapsed / 1000;

        // Normal Update
        for (var i = 0; i < this.GameObjects.length; i++)
        {
            this.GameObjects[i].Update(this, state, elapsedSeconds);
        }

        // Post Update Processing
        for (var i = 0; i < this.GameObjects.length; i++)
        {
            if (this.GameObjects[i].ShouldBeRemoved)
            {
                this.GameObjects[i].OnRemoved(this);
                this.GameObjects.splice(i, 1);
                i--;
            }
        }
    }
    
    // Perform a late update, only done on the active zone after the camera has moved
    public LateUpdate(state: FlightState): void
    {
        for (var i = 0; i < this.GameObjects.length; i++)
        {
            this.GameObjects[i].LateUpdate(this, state);
        }
    }

    // Search for an instance of T and return it, or null if it cannot be found.
    public FindComponent<T extends AbstractComponent>(type: Function): T
    {
        for (var i = 0; i < this.GameObjects.length; i++)
        {
            var comp = this.GameObjects[i].GetComponent<T>(type);

            if (comp != null)
            {
                return comp;
            }
        }

        return null;
    }
    
    // Find and cache any links to other zones
    public CacheZoneLinks(): void
    {
        this.ZoneLinks = [];

        for (var i = 0; i < this.GameObjects.length; i++)
        {
            var zoneLink = this.GameObjects[i].GetComponent<ZoneLink>(ZoneLink);

            if (zoneLink != null)
            {
                this.ZoneLinks.push(zoneLink);
            }
        }
    }

    public CleanForBackground(): void
    {
        for (var i = 0; i < this.GameObjects.length; i++)
        {
            this.GameObjects[i].ForceUninitialize();

            if (!this.GameObjects[i].HasComponent(StaticObjectIdentifier))
            {
                this.GameObjects.splice(i, 1);
                i--;
            }
        }
    }

    public BuildForActive(state: FlightState): void
    {
        for (var i = 0; i < this.Actors.length; i++)
        {
            var obj = this.Actors[i].BuildGameObject(state);

            if (obj != null)
            {
                this.GameObjects.push(obj);
            }
        }
    }
}