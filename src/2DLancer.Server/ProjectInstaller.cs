﻿using System.ComponentModel;

namespace _2DLancer.Server
{
    /// <summary>
    /// Service installer for the project.
    /// </summary>
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        /// <summary>
        /// Construct the installer.
        /// </summary>
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
