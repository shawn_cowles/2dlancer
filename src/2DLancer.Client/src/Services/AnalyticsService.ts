﻿// Handles sending analytics information
class AnalyticsService
{
    private urlBase: string;
    private sessionStart: number;
    private connection: ServerConnectionService;
    
    constructor(connection: ServerConnectionService)
    {
        this.connection = connection;
    }

    public Initialize(state: FlightState): void
    {
        this.connection.SendMessage(new UserConnectedMessage(state.PlayerDataService.PlayerId()));
    }

    public PlayerSpawned(): void
    {
        this.connection.SendMessage(new PlayerSpawnedEvent());
    }

    public ZoneVisited(zoneName: string): void
    {
        this.connection.SendMessage(new ZoneVisitedEvent(zoneName));
    }

    public ShipRepaired(stationName: string): void
    {
        this.connection.SendMessage(new ShipRepairedEvent(stationName));
    }

    public MoneyEarned(category: string, item: string, amount: number): void
    {
        this.connection.SendMessage(new MoneyEarnedEvent(category, item, amount));
    }

    public MoneySpent(category: string, item: string, amount: number): void
    {
        this.connection.SendMessage(new MoneySpentEvent(category, item, amount));
    }
    
    public DockedAtStation(stationName: string): void
    {
        this.connection.SendMessage(new DockedAtStationEvent(stationName));
    }
}