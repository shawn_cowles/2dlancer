﻿/// <reference path="AbstractAiBehavior.ts" />

// A behavior that attacks nearby enemy ships.
class AttackNearbyBehavior extends AbstractAiBehavior
{
    private CHASE_RANGE = 2000;
    private AGGRO_RANGE = 1500;
    private SHOOT_RANGE = 1000;
    private EVADE_DIST_HEAD_ON = 600;
    private EVADE_DIST_TRAILING = 200;
    private EVADE_TIME = 1;

    private targetGameObject: GameObject;
    private targetShip: Ship;
    
    private evadeTimer: number;
    private isEvading: boolean;

    // Initialize the behavior
    public Initialize(control: CompositeBotControl, zone: Zone): void
    {
        this.isEvading = false;
        this.evadeTimer = 0;
    }

    // Perform an update, returns true if the behavior applies, false otherwise
    public Update(
        control: CompositeBotControl,
        state: FlightState,
        zone: Zone,
        inputSource: InputSource,
        elapsedSeconds: number): boolean
    {
        if (this.targetGameObject == null
            || this.targetGameObject.ShouldBeRemoved
            || control.GameObject.Position.DistanceTo(this.targetGameObject.Position) > this.CHASE_RANGE)
        {
            this.targetGameObject = null;
            this.isEvading = false;
            this.evadeTimer = 0;

            this.FindTargetGameObject(control.GameObject, zone.GameObjects);
        }

        if (this.targetGameObject != null)
        {
            this.ShootAtTarget(control, inputSource);

            if (this.isEvading)
            {
                this.EvadeTarget(control, inputSource, elapsedSeconds);
            }
            else
            {
                this.AttackTarget(control, inputSource, elapsedSeconds);
            }
            
            return true;
        }

        return false;
    }
    
    public ControlShipActor(
        ship: Ship,
        state: FlightState,
        elapsedSeconds: number): boolean
    {
        this.isEvading = false;
        this.evadeTimer = 0;

        if (this.targetShip == null
            || this.targetShip.IsDead
            || ship.Position.DistanceTo(this.targetShip.Position) > this.CHASE_RANGE)
        {
            this.targetShip = null;

            this.FindTargetActor(ship, ship.Zone.Actors);
        }

        if (this.targetShip != null)
        {
            if (ship.Position.DistanceTo(this.targetShip.Position) < this.SHOOT_RANGE)
            {
                ship.Attack(this.targetShip, elapsedSeconds);
            }
            else
            {
                ship.MoveTo(this.targetShip.Position, elapsedSeconds);
            }
            
            return true;
        }

        return false;
    }

    private FindTargetGameObject(self: GameObject, potentials: GameObject[]): void
    {
        var nearDist = <number>null;

        for (var i = 0; i < potentials.length; i++)
        {
            // only consider objects that can be damaged
            if (potentials[i].HasComponent(HullHealth))
            {
                // only target hostiles
                if (Mechanics.IsHostile(self, potentials[i]))
                {
                    var dist = self.Position.DistanceTo(potentials[i].Position);

                    if (dist < this.AGGRO_RANGE && (nearDist == null || dist < nearDist))
                    {
                        nearDist = dist;
                        this.targetGameObject = potentials[i];
                        this.targetShip = null;
                    }
                }
            }
        }
    }

    private ShootAtTarget(control: CompositeBotControl, inputSource: InputSource): void
    {
        var distToTarget = control.GameObject.Position.DistanceTo(this.targetGameObject.Position);

        var arcToTarget = control.GameObject.Position.BearingTo(
            control.GameObject.Facing,
            inputSource.TargetPoint);

        inputSource.TargetObject = this.targetGameObject;

        // lead the target if we have guns
        if (inputSource.ProjectileFlightSpeed != null)
        {
            inputSource.TargetPoint = Mechanics.LeadTarget(
                control.GameObject,
                this.targetGameObject,
                inputSource.ProjectileFlightSpeed);
        }
        else
        {
            inputSource.TargetPoint = this.targetGameObject.Position;
        }

        // TODO adjust firing if ship has turrets instead of gimbals

        // Fire weapons if they are in arc
        if (distToTarget < this.SHOOT_RANGE && (Math.abs(arcToTarget) < Math.PI / 4
            || inputSource.HasTurretedWeapons))
        {
            // rapidly toggle shooting to prevent laser overcharge
            inputSource.ActivatingModules[_2DLancer.Common.Data.ModuleType.PrimaryWeapon]
                = !inputSource.ActivatingModules[_2DLancer.Common.Data.ModuleType.PrimaryWeapon];
        }
        else
        {
            inputSource.ActivatingModules[_2DLancer.Common.Data.ModuleType.PrimaryWeapon] = false;
        }
    }

    private AttackTarget(
        control: CompositeBotControl,
        inputSource: InputSource,
        elapsedSeconds: number): void
    {
        var distToTarget = control.GameObject.Position.DistanceTo(this.targetGameObject.Position);
        
        // lead the target if we have guns
        if (inputSource.ProjectileFlightSpeed != null)
        {
            inputSource.TargetPoint = Mechanics.LeadTarget(
                control.GameObject,
                this.targetGameObject,
                inputSource.ProjectileFlightSpeed);
        }
        else
        {
            inputSource.TargetPoint = this.targetGameObject.Position;
        }

        var arcToTarget = control.GameObject.Position.BearingTo(
            control.GameObject.Facing,
            inputSource.TargetPoint);

        var targetArcToSelf = this.targetGameObject.Position.BearingTo(
            this.targetGameObject.Facing,
            control.GameObject.Position);
        
        // If nose on and too close, evade instead
        if (distToTarget < this.EVADE_DIST_HEAD_ON
            && Math.abs(arcToTarget) < Math.PI / 4
            && Math.abs(targetArcToSelf) < Math.PI / 4)
        {
            this.evadeTimer = this.EVADE_TIME;
            this.isEvading = true;
        }

        // don't rear-end a target
        if (distToTarget < this.EVADE_DIST_TRAILING && Math.abs(arcToTarget) < Math.PI / 4)
        {
            this.evadeTimer = this.EVADE_TIME;
            this.isEvading = true;
        }
        
        // Approach the target
        inputSource.DesiredFacing = control.GameObject.Position
            .DirectionTo(inputSource.TargetPoint);
        inputSource.ThrustFoward = true;
    }

    private EvadeTarget(
        control: CompositeBotControl,
        inputSource: InputSource,
        elapsedSeconds: number): void
    {
        var targetPoint = this.targetGameObject.Position;
        var distToTarget = control.GameObject.Position.DistanceTo(targetPoint);

        this.evadeTimer -= elapsedSeconds;

        if (this.evadeTimer <= 0)
        {
            this.isEvading = false;
        }

        inputSource.DesiredFacing = control.GameObject.Position.DirectionTo(targetPoint) + Math.PI;
        inputSource.ThrustFoward = true;
    }
    
    private FindTargetActor(self: Ship, potentials: Actor[]): void
    {
        var nearDist = <number>null;

        for (var i = 0; i < potentials.length; i++)
        {
            // only consider objects that can be damaged
            if (potentials[i] instanceof Ship)
            {
                var potentialShip = <Ship>potentials[i];

                // only target hostiles
                if (self.FactionId != potentialShip.FactionId)
                {
                    var dist = self.Position.DistanceTo(potentialShip.Position);

                    if (dist < this.AGGRO_RANGE && (nearDist == null || dist < nearDist))
                    {
                        nearDist = dist;
                        this.targetGameObject = null;
                        this.targetShip = potentialShip;
                    }
                }
            }
        }
    }
}