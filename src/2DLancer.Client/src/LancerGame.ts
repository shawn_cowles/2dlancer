﻿//Class for the game itself, handles setup and useful persistent properties
class LancerGame extends Phaser.Game
{
    constructor(parentElement: any)
    {
        super("100%", "100%", Phaser.AUTO, parentElement);

        // Setup States
        this.state.add(BootState.NAME, BootState, false);
        this.state.add(LoadState.NAME, LoadState, false);
        this.state.add(FlightState.NAME, FlightState, false);

        //// Start the Boot State
        this.state.start(BootState.NAME);
    }
}

window.onload = () =>
{
    new LancerGame(document.getElementById('game_div'));
};