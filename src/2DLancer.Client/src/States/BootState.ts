﻿/// <reference path="AbstractState.ts" />

// The first state, performs some pre-loading for the proper load state
class BootState extends AbstractState
{
    public static NAME = "state_boot";

    preload()
    {
        this.game.load.image("load_sprite", "images/loading_sprite.png");
        //this.game.load.image("title_splash", "images/Title Splash.png");
    }

    create()
    {
        this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = false;
        this.scale.scaleMode = Phaser.ScaleManager.RESIZE;
        this.scale.trackParentInterval = 250;

        // swallow right click context menu
        this.game.canvas.oncontextmenu = (e: PointerEvent) =>
        {
            e.preventDefault();
        };

        this.game.state.start(LoadState.NAME, true, false);
    }
}