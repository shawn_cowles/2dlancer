﻿/// <reference path="AbstractComponent.ts" />

// Handles creating and moving dust clouds in front of the camera
class DustCloudManager extends AbstractComponent
{
    private static SPRITE_SIZE = 512;
    protected clouds: Phaser.Sprite[][];

    protected spriteLayer: number;
    protected parallaxFactor: number;
    protected heavyDust: boolean;

    constructor(spriteLayer: number, heavyDust: boolean, parallaxFactor: number)
    {
        super();

        this.spriteLayer = spriteLayer;
        this.heavyDust = heavyDust;
        this.parallaxFactor = parallaxFactor;
        this.clouds = new Array<Phaser.Sprite[]>();
    }
    
    // Perform a simulation update
    public LateUpdate(zone: Zone, state: FlightState): void
    {
        super.LateUpdate(zone, state);

        this.CheckForAndAddCloudsIfNeeded(state);

        this.UpdatePositions(Vector2.FromPoint(state.camera.position));
        
    }
    
    // Clean up the clouds spites
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.clouds.length > 0)
        {
            for (var i = 0; i < this.clouds.length; i++)
            {
                for (var j = 0; j < this.clouds[i].length; j++)
                {
                    this.clouds[i][j].destroy();
                }
            }

            this.clouds = [];
        }
    }
    
    private CheckForAndAddCloudsIfNeeded(state: FlightState): void
    {
        var spritesWide = Math.ceil(state.camera.width / DustCloudManager.SPRITE_SIZE) + 3;
        var spritesHigh = Math.ceil(state.camera.height / DustCloudManager.SPRITE_SIZE) + 3;

        // All is good, skip the rest
        if (this.clouds.length == spritesWide && this.clouds[0].length == spritesHigh)
        {
            return;
        }

        // sprites don't match, destroy them all and rebuild

        for (var i = 0; i < this.clouds.length; i++)
        {
            for (var j = 0; j < this.clouds[i].length; j++)
            {
                this.clouds[i][j].destroy();
            }
        }
        this.clouds = [];


        var ulIndex = new Vector2(
            Math.floor(spritesWide / 2),
            Math.floor(spritesHigh / 2));

        for (var i = 0; i < spritesWide; i++)
        {
            this.clouds.push([]);

            for (var j = 0; j < spritesHigh; j++)
            {
                var sprite = <Phaser.Sprite>state.SpriteGroups[this.spriteLayer]
                    .create(0, 0, "dust_clouds");
                sprite.anchor.set(0.5, 0.5);
                sprite.tint = HexColor.WHITE;

                if (this.heavyDust)
                {
                    sprite.alpha = 0.4;
                }
                else
                {
                    sprite.alpha = 0.2;
                }
                
                this.clouds[i].push(sprite);
            }
        }
    }

    private UpdatePositions(cameraUl: Vector2): void
    {
        var basePos = new Vector2(
            Math.floor(cameraUl.X / DustCloudManager.SPRITE_SIZE)
                * DustCloudManager.SPRITE_SIZE,
            Math.floor(cameraUl.Y / DustCloudManager.SPRITE_SIZE)
            * DustCloudManager.SPRITE_SIZE);

        var stepBack = cameraUl
            .Subtract(cameraUl.Scale(this.parallaxFactor))
            .Mod(DustCloudManager.SPRITE_SIZE);
        
        for (var i = 0; i < this.clouds.length; i++)
        {
            for (var j = 0; j < this.clouds[i].length; j++)
            {
                this.clouds[i][j].position.set(
                    basePos.X + (i-1) * DustCloudManager.SPRITE_SIZE + stepBack.X,
                    basePos.Y + (j-1) * DustCloudManager.SPRITE_SIZE + stepBack.Y);
            }
        }
    }
}