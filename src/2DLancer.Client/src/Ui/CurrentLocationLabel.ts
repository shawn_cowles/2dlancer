﻿/// <reference path="UiPanel.ts" />

// A widget that displays the current zone location of the player.
class CurrentLocationLabel extends AbstractUiWidget
{
    public static WIDTH = 200;
    public static HEIGHT = 25;

    private state: FlightState;

    private labelText: Phaser.Text;
    
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.labelText = state.game.add.text(0, 0, "", textStyle);
        this.labelText.anchor.set(0.5, 0.5);
    }
    
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        var xPosition = state.game.camera.width - CurrentLocationLabel.WIDTH / 2 + state.game.camera.view.x;
        var yPosition = state.game.camera.view.y + CurrentLocationLabel.HEIGHT / 2;
        
        this.labelText.position.set(
            xPosition,
            yPosition);

        this.labelText.text = state.GetActiveZone().FriendlyName;
    }

    public Destroy()
    {
        super.Destroy();

        this.labelText.destroy();
    }

    // Determine if a point is over a UI element, used to suppress shooting when the player
    // is using the ui
    public PointIsInPanel(point: Vector2): boolean
    {
        if (this.labelText == null)
        {
            return false;
        }

        return this.labelText.position.x - CurrentLocationLabel.WIDTH / 2 <= point.X
            && point.X <= this.labelText.position.x + CurrentLocationLabel.WIDTH / 2
            && this.labelText.position.y - CurrentLocationLabel.HEIGHT / 2 <= point.Y
            && point.Y <= this.labelText.position.y + CurrentLocationLabel.HEIGHT / 2;
    }
}