﻿/// <reference path="AbstractComponent.ts" />

// A component that allows a GameObject to be collided with. Models collision shape with several
// circles. Can be given a GameObject to ignore, useful for projectiles to ignore collisions with the ship that fired them.
class Collider extends AbstractComponent
{
    private spriteName: string;
    private scale: number;
    private ignoreObject: GameObject;
    public TriggerOnly: boolean;

    private circles: CollisionCircle[];

    public MaximumRadius: number;

    constructor(circles: CollisionCircle[], triggerOnly: boolean, ignoreObject?: GameObject)
    {
        super();

        this.circles = circles;
        this.TriggerOnly = triggerOnly;
        this.ignoreObject = ignoreObject;

        this.MaximumRadius = 0;
        for (var i = 0; i < this.circles.length; i++)
        {
            this.MaximumRadius = Math.max(this.MaximumRadius, this.circles[i].Radius);
        }
    }

    // Check for a collision with another collider
    public CollidesWith(other: Collider): boolean
    {
        // delegate
        return this.GreatestOverlap(other) >= 0;
    }

    // Get the greatest distance by which the two colliders overlap, returns negative numbers
    // when there is no overlap.
    public GreatestOverlap(other: Collider): number
    {
        // cant collide with self
        if (other.GameObject == this.GameObject)
        {
            return -1;
        }

        // testing against the ignore object, ignore it
        if (other.GameObject == this.ignoreObject
            || this.GameObject == other.ignoreObject)
        {
            return -1;
        }

        // Triggers don't hit triggers
        if (this.TriggerOnly && other.TriggerOnly)
        {
            return -1;
        }

        var max = -1;

        for (var i = 0; i < this.circles.length; i++)
        {
            for (var j = 0; j < other.circles.length; j++)
            {
                var overlap = this.circles[i].Overlap(this.GameObject, other.GameObject, other.circles[j]);

                if (overlap > max)
                {
                    max = overlap;
                }
            }
        }

        return max;
    }

    // Does this collider contain a specified point?
    public ContainsPoint(point: Vector2): boolean
    {
        for (var i = 0; i < this.circles.length; i++)
        {
            if (this.circles[i].ContainsPoint(this.GameObject, point))
            {
                return true;
            }
        }

        return false;
    }
}