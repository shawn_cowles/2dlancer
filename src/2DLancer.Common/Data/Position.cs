﻿using System;
using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A position in space.
    /// </summary>
    [TsClass(Module ="", Name ="IPosition")]
    public class Position
    {
        /// <summary>
        /// The X component of the position.
        /// </summary>
        public double X { get; }

        /// <summary>
        /// The Y component of the position.
        /// </summary>
        public double Y { get; }

        /// <summary>
        /// Construct a new Position
        /// </summary>
        /// <param name="x">The X component of the position.</param>
        /// <param name="y">The Y component of the position.</param>
        public Position(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Determine the distance between two positions.
        /// </summary>
        /// <param name="other">The other position.</param>
        /// <returns>The distance between two positions.</returns>
        public double DistanceTo(Position other)
        {
            var dx = X - other.X;
            var dy = Y - other.Y;

            return Math.Sqrt(dx * dx + dy * dy);
        }

        /// <summary>
        /// Scale a position by a number.
        /// </summary>
        /// <param name="pos">The position to scale.</param>
        /// <param name="scalar">The number to scale the position by.</param>
        /// <returns>The original position with components scaled by the scalar value.</returns>
        public static Position operator *(Position pos, double scalar)
        {
            return new Position(
                pos.X * scalar,
                pos.Y * scalar);
        }
    }
}
