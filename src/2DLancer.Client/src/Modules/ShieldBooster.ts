﻿/// <reference path="AbstractModule.ts" />

// The ShieldBooster triggers immediate shield recharge at an increased rate.
class ShieldBooster extends AbstractModule
{
    public static BOOST_FACTOR = 1.5;

    private boostDuration: number;
    private boostCooldown: number;
    private cooldownTimer: number;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.boostDuration = ShieldBooster.DurationForSize(slot.Size);
        this.boostCooldown = ShieldBooster.CooldownForSize(slot.Size);
        
        this.cooldownTimer = 0;
    }
    
    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = this.boostCooldown;

            parentShip
                .GetComponent<ShieldGenerator>(ShieldGenerator)
                .TriggerBoost(this.boostDuration);
        }
    }
    
    public UnInitialize(parentShip: GameObject): void
    {
    }
    
    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / this.boostCooldown, 1, 0);
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public IconSprite(): string
    {
        return "icon_shield_boost";
    }

    public Name(): string
    {
        return "Shield Boost";
    }

    public GetListing(): AbstractModuleListing
    {
        return new ShieldBoosterListing();
    }

    public static DurationForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 5;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 10;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 15;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }

    public static CooldownForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        return ShieldBooster.DurationForSize(moduleSize) + 20;
    }
}

class ShieldBoosterListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Shield Boost";
        this.BaseCost = 200;
        this.Type = _2DLancer.Common.Data.ModuleType.Booster;
    }

    public BuildModule(): AbstractModule
    {
        return new ShieldBooster();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        return [
            "Recharge Boost: " + ShieldBooster.BOOST_FACTOR + "x"
            + "  Duration: " + ShieldBooster.DurationForSize(moduleSize) + "s"
            + "  Cooldown: " + ShieldBooster.CooldownForSize(moduleSize) + "s",
            
            "An Shield Booster is a series of capacitors tuned to deliver a boost of power to a ship's shield generators. The effect is immediate shield recharge at a faster rate, regardless of incoming damage."
        ];
    }
}