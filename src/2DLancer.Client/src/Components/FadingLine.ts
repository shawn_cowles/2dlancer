﻿/// <reference path="AbstractComponent.ts" />

// FadingLine displays a line from the GameObject to another point that fades over time. 
// It can track the target point as an offset of another GameObject, but wont track if
// that parameter is null.
class FadingLine extends AbstractComponent
{
    // If offsetTrack is not null then this will be the offset from offsetTrack
    private toPoint: Vector2;
    private fromOffset: Vector2;
    private offsetTrack: GameObject;
    private lineColor: number;
    private lineWidth: number;
    private maxFadeTime: number;
    private fadeTime: number;
    private overlay: boolean;

    constructor(
        fromOffset: Vector2,
        toPoint: Vector2,
        offsetTrack: GameObject,
        lineColor: number,
        lineWidth: number,
        fadeTime: number,
        overlay: boolean)
    {
        super();

        this.offsetTrack = offsetTrack;

        if (offsetTrack != null)
        {
            this.toPoint = toPoint.Subtract(offsetTrack.Position);
        }
        else
        {
            this.toPoint = toPoint;
        }
        this.lineColor = lineColor;
        this.lineWidth = lineWidth;
        this.maxFadeTime = fadeTime;
        this.fadeTime = fadeTime;
        this.overlay = overlay;

        this.fromOffset = fromOffset;
    }
    
    // Draw the line
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.fadeTime -= elapsedSeconds;
        
        if (this.fadeTime > 0)
        {
            var alpha = this.fadeTime / this.maxFadeTime;

            var graphics = state.Graphics;

            if (this.overlay)
            {
                graphics = state.OverlayGraphics;
            }

            var startPos = this.GameObject.Position.Add(this.fromOffset);

            graphics.moveTo(startPos.X, startPos.Y);
            graphics.lineStyle(this.lineWidth, this.lineColor, alpha);

            var targetPoint = this.toPoint;

            if (this.offsetTrack != null)
            {
                targetPoint = targetPoint.Add(this.offsetTrack.Position);
            }

            graphics.lineTo(targetPoint.X, targetPoint.Y);
        }
    }
}