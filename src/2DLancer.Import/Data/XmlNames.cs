﻿namespace _2DLancer.Import.Data
{
    /// <summary>
    /// Constants for names in xml files.
    /// </summary>
    public static class XmlNames
    {
        /// <summary>
        /// An xml element in a ship details file for a ship.
        /// </summary>
        public const string SHIP_DETAILS_SHIP_ELEMENT = "ship";

        /// <summary>
        /// The ship name attribute on a ship element in a a ship details file.
        /// </summary>
        public const string SHIP_DETAILS_SHIP_NAME = "name";

        /// <summary>
        /// The ship description attribute on a ship element in a a ship details file.
        /// </summary>
        public const string SHIP_DETAILS_SHIP_DESCIPTION = "description";

        /// <summary>
        /// The ship cost attribute on a ship element in a a ship details file.
        /// </summary>
        public const string SHIP_DETAILS_SHIP_COST = "cost";
    }
}
