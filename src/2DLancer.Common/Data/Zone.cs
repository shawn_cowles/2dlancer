﻿using System.Collections.Generic;
using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Structural information for a zone in the world.
    /// </summary>
    [TsClass(Module = "", Name = "IZone")]
    public class Zone
    {
        /// <summary>
        /// The name of the zone.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The user friendly name of the zone.
        /// </summary>
        public string FriendlyName { get; private set; }

        /// <summary>
        /// Static celestials in the zone.
        /// </summary>
        public List<Celestial> Celestials { get; private set; }

        /// <summary>
        /// Spawners in the zone.
        /// </summary>
        public List<Spawner> Spawners { get; private set; }

        /// <summary>
        /// Navigation points in the zone.
        /// </summary>
        public List<NavPoint> NavPoints { get; private set; }

        /// <summary>
        /// SpaceStations in the zone.
        /// </summary>
        public List<SpaceStation> SpaceStations { get; private set; }

        /// <summary>
        /// Background objects in the zone.
        /// </summary>
        public List<BackgroundObject> BackgroundObjects { get; private set; }

        /// <summary>
        /// The width of the zone, in meters.
        /// </summary>
        public double Width { get; private set; }

        /// <summary>
        /// The height of the zone, in meters.
        /// </summary>
        public double Height { get; private set; }

        /// <summary>
        /// Local dust density in the zone.
        /// </summary>
        public DustDensity DustDensity { get; private set; }

        /// <summary>
        /// Construct a new Zone
        /// </summary>
        /// <param name="name">The name of the zone.</param>
        /// <param name="friendlyName">The user friendly name of the zone.</param>
        /// <param name="width">The height of the zone, in meters.</param>
        /// <param name="height">The width of the zone, in meters.</param>
        /// <param name="dustDensity">The dust density of the zone.</param>
        public Zone(string name, string friendlyName, double width, double height, DustDensity dustDensity)
        {
            Name = name;
            FriendlyName = friendlyName;
            Celestials = new List<Celestial>();
            Spawners = new List<Spawner>();
            NavPoints = new List<NavPoint>();
            SpaceStations = new List<SpaceStation>();
            BackgroundObjects = new List<BackgroundObject>();

            Width = width;
            Height = height;
            DustDensity = dustDensity;
        }
    }
}
