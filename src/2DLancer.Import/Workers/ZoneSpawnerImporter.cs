﻿using System;
using System.Collections.Generic;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;
using TiledSharp;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports player and bot spawners for zones.
    /// </summary>
    public class ZoneSpawnerImporter : AbstractWorker
    {
        // Known good property values
        private string[] KNOWN_BEHAVIORS = new[] { "patrol", "trade" };
        private string[] KNOWN_SHIPS = new[] { "Talis", "Pulsar", "Canis", "Cherubim", "Junker", "Quasar" };
        private string[] KNOWN_WEAPONS = new[] { "Autocannon", "Laser" };
        private Dictionary<string, int> KNOWN_FACTIONS_AND_COLORS = new Dictionary<string, int>
        {
            {"blue", 0x0000FF },
            {"red", 0xFF0000 }
        };

        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Zone Spawner Importer"; }
        }


        /// <summary>
        /// Import celestials.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                foreach (var zoneName in importContext.Zones.Keys)
                {
                    var zone = importContext.Zones[zoneName];

                    foreach (var layer in importContext.ZoneMaps[zoneName].ObjectGroups)
                    {
                        foreach (var obj in layer.Objects)
                        {
                            try
                            {
                                switch (obj.Type)
                                {
                                    case ObjectTypes.BOT_SPAWNER:
                                        ImportBotSpawner(zone, obj);
                                        break;
                                    case ObjectTypes.PLAYER_SPAWNER:
                                        ImportPlayerSpawner(zone, obj);
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}' in zone '{zone.Name}'", ex);
                            }
                        }
                    }
                }

                return new WorkResult(Name, true, string.Empty);
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }
        }

        private void ImportBotSpawner(Zone zone, TmxObject obj)
        {
            var behavior = GetProperty(obj, PropertyNames.SPAWNER_BEHAVIOR);
            var shipClass = GetProperty(obj, PropertyNames.SPAWNER_SHIP);
            var weaponClass = GetProperty(obj, PropertyNames.SPAWNER_WEAPON);
            var faction = GetProperty(obj, PropertyNames.SPAWNER_FACTION);
            var population = GetIntProperty(obj, PropertyNames.SPAWNER_POPULATION);
            var spawnDelay = GetDoubleProperty(obj, PropertyNames.SPAWNER_SPAWN_DELAY);

            if (!KNOWN_BEHAVIORS.Contains(behavior))
            {
                throw new ImportException(
                    $"Unknown behavior type: '{behavior}' on object {obj.Name}");
            }

            if (!KNOWN_SHIPS.Contains(shipClass))
            {
                throw new ImportException(
                    $"Unknown ship class: '{shipClass}' on object {obj.Name}");
            }

            if (!KNOWN_WEAPONS.Contains(weaponClass))
            {
                throw new ImportException(
                    $"Unknown weapon class: '{weaponClass}' on object {obj.Name}");
            }

            if (!KNOWN_FACTIONS_AND_COLORS.ContainsKey(faction))
            {
                throw new ImportException(
                    $"Unknown faction: '{faction}' on object {obj.Name}");
            }

            if (!KNOWN_FACTIONS_AND_COLORS.ContainsKey(faction))
            {
                throw new ImportException(
                    $"Unknown faction: '{faction}' on object {obj.Name}");
            }

            zone.Spawners.Add(new Spawner
            {
                Position = new Position(obj.X + obj.Width / 2, obj.Y + obj.Height / 2),
                Facing = obj.Rotation,
                IsPlayerSpawner = false,
                Faction = faction,
                Color = KNOWN_FACTIONS_AND_COLORS[faction],
                Behavior = behavior,
                ShipClass = shipClass,
                WeaponClass = weaponClass,
                Population = population,
                SpawnDelay = spawnDelay
            });
        }

        private void ImportPlayerSpawner(Zone zone, TmxObject obj)
        {
            var faction = GetProperty(obj, PropertyNames.SPAWNER_FACTION);

            if (!KNOWN_FACTIONS_AND_COLORS.ContainsKey(faction))
            {
                throw new ImportException(
                    $"Unknown faction: '{faction}' on object {obj.Name}");
            }

            zone.Spawners.Add(new Spawner
            {
                Position = new Position(obj.X + obj.Width / 2, obj.Y + obj.Height / 2),
                Facing = obj.Rotation / 180 * Math.PI,
                IsPlayerSpawner = true,
                Faction = faction,
                Color = KNOWN_FACTIONS_AND_COLORS[faction]
            });
        }
    }
}

