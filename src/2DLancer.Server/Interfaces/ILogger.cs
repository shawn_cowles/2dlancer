﻿using System;

namespace _2DLancer.Server.Interfaces
{
    /// <summary>
    /// A logger. This isn't available though dependency injection, and should be obtained from an <see cref="ILoggerProvider"/>.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Log a debug message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Debug(string message);

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Info(string message);

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Warn(string message);

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        void Error(string message);

        /// <summary>
        /// Log an error message and an exception.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception.</param>
        void Error(string message, Exception ex);

        /// <summary>
        /// Log an exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        void Error(Exception ex);
    }
}
