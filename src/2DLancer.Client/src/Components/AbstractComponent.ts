﻿// The base class for any component on a game object
abstract class AbstractComponent
{
    // The GameObject containing this component.
    public GameObject: GameObject;

    private isInitialized: boolean;
    public IsControlOverride: boolean;

    constructor()
    {
        this.isInitialized = false;
        this.IsControlOverride = false;
    }

    // Initialize the component
    protected Initialize(zone: Zone, state: FlightState): void
    {
    }

    // Perform a simulation update
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        if (!this.isInitialized)
        {
            this.Initialize(zone, state);
            
            this.isInitialized = true;
        }
    }
    
    // Perform a late update, only done on the active zone after the camera has moved
    public LateUpdate(zone: Zone, state: FlightState): void
    {
    }

    // Uninitialize the component from active sim mode.
    protected Uninitialize(): void
    {
    }
    
    // Called when the component is removed from a GameObject, or when the GameObject itself is removed from its zone
    public OnRemoved(zone: Zone): void
    {
        if (this.isInitialized)
        {
            this.Uninitialize();
        }
    }

    // Called when this component's GameObject has collided with another
    public OnCollided(zone: Zone, state: FlightState, other: GameObject): void
    {
    }

    // Called when the GameObject is told to broadcast a message
    public OnMessage(zone: Zone, message: string): void
    {
    }

    // Force unintialize a component, useful for temporarily removing an object from a zone
    public ForceUninitialize(): void
    {
        if (this.isInitialized)
        {
            this.Uninitialize();
        }
        
        this.isInitialized = false;
    }
}