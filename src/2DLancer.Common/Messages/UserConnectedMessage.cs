﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// The first message sent when a user starts the game, initializes tracking on the backend.
    /// </summary>
    [TsClass(Module = "", Name = "IUserConnectedMessage")]
    [DataContract]
    public class UserConnectedMessage : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "UserConnectedMessage";

        /// <summary>
        /// The unique id of the user, stored on the client.
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Construct a new UserConnectedMessage.
        /// </summary>
        public UserConnectedMessage() 
            : base(NAME)
        {
        }
    }
}
