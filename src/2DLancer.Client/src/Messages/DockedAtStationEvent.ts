﻿class DockedAtStationEvent implements IDockedAtStationEvent
{
    public MessageName = "DockedAtStationEvent";

    public StationName: string;

    constructor(stationName: string)
    {
        this.StationName = stationName;
    }
}