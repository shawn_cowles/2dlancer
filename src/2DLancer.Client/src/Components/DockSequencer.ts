﻿/// <reference path="AbstractComponent.ts" />

// Handles docking the player's ship to a station.
class DockSequencer extends AbstractComponent
{
    private inputSource: InputSource;

    private destinationStation: SpaceStationBehavior;

    constructor(destiationStation: SpaceStationBehavior)
    {
        super();

        this.destinationStation = destiationStation;
        this.IsControlOverride = true;
    }
    
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
    }
    
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);
        
        this.inputSource.DesiredFacing = this.GameObject.Position.DirectionTo(this.destinationStation.NearestWaypoint.Position);
        this.inputSource.DeactivateAllModules();
        this.inputSource.ThrustAft = false;
        this.inputSource.ThrustFoward = false;
        this.inputSource.ThrustPort = false;
        this.inputSource.ThrustStarboard = false;

        var bearingToStation = this.GameObject.Position.BearingTo(this.GameObject.Facing, this.destinationStation.NearestWaypoint.Position);
        if (Math.abs(bearingToStation) < 0.01)
        {
            this.inputSource.ThrustFoward = true;
        }

        var distanceToTarget = this.GameObject.Position.DistanceTo(this.destinationStation.NearestWaypoint.Position);
        if (distanceToTarget < 100)
        {
            // Remove the docking ship
            zone.GameObjects.splice(zone.GameObjects.indexOf(this.GameObject), 1);
            // Uninit all of the ship components
            this.GameObject.ForceUninitialize();
            // Remove the sequencer, its job is done
            this.GameObject.RemoveComponent(zone, this);
            // Change the UI to "docked" mode
            state.SetupDockedUI(this.destinationStation);
        }
    }
}