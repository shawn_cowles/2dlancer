﻿/// <reference path="Actor.ts" />

// Spawns and respawns bots.
class BotSpawner extends Actor
{
    private loadout: ShipLoadout;
    private behavior: string;

    private bots: Ship[];

    private spawnDelay: number;
    private spawnTimer: number;
    private maxPopulation: number;

    constructor(
        position: Vector2,
        zone: Zone,
        loadout: ShipLoadout,
        behavior: string,
        spawnDelay: number,
        maxPopulation: number)
    {
        super(position, zone);

        this.loadout = loadout;
        this.behavior = behavior;
        this.spawnDelay = spawnDelay;
        this.spawnTimer = 0;
        this.maxPopulation = maxPopulation;
        this.bots = [];
    }

    public Update(state: FlightState, bgs: BackgroundSimulation, elapsedSeconds: number): void
    {
        this.spawnTimer -= elapsedSeconds;

        if (this.spawnTimer <= 0)
        {
            this.spawnTimer = this.spawnDelay;

            if (this.bots.length < this.maxPopulation)
            {
                var botShip = new Ship(
                    this.Position,
                    this.Zone,
                    this.loadout,
                    this.behavior);

                bgs.AddActor(botShip);
                this.bots.push(botShip);
            }
        }

        // cull dead bots
        for (var i = 0; i < this.bots.length; i++)
        {
            if (this.bots[i].IsDead)
            {
                this.bots.splice(i, 1);
            }
        }
    }

    // No GameObject representation
    public BuildGameObject(state: FlightState): GameObject
    {
        return null;
    }
}