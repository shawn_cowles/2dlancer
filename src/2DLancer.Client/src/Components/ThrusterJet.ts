﻿/// <reference path="AbstractComponent.ts" />

// Creates the visual effect of a thruster firing. Illustrates maneuvering thrusters at work.
class ThrusterJet extends AbstractComponent
{
    protected sprite: Phaser.Sprite;
    private offsetMagnitude: number;
    private offsetDirection: number;
    private tint: number;
    private facing: number;

    private inputSource: InputSource;
    private thrusters: Thrusters;
    
    constructor(offsetPoint: Vector2, tint: number, facing: number)
    {
        super();

        // Store the offset point in polar coordinates so it can track the game object properly as it turns
        this.offsetMagnitude = offsetPoint.Magnitude();
        this.offsetDirection = offsetPoint.Direction();
        this.tint = tint;
        this.facing = facing;
    }

    // create the sprite in phaser.
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
        this.thrusters = this.GameObject.GetComponent<Thrusters>(Thrusters);

        this.sprite = state.SpriteGroups[FlightState.LAYER_MIDGROUND].create(0, 0, "thruster_plume");
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = this.tint;
        this.sprite.scale.set(0.5, 0.5);
        this.sprite.visible = false;
    }

    // Handle visibility changes based on current thrusting
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        var point = Vector2.FromPolarOffset(this.offsetMagnitude, this.offsetDirection, this.GameObject);

        this.sprite.position.set(point.X, point.Y);
        this.sprite.rotation = this.facing + this.GameObject.Facing;

        var localFacingVector = Vector2.FromPolar(1, this.facing);

        // Directions appear to be wonky because it's based on the zero-rotation is facing right. So right (+x) on the sprite is going toward the bow
        this.sprite.visible = this.inputSource.ThrustFoward && localFacingVector.Dot(Vector2.RIGHT) > 0.5
            || this.inputSource.ThrustAft && localFacingVector.Dot(Vector2.LEFT) > 0.5
            || this.inputSource.ThrustStarboard && localFacingVector.Dot(Vector2.DOWN) > 0.5
            || this.inputSource.ThrustPort && localFacingVector.Dot(Vector2.UP) > 0.5;

        if (this.thrusters.IsBoosting())
        {
            this.sprite.scale.set(1, 1);
        }
        else
        {
            this.sprite.scale.set(0.5, 0.5);
        }
    }

    // When the component is removed, destroy the sprite
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.sprite != null)
        {
            this.sprite.destroy();
            this.sprite = null;
        }
    }
}