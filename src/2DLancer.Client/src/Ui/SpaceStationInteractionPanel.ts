﻿/// <reference path="UiPanel.ts" />

// A widget that displays an panel for interacting with a space station you are docked at.
class SpaceStationInteractionPanel extends UiPanel
{
    public static WIDTH = 800;
    public static HEIGHT = 400;

    private station: SpaceStationBehavior;
    private state: FlightState;
    private activeMenuPage: AbstractStationPage;
    private activePageInitailized: boolean;
    
    constructor(station: SpaceStationBehavior)
    {
        super(new Phaser.Point(0, 0), SpaceStationInteractionPanel.WIDTH, SpaceStationInteractionPanel.HEIGHT);
        this.station = station;
        this.activeMenuPage = null;
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        this.activeMenuPage = new MainStationPage();

        this.SetPage(new MainStationPage());
        
        var bodyTextStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };
    }

    // Update position and show/hide
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        if (!this.activePageInitailized)
        {
            this.activePageInitailized = true;
            this.activeMenuPage.Initialize(this, state, this.station);
        }

        this.CenterPosition = new Phaser.Point(
            state.game.camera.width / 2 + state.game.camera.view.x,
            state.game.camera.height / 2 + state.game.camera.view.y);

        var upperLeft = new Phaser.Point(
            this.CenterPosition.x - SpaceStationInteractionPanel.WIDTH / 2,
            this.CenterPosition.y - SpaceStationInteractionPanel.HEIGHT / 2);
        
        this.activeMenuPage.Update(state, upperLeft);
        
        this.ForceUpdatePosition();
    }
    
    public Destroy()
    {
        super.Destroy();

        if (this.activeMenuPage != null && this.activePageInitailized)
        {
            this.activeMenuPage.Destroy();
        }
    }

    public SetPage(newPage: AbstractStationPage): void
    {
        if (this.activeMenuPage != null && this.activePageInitailized)
        {
            this.activeMenuPage.Destroy();
        }

        this.activeMenuPage = newPage;
        this.activePageInitailized = false;
    }
}