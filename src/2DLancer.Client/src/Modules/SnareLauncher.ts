﻿/// <reference path="AbstractModule.ts" />

// The SnareLauncher launches homing snare mines.
class SnareLauncher extends AbstractModule
{
    private maxCooldown: number;
    private cooldownTimer: number;

    private offsetMagnitude: number;
    private offsetDirection: number;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.offsetMagnitude = offsetMagnitudes[0];
        this.offsetDirection = offsetDirections[0];

        this.maxCooldown = MissileLauncher.CooldownForSize(slot.Size);
        
        this.cooldownTimer = 0;
    }
    
    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = this.maxCooldown;

            this.DropSnare(zone, state, parentShip);
        }
    }

    public UnInitialize(parentShip: GameObject): void
    {
    }
    
    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / this.maxCooldown, 1, 0);
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public IconSprite(): string
    {
        return "icon_mine_launcher";
    }

    public Name(): string
    {
        return "Snare";
    }

    public GetListing(): AbstractModuleListing
    {
        return new SnareLauncherListing();
    }

    private DropSnare(zone: Zone, state: FlightState, parentShip: GameObject): void
    {
        var barrelPos = Vector2.FromPolarOffset(
            this.offsetMagnitude,
            this.offsetDirection,
            parentShip);

        var missile = new GameObject(barrelPos, parentShip.Facing);
        missile.AddComponent(new SnareMineBehavior());

        missile.AddComponent(new SpriteDisplay(
            "mine",
            FlightState.LAYER_MIDGROUND,
            HexColor.WHITE,
            1));
        //missile.AddComponent(new Collider(
        //    [new CollisionCircle(Vector2.ZERO, 4)],
        //    true,
        //    parentShip));
        missile.AddComponent(new TimedLife(20, true));
        missile.AddComponent(new FactionIdentifier(parentShip.GetComponent<FactionIdentifier>(FactionIdentifier).FactionId));
        missile.AddComponent(new ExplodeOnDeath());
        missile.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_PROJECTILE));

        zone.GameObjects.push(missile);
    }
    
    public static CooldownForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 15;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 10;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 5;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }
}

class SnareLauncherListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Snare";
        this.BaseCost = 100;
        this.Type = _2DLancer.Common.Data.ModuleType.Bay;
    }

    public BuildModule(): AbstractModule
    {
        return new SnareLauncher();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        var cooldown = MissileLauncher.CooldownForSize(moduleSize);

        return [
            "Cooldown: " + cooldown + "s"
            + "  Slowdown: " + (SnareMineBehavior.SNARE_PENALTY * 100) + "%",

            "This module drops snare mines. These useful devices attach to any nearby enemy ships and slow them down, giving an advantage in combat or allowing a freighter time to escape an attack. The more snares on an enemy, the slower they go."
        ];
    }
}