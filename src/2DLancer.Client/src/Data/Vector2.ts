﻿// A Vector combines a horizontal and vertical component. They can be used for positions and velocity, and simplify doing physics calculations.
class Vector2
{
    public static ZERO = new Vector2(0, 0);
    public static UP = new Vector2(0, -1);
    public static DOWN = new Vector2(0, 1);
    public static RIGHT = new Vector2(1, 0);
    public static LEFT = new Vector2(-1, 0);

    public X: number;
    public Y: number;

    constructor(x: number, y: number)
    {
        this.X = x;
        this.Y = y;
    }

    public Magnitude(): number
    {
        return Math.sqrt(this.X * this.X + this.Y * this.Y);
    }

    public Direction(): number
    {
        return Math.atan2(this.Y, this.X);
    }

    public ToPoint(): Phaser.Point
    {
        return new Phaser.Point(this.X, this.Y);
    }

    public Add(other: Vector2): Vector2
    {
        return new Vector2(this.X + other.X, this.Y + other.Y);
    }

    public Subtract(other: Vector2): Vector2
    {
        return new Vector2(this.X - other.X, this.Y - other.Y);
    }

    public Dot(other: Vector2): number
    {
        return this.X * other.X + this.Y * other.Y;
    }

    public Normalize(): Vector2
    {
        return Vector2.FromPolar(1, this.Direction());
    }

    public Scale(factor: number): Vector2
    {
        return Vector2.FromPolar(this.Magnitude() * factor, this.Direction());
    }

    public Mod(divisor: number): Vector2
    {
        return new Vector2(
            this.X % divisor,
            this.Y % divisor);
    }
    
    public DistanceTo(other: Vector2): number
    {
        var dx = this.X - other.X;
        var dy = this.Y - other.Y;

        return Math.sqrt(dx * dx + dy * dy);
    }

    public DirectionTo(other: Vector2): number
    {
        var dx = other.X - this.X;
        var dy = other.Y - this.Y;

        return Math.atan2(dy, dx);
    }

    public BearingTo(ownFacing: number, other: Vector2): number
    {
        var direction = this.DirectionTo(other);

        var raw = direction - ownFacing;

        if (raw > Math.PI)
        {
            return raw - 2 * Math.PI;
        }

        if (raw < -Math.PI)
        {
            return raw + 2 * Math.PI;
        }

        return raw;
    }

    public static FromPolar(magnitude: number, direction: number): Vector2
    {
        var x = magnitude * Math.cos(direction);
        var y = magnitude * Math.sin(direction);

        return new Vector2(x, y);
    }

    public static FromPolarOffset(magnitude: number, direction: number, basis: GameObject): Vector2
    {
        var raw = Vector2.FromPolar(magnitude, direction + basis.Facing);

        return raw.Add(basis.Position);
    }

    public static FromPoint(point: Phaser.Point): Vector2
    {
        return new Vector2(point.x, point.y);
    }

    public static FromPosition(position: IPosition): Vector2
    {
        return new Vector2(position.X, position.Y);
    }
}