﻿using System;
using _2DLancer.Server.Interfaces;
using log4net;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// A provider of log4net based <see cref="ILogger"/>s.
    /// This is initialized in AssemblyInfo.cs
    /// </summary>
    public class Log4NetLoggerProvider : ILoggerProvider
    {
        /// <summary>
        /// Get a logger for a type.
        /// </summary>
        /// <param name="t">The type to create a logger for.</param>
        /// <returns>A logger for the provided type.</returns>
        public ILogger GetLoggerFor(Type t)
        {
            return new Log4NetLogger(LogManager.GetLogger(t));
        }
    }
}
