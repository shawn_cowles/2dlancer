﻿namespace _2DLancer.Server.Interfaces
{
    /// <summary>
    /// Any service that needs to be initialized before use.
    /// </summary>
    public interface IInitializableService
    {
        /// <summary>
        /// Initialize the service.
        /// </summary>
        void Initialize();
    }
}
