﻿/// <reference path="UiPanel.ts" />

// A widget that displays a message log to the player.
class MessageLogDisplay extends AbstractUiWidget
{
    public static WIDTH = 200;
    
    private state: FlightState;

    private logText: Phaser.Text;
    
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "16px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.logText = state.game.add.text(0, 0, "", textStyle);
        this.logText.anchor.set(0, 0);
    }
    
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        var xPosition = state.game.camera.width - MessageLogDisplay.WIDTH + state.game.camera.view.x;
        var yPosition = state.game.camera.view.y
            + CurrentLocationLabel.HEIGHT
            + MiniMap.MINIMAP_SIZE
            + PlayerStatusPanel.HEIGHT
            + 30;
        
        this.logText.position.set(
            xPosition,
            yPosition);

        this.logText.text = "";
        var messages = state.MessageLog.GetMessages();
        for (var i = 0; i < messages.length; i++)
        {
            this.logText.text +=
                Util.WrapText(messages[i], 20)
                + "\n";
        }
    }

    public Destroy()
    {
        super.Destroy();

        this.logText.destroy();
    }

    // Determine if a point is over a UI element, used to suppress shooting when the player
    // is using the ui
    public PointIsInPanel(point: Vector2): boolean
    {
        return false;
    }
}