﻿class UserConnectedMessage implements IUserConnectedMessage
{
    public MessageName = "UserConnectedMessage";

    public UserId: string;

    constructor(userId: string)
    {
        this.UserId = userId;
    }
}