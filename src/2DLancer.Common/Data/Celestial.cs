﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// A static celestial object.
    /// </summary>
    [TsClass(Module ="", Name ="ICelestial")]
    public class Celestial
    {
        /// <summary>
        /// The position of the celestial.
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// The type of the celestial.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The subtype of the celestial.
        /// </summary>
        public string Subtype { get; set; }
    }
}
