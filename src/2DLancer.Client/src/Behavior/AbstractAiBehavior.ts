﻿// Base class for any AI behavior
abstract class AbstractAiBehavior
{
    // Initialize the behavior
    public abstract Initialize(control: CompositeBotControl, zone: Zone): void;

    // Perform an update, returns true if the behavior applies, false otherwise
    public abstract Update(
        control: CompositeBotControl,
        state: FlightState,
        zone: Zone,
        inputSource: InputSource,
        elapsedSeconds: number): boolean;

    public abstract ControlShipActor(
        ship: Ship,
        state: FlightState,
        elapsedSeconds: number): boolean;
}