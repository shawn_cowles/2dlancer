﻿using System;
using System.Linq;
using _2DLancer.Common.Messages;
using _2DLancer.Server.Interfaces;
using Newtonsoft.Json;
using SuperSocket.SocketBase;
using SuperWebSocket;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// A websocket based connection service that provides a connection to the clients.
    /// This serves as a bridge between the backend system and clients, moving messages 
    /// between websockets and the message bus.
    /// </summary>
    public class WebSocketConnectionService : AbstractBusService, IInitializableService
    {
        private readonly ISettingsProvider _settingsProvider;
        private WebSocketServer _socketServer;
        private ILogger _logger;

        /// <summary>
        /// Construct a new WebsocketConnectionService.
        /// </summary>
        /// <param name="settingsProvider">The settings provider to use.</param>
        /// <param name="loggerProvider">The logger provider to use.</param>
        public WebSocketConnectionService(
            ISettingsProvider settingsProvider,
            Interfaces.ILoggerProvider loggerProvider)
            : base(MessagePredicate)
        {
            _settingsProvider = settingsProvider;
            _logger = loggerProvider.GetLoggerFor(typeof(WebSocketConnectionService));
        }

        /// <summary>
        /// Initialize the websocket server.
        /// </summary>
        public void Initialize()
        {
            _socketServer = new WebSocketServer();
            _socketServer.Setup(_settingsProvider.GetWebsocketConfiguration().Servers.First());

            _socketServer.NewMessageReceived += MessageRecieved;
            _socketServer.NewSessionConnected += NewConnection;
            _socketServer.SessionClosed += ClosedConnection;
        }

        private void NewConnection(WebSocketSession session)
        {
            _logger.Info("New Connection");
        }

        private void ClosedConnection(WebSocketSession session, CloseReason value)
        {
            _logger.Info("Closed Connection");
            SendMessage(new UserDisconnectedMessage(session.SessionID));
        }

        /// <summary>
        /// Start listening for new connections.
        /// </summary>
        /// <param name="bus">The message bus to send and receive messages to the system.</param>
        public override void Start(MessageBus bus)
        {
            _socketServer.Start();
            _logger.Info("Listening on port " + _socketServer.Config.Port);

            base.Start(bus);
        }

        /// <summary>
        /// Stop listening for connections, and close any existing connections.
        /// </summary>
        public override void Stop()
        {
            base.Stop();

            _socketServer.Stop();
        }

        private void MessageRecieved(WebSocketSession session, string json)
        {
            try
            {
                var commaIndex = json.IndexOf(',');

                var messageType = json.Substring(0, commaIndex);
                var body = json.Substring(commaIndex + 1);

                // really noisy
                //_logger.Debug("Received message: " + messageType);

                var message = AbstractIncomingClientMessage.Deserialize(messageType, body, session.SessionID);

                if (message != null)
                {
                    // dump incoming messages onto the bus
                    SendMessage(message);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// Handle messages from the bus.
        /// </summary>
        /// <param name="messages">The incoming messages.</param>
        protected override void HandleMessages(AbstractMessage[] messages)
        {
            try
            {
                // forward outgoing messages to the clients
                foreach (var outgoingMessage in messages.OfType<AbstractOutgoingClientMessage>())
                {
                    var messageBody = outgoingMessage.MessageName + ","
                        + JsonConvert.SerializeObject(outgoingMessage);

                    // really noisy 
                    //_logger.Debug("Sending message: " + outgoingMessage.MessageName);

                    foreach (var id in outgoingMessage.ClientIds)
                    {
                        var session = _socketServer.GetSessionByID(id);

                        if(session != null)
                        {
                            _socketServer.GetSessionByID(id).Send(messageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private static bool MessagePredicate(AbstractMessage message)
        {
            return message is AbstractOutgoingClientMessage;
        }
    }
}
