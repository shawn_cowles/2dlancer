﻿/// <reference path="AbstractComponent.ts" />

// 
class ShieldGenerator extends AbstractComponent
{
    private collider: Collider;
    private radius: number;
    private maxStrength: number;
    private strength: number;
    private colliderAdded: boolean;
    private regeneration: number;
    private regenDelayTimer: number;

    private boostRemaining: number;

    private boostSound: SoundEffect;

    constructor(size: number, radius: number)
    {
        super();

        this.collider = new Collider(
            [new CollisionCircle(Vector2.ZERO, radius)],
            false);
        this.radius = radius;
        this.maxStrength = DesignConstants.ShieldHealthPerSize * size;
        this.strength = this.maxStrength;
        this.colliderAdded = false;
        this.regeneration = DesignConstants.ShieldRegenPerSize * size;
        this.regenDelayTimer = 0;
        this.boostRemaining = 0;
    }

    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        if (this.boostSound == null)
        {
            this.boostSound = new SoundEffect("shield_boost", 0.5);
            this.boostSound.AutoPlay = false;
            this.GameObject.AddComponent(this.boostSound);
        }
    }

    // Draw the shield
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);
        
        this.regenDelayTimer -= elapsedSeconds;

        this.boostRemaining -= elapsedSeconds;

        if (this.regenDelayTimer <= 0 || this.boostRemaining > 0)
        {
            var regenAmount = this.regeneration * elapsedSeconds

            if (this.boostRemaining > 0)
            {
                regenAmount *= ShieldBooster.BOOST_FACTOR;
            }
            
            this.strength = Math.min(this.maxStrength, this.strength + regenAmount);
        }

        if (this.strength > 0 && !this.colliderAdded)
        {
            this.collider = new Collider(
                [new CollisionCircle(Vector2.ZERO, this.radius)],
                false);

            this.GameObject.AddComponent(this.collider);
            this.colliderAdded = true;
        }

        if (this.strength <= 0 && this.colliderAdded)
        {
            this.GameObject.RemoveComponent(zone, this.collider);
            this.colliderAdded = false;
        }

        if (this.strength > 0)
        {
            state.Graphics.lineStyle(this.strength / 5, HexColor.CYAN, 0.5);
            state.Graphics.drawCircle(
                this.GameObject.Position.X,
                this.GameObject.Position.Y,
                this.radius * 2);
        }
    }

    public ShieldsUp(): boolean
    {
        return this.strength > 0;
    }

    public DealDamage(damage: number): void
    {
        this.regenDelayTimer = DesignConstants.ShieldRechargeDelay;
        this.strength = Math.max(0, this.strength - damage);
    }

    public StrengthPercent(): number
    {
        return Math.round(this.strength / this.maxStrength * 100);
    }

    public TriggerBoost(duration: number): void
    {
        this.boostSound.PlayOnce();
        this.boostRemaining = duration;
    }

    public ForceDropShields(): void
    {
        this.regenDelayTimer = DesignConstants.ShieldRechargeDelay;
        this.strength = 0;
        this.boostRemaining = 0;
    }
}