﻿/// <reference path="AbstractComponent.ts" />

// SmokeEmitter wraps a particle effect.
class SmokeEmitter extends AbstractComponent
{
    private emitter: Phaser.Particles.Arcade.Emitter;
    private offsetMagnitude: number;
    private offsetDirection: number;

    constructor(offset: Vector2)
    {
        super();

        this.offsetMagnitude = offset.Magnitude();
        this.offsetDirection = offset.Direction();
    }

    // Create the particle emitter
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.emitter = new Phaser.Particles.Arcade.Emitter(state.game);
        this.emitter = state.SpriteGroups[FlightState.LAYER_MIDGROUND].add(this.emitter);

        this.emitter.particleDrag.set(1, 1);
        this.emitter.makeParticles("cloud_1");
        
        this.emitter.setXSpeed(-50, 50);
        this.emitter.setYSpeed(-50, 50);
        this.emitter.setRotation(-50, 50);

        this.emitter.setAlpha(1, 0, 400);
        this.emitter.setScale(0.5, 1, 0.5, 1, 300);
        this.emitter.gravity = 0;
        
        this.emitter.start(false, 400, 15);
    }

    // Perform an active mode simulation update
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        var position = Vector2.FromPolarOffset(
            this.offsetMagnitude,
            this.offsetDirection,
            this.GameObject);

        this.emitter.emitX = position.X;
        this.emitter.emitY = position.Y;
    }
    
    // Uninitialize the component from active sim mode.
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.emitter != null)
        {
            this.emitter.destroy();
            this.emitter = null;
        }
    }
}