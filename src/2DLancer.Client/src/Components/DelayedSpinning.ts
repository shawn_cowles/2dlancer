﻿/// <reference path="AbstractComponent.ts" />

// DelayedSpinning starts a GameObject spinning after a delay.
class DelayedSpinning extends AbstractComponent
{
    private delay: number;
    private rotationRate: number;

    constructor(delay: number, rotationRate: number)
    {
        super();

        this.delay = delay;
        this.rotationRate = rotationRate;
    }

    // Count down the timer and apply the impulse
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.delay >= 0)
        {
            this.delay -= elapsedSeconds;
        }
        else
        {
            this.GameObject.Facing = Util.CorrectAngle(this.GameObject.Facing + this.rotationRate * elapsedSeconds);
        }
    }
}