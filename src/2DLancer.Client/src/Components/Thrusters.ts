﻿/// <reference path="AbstractComponent.ts" />

// Thrusters handle moving and turning a GameObject based on the local input source
class Thrusters extends AbstractComponent
{
    private inputSource: InputSource;
    private acceleration: number;
    private drag: number;
    private turnRate: number;
    private size: number;

    private engineSound: SoundEffect;

    private boostRemaining: number;
    private snares: SnareMineBehavior[];

    constructor(size: number, drag: number)
    {
        super();

        this.size = size;
        this.drag = drag;
        this.snares = [];
    }

    // grab the local input source and calculate performance
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.inputSource = this.GameObject.GetComponent<InputSource>(InputSource);
        
        this.acceleration = DesignConstants.EngineThrustPerSize * this.size / this.GameObject.PhysicsBody.Mass;
        
        this.turnRate = DesignConstants.EngineTorquePerSize * this.size / this.GameObject.PhysicsBody.Mass;

        if (this.engineSound == null)
        {
            this.engineSound = new SoundEffect("engine_base", 0.5, true, 3);
            this.GameObject.AddComponent(this.engineSound);
        }
    }
    
    // accelerate the game object
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.boostRemaining -= elapsedSeconds;

        var adjustedAcceleration = this.acceleration * elapsedSeconds;
        var adjustedTurnRate = this.turnRate;

        if (this.IsBoosting())
        {
            adjustedAcceleration *= EngineBooster.BOOST_FACTOR;
        }

        for (var i = 0; i < this.snares.length; i++)
        {
            adjustedAcceleration *= (1 - SnareMineBehavior.SNARE_PENALTY);
            adjustedTurnRate *= (1 - SnareMineBehavior.SNARE_PENALTY);
        }

        // Turn
        var maxTurn = adjustedTurnRate * elapsedSeconds;
        var facing = Util.CorrectAngle(this.GameObject.Facing);
        var delta = this.inputSource.DesiredFacing - facing;
        var correctedDelta = Util.CorrectAngle(delta);
        var turn = Util.Clamp(correctedDelta, maxTurn, -maxTurn);
        this.GameObject.Facing = Util.CorrectAngle(facing + turn);
        
        //Audio
        var mainThrustVolume = 0;
        var lateralThrustVolume = 0;

        

        // Thrusting
        if (this.inputSource.ThrustFoward)
        {
            this.GameObject.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity
                .Add(Vector2.FromPolar(
                    adjustedAcceleration,
                    this.GameObject.Facing));

            mainThrustVolume = 0.2;

            if (this.IsBoosting())
            {
                mainThrustVolume = 0.5;
            }
        }

        if (this.inputSource.ThrustAft)
        {
            this.GameObject.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity
                .Add(Vector2.FromPolar(
                    adjustedAcceleration * DesignConstants.LateralAccelerationFactor,
                    this.GameObject.Facing + Math.PI));

            lateralThrustVolume = 0.1;
        }

        if (this.inputSource.ThrustStarboard)
        {
            this.GameObject.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity
                .Add(Vector2.FromPolar(
                    adjustedAcceleration * DesignConstants.LateralAccelerationFactor,
                    this.GameObject.Facing + Math.PI / 2));

            lateralThrustVolume = 0.1;
        }

        if (this.inputSource.ThrustPort)
        {
            this.GameObject.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity
                .Add(Vector2.FromPolar(
                    adjustedAcceleration * DesignConstants.LateralAccelerationFactor,
                    this.GameObject.Facing - Math.PI / 2));

            lateralThrustVolume = 0.1;
        }

        // Apply Drag
        this.GameObject.PhysicsBody.Velocity = this.GameObject.PhysicsBody.Velocity
            .Add(Vector2.FromPolar(
                -1 * this.drag * this.GameObject.PhysicsBody.Velocity.Magnitude() * elapsedSeconds,
                this.GameObject.PhysicsBody.Velocity.Direction()));

        this.engineSound.FadeToVolume(mainThrustVolume + lateralThrustVolume);
    }
    
    public TriggerBoost(duration: number): void
    {
        this.boostRemaining = duration;
    }

    public IsBoosting(): boolean
    {
        return this.boostRemaining > 0;
    }

    public AddSnare(snare: SnareMineBehavior): void
    {
        if (this.snares.indexOf(snare) < 0)
        {
            this.snares.push(snare);
        }
    }

    public RemoveSnare(snare: SnareMineBehavior): void
    {
        var index = this.snares.indexOf(snare);

        if (index >= 0)
        {
            this.snares.splice(index, 1);
        }
    }
}