﻿/// <reference path="UiPanel.ts" />

// A widget that displays an interaction panel for using nav bouys.
class NavBouyPanel extends UiPanel
{
    private static WIDTH = 300;
    private static HEIGHT = 100;

    private state: FlightState;

    private text: Phaser.Text;
    private jumpButton: TextButton;
    
    private proximityLink: ZoneLink;
    
    constructor()
    {
        super(new Phaser.Point(0, 0), NavBouyPanel.WIDTH, NavBouyPanel.HEIGHT);
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.text = state.game.add.text(0, 0, "", textStyle);
        this.text.anchor.set(0.5, 0.5);
        this.jumpButton = new TextButton(state.game, 0, 0, "Jump",
            () =>
            {
                if (this.proximityLink == null)
                {
                    return;
                }

                state.PlayerShip.AddComponent(new JumpSequencer(
                    this.proximityLink.TargetZone,
                    this.proximityLink.TargetPosition));
            },
            this);
        this.jumpButton.anchor.set(0.5, 0.5);

        this.SetVisible(false);
    }
    
    // Update position and show/hide
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        var mousePos = new Vector2(
            state.input.activePointer.worldX,
            state.input.activePointer.worldY);

        this.proximityLink = null;
        var nearDist = 100; // functionally the minimum distance

        if (state.PlayerShip != null
            && !state.PlayerShip.ShouldBeRemoved
            && !state.PlayerShip.HasComponent(JumpSequencer))
        {
            for (var i = 0; i < state.GetActiveZone().ZoneLinks.length; i++)
            {
                var dist = state.GetActiveZone().ZoneLinks[i].GameObject.Position.DistanceTo(mousePos);
                
                if (dist < nearDist)
                {
                    this.proximityLink = state.GetActiveZone().ZoneLinks[i];
                    nearDist = dist;
                }
            }
        }
        
        if (this.proximityLink != null)
        {
            this.text.visible = true;
            this.jumpButton.visible = true;
            this.SetVisible(true);

            this.text.text = this.proximityLink.Name;
            
            this.CenterPosition = this.proximityLink.GameObject.Position.ToPoint();
            
            this.text.position.set(
                this.CenterPosition.x,
                this.CenterPosition.y - 40);

            this.jumpButton.position.set(
                this.CenterPosition.x,
                this.CenterPosition.y + 20);

            this.ForceUpdatePosition();
        }
        else
        {
            this.text.visible = false;
            this.jumpButton.visible = false;
            this.SetVisible(false);
        }
    }

    public Destroy()
    {
        super.Destroy();

        this.text.destroy();
        this.jumpButton.destroy();
    }
}