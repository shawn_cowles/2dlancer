﻿/// <reference path="AbstractComponent.ts" />

// Links to another zone
class ZoneLink extends AbstractComponent
{
    public Name: string;
    public TargetZone: string;
    public TargetPosition: Vector2;
    
    constructor(name: string, targetZone: string, targetPosition: Vector2)
    {
        super();

        this.Name = name;
        this.TargetZone = targetZone;
        this.TargetPosition = targetPosition;
    }
}