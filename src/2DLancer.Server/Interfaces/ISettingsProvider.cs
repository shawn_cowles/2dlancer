﻿using SuperSocket.SocketBase.Config;

namespace _2DLancer.Server.Interfaces
{
    /// <summary>
    /// A provider of configuration settings.
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// The directory for loading and storing data.
        /// </summary>
        string GetDataDirectory { get; }

        /// <summary>
        /// Get the websocket configuration section. 
        /// </summary>
        /// <returns> The websocket configuration section.</returns>
        IConfigurationSource GetWebsocketConfiguration();
    }
}
