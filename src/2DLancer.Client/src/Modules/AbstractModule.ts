﻿// Abstract base class for all modules that can be mounted in a ModuleSlot
abstract class AbstractModule
{
    // Initialize the module before use;
    public abstract Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void;

    // Update the module in active simulation mode
    public abstract Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void;
    
    // Do any cleanup from module removal
    public abstract UnInitialize(parentShip: GameObject): void;
    
    public abstract CooldownPercent(): number;

    public abstract CanBeActivated(): boolean;

    // The name of the icon sprite for the button to activate this module
    public abstract IconSprite(): string;

    public abstract Name(): string;

    public abstract GetListing(): AbstractModuleListing;
}

abstract class AbstractModuleListing
{
    public Name: string;
    
    public BaseCost: number;

    public Type: _2DLancer.Common.Data.ModuleType;
    
    public abstract BuildModule(): AbstractModule;

    public abstract GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[];
    
    public CostForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 1 * this.BaseCost;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 2 * this.BaseCost;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 4 * this.BaseCost;
            default:
                throw "Unknown module size " + moduleSize;
        }
    }
    
    public static GetAllListings(): AbstractModuleListing[]
    {
        return [
            new EngineBoosterListing(),
            new MissileLauncherListing(),
            new SnareLauncherListing(),
            new ShieldBoosterListing(),
            new EmpBurstListing(),
            new AutoturretListing(),
            new AutocannonListing(),
            new LaserCannonListing()
        ];
    }
    
    public static GetByName(listingName: string): AbstractModuleListing
    {
        var allListings = AbstractModuleListing.GetAllListings();

        for (var i = 0; i < allListings.length; i++)
        {
            if (allListings[i].Name == listingName)
            {
                return allListings[i];
            }
        }

        // Handle failures more gracefully, so as to not crash while loading
        return null;
    }
}