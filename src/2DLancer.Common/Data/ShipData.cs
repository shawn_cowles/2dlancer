﻿using System.Collections.Generic;
using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Ship class information.
    /// </summary>
    [TsClass(Module = "", Name = "IShipData")]
    public class ShipData
    {
        /// <summary>
        /// The name of the ship class.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The scale of the ship.
        /// </summary>
        public double Scale { get; set; }

        /// <summary>
        /// The engines of the ship.
        /// </summary>
        public List<Engine> Engines { get; private set; }

        /// <summary>
        /// The thrusters of the ship.
        /// </summary>
        public List<Thruster> Thrusters { get; private set; }
        
        /// <summary>
        /// The ship's colliders.
        /// </summary>
        public List<Collider> Colliders { get; private set; }

        /// <summary>
        /// The radius of the ship's shield.
        /// </summary>
        public double ShieldRadius { get; set; }
        
        /// <summary>
        /// The maximum engine class of the ship.
        /// </summary>
        public int MaxEngineClass { get; set; }

        /// <summary>
        /// The maximum shield class of the ship.
        /// </summary>
        public int MaxShieldClass { get; set; }

        /// <summary>
        /// The mass of the ship.
        /// </summary>
        public double Mass { get; set; }

        /// <summary>
        /// The base sprite of the ship.
        /// </summary>
        public string BaseSprite { get; set; }

        /// <summary>
        /// The paint sprite of the ship.
        /// </summary>
        public string PaintSprite { get; set; }

        /// <summary>
        /// The health of the ship.
        /// </summary>
        public int Health { get; set; }

        /// <summary>
        /// The scaled width of the ship's sprite.
        /// </summary>
        public int SpriteWidth { get; set; }

        /// <summary>
        /// The scaled height of the ship's sprite.
        /// </summary>
        public int SpriteHeight { get; set; }

        /// <summary>
        /// The drag coefficient of the ship.
        /// </summary>
        public double Drag { get; set; }

        /// <summary>
        /// Information on the debris the ship leaves behind after exploding.
        /// </summary>
        public List<DebrisInfo> Debris { get; private set; }

        /// <summary>
        /// Module mounting slots on the ship.
        /// </summary>
        public List<ModuleSlot> ModuleSlots { get; private set; }

        /// <summary>
        /// A fluffy description of the ship.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The base cost of the ship.
        /// </summary>
        public double Cost { get; set; }

        /// <summary>
        /// Does the ship have turreted weapons?
        /// </summary>
        public bool TurretedWeapons { get; set; }

        /// <summary>
        /// Construct a new ShipData
        /// </summary>
        public ShipData()
        {
            Engines = new List<Engine>();
            Thrusters = new List<Thruster>();
            Colliders = new List<Collider>();
            Debris = new List<DebrisInfo>();
            ModuleSlots = new List<ModuleSlot>()
            {
                new ModuleSlot()
                {
                    Size = ModuleSize.None,
                    Type = ModuleType.PrimaryWeapon
                },
                new ModuleSlot()
                {
                    Size = ModuleSize.None,
                    Type = ModuleType.SecondaryWeapon
                },
                new ModuleSlot()
                {
                    Size = ModuleSize.None,
                    Type = ModuleType.Bay
                },
                new ModuleSlot()
                {
                    Size = ModuleSize.None,
                    Type = ModuleType.Booster
                },
            };
        }
    }
}
