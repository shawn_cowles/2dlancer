﻿/// <reference path="AbstractComponent.ts" />

// A component that displays a sprite
class SpriteDisplay extends AbstractComponent
{
    protected spriteName: string;
    protected spriteLayer: number;
    protected tint: number;
    protected sprite: Phaser.Sprite;
    protected scale: number;

    constructor(spriteName: string, spriteLayer:number,tint:number,scale:number)
    {
        super();
        this.spriteName = spriteName;
        this.spriteLayer = spriteLayer;
        this.tint = tint;
        this.scale = scale;
    }

    // initialize the sprite display by creating the sprite in phaser
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.sprite = state.SpriteGroups[this.spriteLayer].create(0, 0, this.spriteName);
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = this.tint;
        this.sprite.scale = new Phaser.Point(this.scale, this.scale);
    }

    // Perform a simulation update
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        // have the sprite follow the game object
        this.sprite.position.set(this.GameObject.Position.X, this.GameObject.Position.Y);
        this.sprite.rotation = this.GameObject.Facing;
    }

    // Clean up the sprite
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.sprite != null)
        {
            this.sprite.destroy();
            this.sprite = null;
        }
    }
}