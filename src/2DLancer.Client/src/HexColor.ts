﻿class HexColor
{
    public static WHITE = 0xFFFFFF;

    public static RED = 0xFF0000;

    public static BLUE = 0x0000FF;

    public static GREEN = 0x00FF00;

    public static CYAN = 0x00FFFF;

    public static YELLOW = 0xFFFF00;

    public static PURPLE = 0xFF00FF;

    public static ORANGE = 0xFF8000;

    public static WINDOW_BORDER = 0x3e81ff;

    public static WINDOW_BACKGROUND = 0x2e4080;
}