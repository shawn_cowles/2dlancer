﻿/// <reference path="AbstractComponent.ts" />

// Fading text shows a short piece of text that fades away. Useful for brief in-game-world 
// notifications.
class FadingText extends AbstractComponent
{
    private text: string;
    private maxDuration: number;
    private remainingDuration: number;

    private textObj: Phaser.Text;

    constructor(text: string, duration: number)
    {
        super();

        this.text = text;
        this.maxDuration = duration;
        this.remainingDuration = duration;
    }

    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        var textStyle = {
            font: "16px courier",
            fill: "#00FFFF",
            align: "left",
        };

        this.textObj = state.game.add.text(0, 0, this.text, textStyle);
        this.textObj.anchor.set(0.5, 0.5);
    }

    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        this.remainingDuration -= elapsedSeconds;

        this.textObj.position.set(
            this.GameObject.Position.X,
            this.GameObject.Position.Y);

        this.textObj.alpha = Util.Clamp(this.remainingDuration / this.maxDuration, 1, 0);
    }
    
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.textObj != null)
        {
            this.textObj.destroy();
            this.textObj = null;
        }
    }
}