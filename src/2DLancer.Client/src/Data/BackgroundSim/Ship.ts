﻿/// <reference path="Actor.ts" />

// A ship is an Actor that represents a ship.
class Ship extends Actor
{
    private loadout: ShipLoadout;
    private targetZone: Zone;

    private control: CompositeBotControl;

    public FactionId: string;

    private statsCalculated: boolean;
    private dps: number;
    private speed: number;
    private maxHealth: number;
    public Health: number;
    private facingTowardGoal: number;

    constructor(position: Vector2, zone: Zone, loadout: ShipLoadout, behaviorName: string)
    {
        super(position, zone);

        this.loadout = loadout;
        this.targetZone = null;
        this.FactionId = loadout.FactionId;
        this.facingTowardGoal = 0;

        this.control = this.BuildBehavior(behaviorName);

        this.statsCalculated = false;
        this.dps = 0;
        this.speed = 0;
    }

    public Update(state: FlightState, bgs: BackgroundSimulation, elapsedSeconds: number): void
    {
        if (!this.statsCalculated)
        {
            this.CalculateStats(state);
        }

        if (this.Health <= 0)
        {
            this.IsDead = true;
        }

        if (this.targetZone != null)
        {
            bgs.MoveToZone(this, this.targetZone);
            this.targetZone = null;
        }

        // if ship isn't in the active zone, update it
        if (this.Zone != state.GetActiveZone())
        {
            this.control.ControlShipActor(this, state, elapsedSeconds);
        }
    }
    
    public BuildGameObject(state: FlightState): GameObject
    {
        var ship = state.ShipBuilder.BuildShip(
            this.Position,
            this.facingTowardGoal,
            this.loadout,
            () => this.control);
        ship.AddComponent(new ActorLink(this));
        ship.GetComponent<PhysicsBody>(PhysicsBody).Velocity = Vector2.FromPolar(
            this.speed,
            this.facingTowardGoal);

        if (this.Health < this.maxHealth)
        {
            // update GameObject representation to have the appropriate health
            ship.GetComponent<HullHealth>(HullHealth).DealDamage(this.maxHealth - this.Health);
        }

        return ship;
    }

    public TransitionToZone(position: Vector2, zone: Zone): void
    {
        this.Position = position;
        this.targetZone = zone;
    }
    
    public Attack(target: Ship, elapsedSeconds: number): void
    {
        target.Health -= (this.dps * elapsedSeconds);
        this.facingTowardGoal = this.Position.DirectionTo(target.Position);
    }

    public MoveTo(destination: Vector2, elapsedSeconds: number): void
    {
        var toDest = destination.Subtract(this.Position);

        var movement = Vector2.FromPolar(
            Math.min(toDest.Magnitude(), this.speed * elapsedSeconds),
            toDest.Direction())

        this.Position = this.Position.Add(movement);
        this.facingTowardGoal = toDest.Direction();
    }
    

    private BuildBehavior(behaviorName: string): CompositeBotControl
    {
        switch (behaviorName)
        {
            case "patrol":
                return new CompositeBotControl(
                    [
                        new ObjectAvoidanceBehavior(),
                        new AttackNearbyBehavior(),
                        new NavMeshWanderBehavior()
                    ]);
            case "trade":
                return new CompositeBotControl(
                    [
                        new ObjectAvoidanceBehavior(),
                        new AttackNearbyBehavior(),
                        new StationWanderBehavior()
                    ]);
            default:
                throw "Unknown behavior name: " + behaviorName;
        }
    }

    private CalculateStats(state: FlightState): void
    {
        this.statsCalculated = true;

        var shipData = state.ShipBuilder.GetShipData(this.loadout.ShipClass);
        this.speed = (shipData.MaxEngineClass * DesignConstants.EngineThrustPerSize / shipData.Mass) / shipData.Drag;
        this.maxHealth = shipData.Health;
        this.Health = this.maxHealth;
        this.dps = 0;

        for (var i = 0; i < shipData.ModuleSlots.length; i++)
        {
            if (shipData.ModuleSlots[i].Type == _2DLancer.Common.Data.ModuleType.PrimaryWeapon)
            {
                this.dps = Autocannon.DpsForModuleSize(shipData.ModuleSlots[i].Size);
            }
        }
    }
}