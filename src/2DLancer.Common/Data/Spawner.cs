﻿using TypeLite;

namespace _2DLancer.Common.Data
{
    /// <summary>
    /// Information for spawning bots.
    /// </summary>
    [TsClass(Module ="", Name = "ISpawner")]
    public class Spawner
    {
        /// <summary>
        /// The position of the spawner.
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// The direction spawned bots should face.
        /// </summary>
        public double Facing { get; set; }

        /// <summary>
        /// The faction for ships created by this spawner.
        /// </summary>
        public string Faction { get; set; }
        
        /// <summary>
        /// The color for ships created by this spawner.
        /// </summary>
        public int Color { get; set; }

        /// <summary>
        /// Is this a player spawner? If false it spawns bots.
        /// </summary>
        public bool IsPlayerSpawner { get; set; }

        /// <summary>
        /// The behavior of bots spawned from this spawner. Ignored for player spawners.
        /// </summary>
        public string Behavior { get; set; }

        /// <summary>
        /// The ship class for bots spawned from this spawner. Ignored for player spawners.
        /// </summary>
        public string ShipClass { get; set; }

        /// <summary>
        /// The weapon class for bots spawned from this spawner. Ignored for player spawners.
        /// </summary>
        public string WeaponClass { get; set; }
        
        /// <summary>
        /// The total number of alive bots this spawner creates. Ignored for player spawners.
        /// </summary>
        public int Population { get; set; }

        /// <summary>
        /// The time this spawner waits before each spawn attempt. Ignored for player spawners.
        /// </summary>
        public double SpawnDelay { get; set; }
    }
}
