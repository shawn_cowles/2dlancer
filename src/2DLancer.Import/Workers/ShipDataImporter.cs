﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using _2DLancer.Common.Data;
using _2DLancer.Import.Data;
using TiledSharp;

namespace _2DLancer.Import.Workers
{
    /// <summary>
    /// Imports ship data: thruster offsets, colliders, and the like.
    /// </summary>
    public class ShipDataImporter : AbstractWorker
    {
        /// <summary>
        /// The name of the worker.
        /// </summary>
        public override string Name
        {
            get { return "Ship Data Importer"; }
        }

        /// <summary>
        /// Import ships.
        /// </summary>
        /// <param name="importContext">The context to use for importing.</param>
        /// <returns>The result of the work.</returns>
        public override WorkResult DoWork(ImportContext importContext)
        {
            try
            {
                var shipFiles = Directory.EnumerateFiles(importContext.ShipPath)
                    .Where(f => Path.GetExtension(f) == ".tmx")
                    .ToArray();

                foreach (var shipFile in shipFiles)
                {
                    try
                    {
                        var map = new TmxMap(shipFile);

                        importContext.Ships.Add(ImportShipFromMap(
                            Path.GetFileNameWithoutExtension(shipFile),
                            map));
                    }
                    catch (Exception ex)
                    {
                        throw new ImportException($"Failed to import ship '{shipFile}'", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                return ReportOnException(ex);
            }

            return new WorkResult(Name, true, string.Empty);
        }

        private ShipData ImportShipFromMap(string className, TmxMap map)
        {
            var shipData = new ShipData();

            shipData.Name = className;
            shipData.Scale = GetDoubleProperty(map, PropertyNames.SHIP_SCALE);
            shipData.MaxEngineClass = GetIntProperty(map, PropertyNames.SHIP_ENGINE_CLASS);
            shipData.MaxShieldClass = GetIntProperty(map, PropertyNames.SHIP_SHIELD_CLASS);
            shipData.SpriteWidth = map.Width * map.TileWidth;
            shipData.SpriteHeight = map.Height * map.TileHeight;
            shipData.Health = GetIntProperty(map, PropertyNames.SHIP_HEALTH);
            shipData.Mass = GetIntProperty(map, PropertyNames.SHIP_MASS);
            shipData.BaseSprite = className + "_base";
            shipData.PaintSprite = className + "_paint";
            shipData.Drag = GetDoubleProperty(map, PropertyNames.SHIP_DRAG);
            shipData.TurretedWeapons = GetBoolProperty(map, PropertyNames.SHIP_HAS_TURRETS);


            foreach (var layer in map.ObjectGroups)
            {
                foreach (var obj in layer.Objects)
                {
                    try
                    {
                        switch(obj.Type)
                        {
                            case ObjectTypes.ENGINE:
                                ImportEngine(map, shipData, obj);
                                break;
                            case ObjectTypes.THRUSTER:
                                ImportThruster(map, shipData, obj);
                                break;
                            case ObjectTypes.COLLIDER:
                                ImportCollider(map, shipData, obj);
                                break;
                            case ObjectTypes.SHIELD:
                                ImportShield(map, shipData, obj);
                                break;
                            case ObjectTypes.DEBRIS:
                                ImportDebris(map, shipData, obj);
                                break;
                            case ObjectTypes.MODULE_SLOT:
                                ImportModuleSlot(map, shipData, obj);
                                break;
                            case ObjectTypes.WEAPON_BARREL:
                                // handled in ImpordModuleSlot
                                break;
                            default:
                                throw new ImportException($"Unknown object type '{obj.Type}'");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ImportException($"Failed to import object '{obj.Name}' in layer '{layer.Name}'", ex);
                    }
                }
            }

            var moduleTypes = shipData.ModuleSlots.Select(s => s.Type).Distinct();
            foreach(var type in moduleTypes)
            {
                var count = shipData.ModuleSlots.Count(s => s.Type == type);

                if(count > 1)
                {
                    throw new ImportException($"Ship has more than one module of type '{type}'");
                }
            }

            if(!moduleTypes.Contains(ModuleType.PrimaryWeapon))
            {
                throw new ImportException($"Ship doesn't have a primary weapon.");
            }

            return shipData;
        }

        private void ImportEngine(TmxMap map, ShipData ship, TmxObject engine)
        {
            ship.Engines.Add(new Engine
            {
                Offset = new Position(
                    engine.X - ship.SpriteWidth / 2,
                    engine.Y - ship.SpriteHeight / 2)
                    * ship.Scale,

                Width = GetDoubleProperty(engine, PropertyNames.SHIP_ENGINE_TRAIL_WIDTH)
                        * ship.Scale
            });
        }

        private void ImportThruster(TmxMap map, ShipData ship, TmxObject thruster)
        {
            var direction = 0.0;
            var directionName = GetProperty(thruster, PropertyNames.SHIP_THRUSTER_DIRECTION);

            switch(directionName)
            {
                case "rear":
                    direction = Math.PI;
                    break;
                case "left":
                    direction = -Math.PI / 2;
                    break;
                case "right":
                    direction = Math.PI / 2;
                    break;
                default:
                    throw new ImportException("Unknown thruster direction name: " + directionName);
            }

            ship.Thrusters.Add(new Thruster
            {
                Offset = new Position(
                    thruster.X - ship.SpriteWidth / 2,
                    thruster.Y - ship.SpriteHeight / 2)
                    * ship.Scale,

                Direction = direction
            });
        }
        
        private void ImportCollider(TmxMap map, ShipData ship, TmxObject collider)
        {
            ship.Colliders.Add(new Collider
            {
                Offset = new Position(
                    (collider.X + collider.Width / 2) - ship.SpriteWidth / 2,
                    (collider.Y + collider.Height/ 2) - ship.SpriteHeight / 2)
                    * ship.Scale,

                Radius = Math.Max(collider.Width, collider.Height) / 2
                        * ship.Scale
            });
        }

        private void ImportShield(TmxMap map, ShipData ship, TmxObject collider)
        {
            var radius = Math.Max(collider.Width, collider.Height) / 2 * ship.Scale;

            ship.ShieldRadius = Math.Max(ship.ShieldRadius, radius);
        }

        private void ImportDebris(TmxMap map, ShipData ship, TmxObject debris)
        {
            var offset = new Position(
                    (debris.X + debris.Width / 2) - ship.SpriteWidth / 2,
                    (debris.Y + debris.Height / 2) - ship.SpriteHeight / 2)
                    * ship.Scale;

            ship.Debris.Add(new DebrisInfo
            {
                LaunchDirection = Math.Atan2(offset.Y, offset.X),
                SpriteName = GetProperty(debris, PropertyNames.DEBRIS_SPRITE)
            });

            var radius = Math.Max(debris.Width, debris.Height) / 2 * ship.Scale;

            ship.ShieldRadius = Math.Max(ship.ShieldRadius, radius);
        }

        private void ImportModuleSlot(TmxMap map, ShipData ship, TmxObject engine)
        {
            var type = GetEnumProperty<ModuleType>(engine, PropertyNames.MODULE_TYPE);
            var size = GetEnumProperty<ModuleSize>(engine, PropertyNames.MODULE_SIZE);

            if (type == ModuleType.PrimaryWeapon)
            {
                var barrels = map.ObjectGroups
                    .SelectMany(g => g.Objects)
                    .Where(o => o.Type == ObjectTypes.WEAPON_BARREL)
                    .Select(barrel => new Position(
                            barrel.X - ship.SpriteWidth / 2,
                            barrel.Y - ship.SpriteHeight / 2)
                        * ship.Scale)
                    .ToArray();

                if(!barrels.Any())
                {
                    throw new ImportException($"Ship has no {ObjectTypes.WEAPON_BARREL} objects.");
                }

                ship.ModuleSlots[(int)type] = new ModuleSlot
                {
                    Offsets = barrels,

                    Size = size,

                    Type = type
                };
            }
            else
            {
                var offset = new Position(
                    engine.X - ship.SpriteWidth / 2,
                    engine.Y - ship.SpriteHeight / 2)
                    * ship.Scale;

                ship.ModuleSlots[(int)type] = new ModuleSlot
                {
                    Offsets = new [] { offset },

                    Size = size,

                    Type = type
                };
            }
        }
    }
}
