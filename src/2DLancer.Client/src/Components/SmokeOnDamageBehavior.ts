﻿/// <reference path="AbstractComponent.ts" />

// SmokeOnDamageBehavior creates smoke emitters as its GameObject takes damage.
class SmokeOnDamageBehavior extends AbstractComponent
{
    private smokeAreaWidth: number;
    private smokeAreaHeight: number;
    private health: HullHealth;
    private emitters: SmokeEmitter[];

    constructor(smokeAreaWidth: number, smokeAreaHeight: number)
    {
        super();

        this.smokeAreaWidth = smokeAreaWidth;
        this.smokeAreaHeight = smokeAreaHeight;
        this.emitters = [];
    }
    
    // Grab hull health
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.health = this.GameObject.GetComponent<HullHealth>(HullHealth);
    }

    // Create smoke emitters if needed
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);
        
        if (this.health == null)
        {
            return;
        }

        var emitterCount = Math.round((100 - this.health.HealthPercent()) / 20);

        // Add new emitters
        if (emitterCount > this.emitters.length)
        {
            var emitterOffset = new Vector2(
                (Math.random() - 0.5) * this.smokeAreaWidth,
                (Math.random() - 0.5) * this.smokeAreaHeight);

            var emitter = new SmokeEmitter(emitterOffset);

            this.emitters.push(emitter);
            this.GameObject.AddComponent(emitter);
        }

        // remove old emitters as repairs are made
        if (emitterCount < this.emitters.length)
        {
            this.GameObject.RemoveComponent(zone, this.emitters[this.emitters.length - 1]);
            this.emitters.splice(this.emitters.length - 1, 1);
        }
    }
}