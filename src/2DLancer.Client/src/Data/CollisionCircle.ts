﻿// A circle used to check for colisions.
class CollisionCircle
{
    private offsetMagnitude: number;
    private offsetDirection: number;
    public Radius: number;

    constructor(offset: Vector2, radius: number)
    {
        this.offsetDirection = offset.Direction();
        this.offsetMagnitude = offset.Magnitude();
        this.Radius = radius;
    }

    public Overlap(
        ownObject: GameObject,
        otherObject: GameObject,
        otherCircle: CollisionCircle): number
    {
        var ownPoint = Vector2.FromPolarOffset(
            this.offsetMagnitude,
            this.offsetDirection,
            ownObject);

        var otherPoint = Vector2.FromPolarOffset(
            otherCircle.offsetMagnitude,
            otherCircle.offsetDirection,
            otherObject);

        return (this.Radius + otherCircle.Radius) - ownPoint.DistanceTo(otherPoint);
    }

    // Does this CollisionCircle contain a specified point?
    public ContainsPoint(ownObject: GameObject, point: Vector2): boolean
    {
        var ownPoint = Vector2.FromPolarOffset(
            this.offsetMagnitude,
            this.offsetDirection,
            ownObject);

        return ownPoint.DistanceTo(point) <= this.Radius;
    }
}