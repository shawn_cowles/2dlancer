﻿/// <reference path="UiPanel.ts" />

// A widget that displays an interaction panel for docking with space stations.
class SpaceStationDockPanel extends UiPanel
{
    private static WIDTH = 300;
    private static HEIGHT = 100;

    private state: FlightState;

    private title: Phaser.Text;
    private text: Phaser.Text;
    private dockButton: TextButton;
    
    private proximityStation: SpaceStationBehavior;
    
    constructor()
    {
        super(new Phaser.Point(0, 0), SpaceStationDockPanel.WIDTH, SpaceStationDockPanel.HEIGHT);
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);

        this.state = state;

        var textStyle = {
            font: "18px courier",
            fill: "#00FFFF",
            align: "center",
        };

        this.text = state.game.add.text(0, 0, "", textStyle);
        this.text.anchor.set(0.5, 0.5);
        this.dockButton = new TextButton(state.game, 0, 0, "Dock at Station",
            () =>
            {
                if (this.proximityStation == null)
                {
                    return;
                }

                state.PlayerShip.AddComponent(new DockSequencer(this.proximityStation));
                state.Analytics.DockedAtStation(this.proximityStation.Name);
            },
            this);
        this.dockButton.anchor.set(0.5, 0.5);

        this.SetVisible(false);
    }
    
    // Update position and show/hide
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);

        var mousePos = new Vector2(
            state.input.activePointer.worldX,
            state.input.activePointer.worldY);

        this.proximityStation = null;
        var nearDist = 100; // functionally the minimum distance

        if (state.PlayerShip != null
            && !state.PlayerShip.ShouldBeRemoved
            && !state.PlayerShip.HasComponent(DockSequencer))
        {
            for (var i = 0; i < state.BackgroundSim.Stations.length; i++)
            {
                var station = state.BackgroundSim.Stations[i];

                if (station.NearestWaypoint.ZoneName == state.GetActiveZone().Name)
                {
                    var dist = station.GameObject.Position.DistanceTo(mousePos);

                    if (dist < nearDist)
                    {
                        this.proximityStation = station;
                        nearDist = dist;
                    }
                }
            }
        }
        
        if (this.proximityStation != null)
        {
            this.text.visible = true;
            this.dockButton.visible = true;
            this.SetVisible(true);

            this.text.text = this.proximityStation.Name;
            
            this.CenterPosition = this.proximityStation.GameObject.Position.ToPoint();
            
            this.text.position.set(
                this.CenterPosition.x,
                this.CenterPosition.y - 40);

            this.dockButton.position.set(
                this.CenterPosition.x,
                this.CenterPosition.y + 20);
            
            this.ForceUpdatePosition();
        }
        else
        {
            this.text.visible = false;
            this.dockButton.visible = false;
            this.SetVisible(false);
        }
    }

    public Destroy()
    {
        super.Destroy();

        this.text.destroy();
        this.dockButton.destroy();
    }
}