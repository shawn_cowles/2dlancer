﻿/// <reference path="AbstractStationPage.ts" />

// The station menu page for outfitting.
class OutfittingStationPage extends AbstractStationPage
{
    public BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void
    {
        title.text = "Outfitting";
        
        var moduleSlots = state.PlayerShip.GetComponents<ModuleSlot>(ModuleSlot);

        for (var i = 0; i < moduleSlots.length; i++)
        {
            if (moduleSlots[i].Size != _2DLancer.Common.Data.ModuleSize.None)
            {
                // becuase javascript scoping
                buttons.push(this.BuildOutfitSlotButton(moduleSlots[i], state, panel));
            }
        }

        buttons.push(new TextButton(state.game, 0, 0, "Back",
            () =>
            {
                panel.SetPage(new MainStationPage());
            },
            this));

        var descriptionElement = new Phaser.Text(state.game, 0, 0,
            Util.WrapText("Few pilots are satisfied with factory default equipment for their ships. Here skilled mechanics can refit ship modules, allowing a pilot to tailor their ship to their own specific needs.", 55),
            this.BodyTextStyle);
        state.game.add.existing(descriptionElement);
        mainBodyContent.push(descriptionElement);
    }

    protected OnUpdate(state: FlightState): void
    {
    }

    private BuildOutfitSlotButton(
        moduleSlot: ModuleSlot,
        state: FlightState,
        panel: SpaceStationInteractionPanel): TextButton
    {
        return new TextButton(state.game, 0, 0, moduleSlot.ModuleName(),
            () =>
            {
                panel.SetPage(new OutfitSlotPage(moduleSlot));
            },
            this);
    }
}