﻿/// <reference path="AbstractModule.ts" />

// The MissileLauncher launches seeking missiles.
class MissileLauncher extends AbstractModule
{
    public static DAMAGE = 20;
    
    private maxCooldown: number;
    private cooldownTimer: number;

    private offsetMagnitude: number;
    private offsetDirection: number;
    
    public Initialize(
        state: FlightState,
        parentShip: GameObject,
        slot: ModuleSlot,
        offsetMagnitudes: number[],
        offsetDirections: number[]): void
    {
        this.offsetMagnitude = offsetMagnitudes[0];
        this.offsetDirection = offsetDirections[0];

        this.maxCooldown = MissileLauncher.CooldownForSize(slot.Size);
        
        this.cooldownTimer = 0;
    }
    
    public Update(
        zone: Zone,
        state: FlightState,
        parentShip: GameObject,
        elapsedSeconds: number,
        isActivating: boolean): void
    {
        this.cooldownTimer -= elapsedSeconds;

        if (isActivating && this.cooldownTimer <= 0)
        {
            this.cooldownTimer = this.maxCooldown;

            this.LaunchMissile(zone, state, parentShip);
        }
    }
    
    public UnInitialize(parentShip: GameObject): void
    {
    }
    
    public CooldownPercent(): number
    {
        return Util.Clamp(this.cooldownTimer / this.maxCooldown, 1, 0);
    }

    public CanBeActivated(): boolean
    {
        return this.cooldownTimer <= 0;
    }

    public IconSprite(): string
    {
        return "icon_missile_launcher";
    }

    public Name(): string
    {
        return "Missile";
    }

    public GetListing(): AbstractModuleListing
    {
        return new MissileLauncherListing();
    }

    private LaunchMissile(zone: Zone, state: FlightState, parentShip: GameObject): void
    {
        var barrelPos = Vector2.FromPolarOffset(
            this.offsetMagnitude,
            this.offsetDirection,
            parentShip);

        var missile = new GameObject(barrelPos, parentShip.Facing);
        missile.AddComponent(new PhysicsBody(200));
        missile.PhysicsBody.Velocity = parentShip.PhysicsBody.Velocity;

        missile.AddComponent(new SpriteDisplay(
            "missile",
            FlightState.LAYER_MIDGROUND,
            HexColor.WHITE,
            1));
        missile.AddComponent(new Collider(
            [new CollisionCircle(Vector2.ZERO, 4)],
            true,
            parentShip));
        missile.AddComponent(new TimedLife(15, true));
        missile.AddComponent(new FactionIdentifier(parentShip.GetComponent<FactionIdentifier>(FactionIdentifier).FactionId));
        missile.AddComponent(new ExplodeOnDeath());
        missile.AddComponent(new ProjectileDamage(MissileLauncher.DAMAGE, parentShip, true));
        missile.AddComponent(new HomingProjectileBehavior(2000, false));
        missile.AddComponent(new SoundEffect("engine_base", 0.5, true, 3));
        missile.AddComponent(new SmokeEmitter(new Vector2(-17, 0)));
        missile.AddComponent(new Thrusters(1, 1.25));
        missile.AddComponent(new InputSource());
        missile.AddComponent(new MinimapDisplay(MinimapDisplay.TYPE_PROJECTILE));

        zone.GameObjects.push(missile);
    }
    
    public static CooldownForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): number
    {
        switch (moduleSize)
        {
            case _2DLancer.Common.Data.ModuleSize.Small:
                return 15;
            case _2DLancer.Common.Data.ModuleSize.Medium:
                return 10;
            case _2DLancer.Common.Data.ModuleSize.Large:
                return 5;
            default:
                throw "Unknown module size: " + moduleSize;
        }
    }
}

class MissileLauncherListing extends AbstractModuleListing
{
    constructor()
    {
        super();

        this.Name = "Missile";
        this.BaseCost = 200;
        this.Type = _2DLancer.Common.Data.ModuleType.SecondaryWeapon;
    }

    public BuildModule(): AbstractModule
    {
        return new MissileLauncher();
    }

    public GetDescriptionsForSize(moduleSize: _2DLancer.Common.Data.ModuleSize): string[]
    {
        var cooldown = MissileLauncher.CooldownForSize(moduleSize);
        var effectiveDps = MissileLauncher.DAMAGE / cooldown;

        return [
            "Cooldown: " + cooldown + "s"
            + "  Effective DPS: " + effectiveDps.toFixed(1),

            "A  missile launcher fires powerful autonomous seeking missiles. Each independently tracks and engages enemy ships. Larger missile launchers have additional launch tubes, and can fire more often."
        ];
    }
}