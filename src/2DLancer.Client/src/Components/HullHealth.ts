﻿/// <reference path="AbstractComponent.ts" />

// HullHeath lets a GameObject be hit by projectiles, damaged, and destroyed
class HullHealth extends AbstractComponent
{
    private health: number;
    private maxHealth: number;
    
    constructor(health: number)
    {
        super();

        this.maxHealth = health;
        this.health = health;
    }
    
    // Perform an active mode simulation update
    public Update(zone: Zone, state: FlightState, elapsedSeconds: number): void
    {
        super.Update(zone, state, elapsedSeconds);

        if (this.health <= 0)
        {
            this.GameObject.BroadcastMessage(zone, Messages.DESTROYED);
            this.GameObject.ShouldBeRemoved = true;
        }
    }

    public GetHealth(): number
    {
        return this.health;
    }

    public DealDamage(damage: number): void
    {
        this.health -= damage;
    }

    public HealthPercent(): number
    {
        return Math.round(this.health / this.maxHealth * 100);
    }

    public Repair(): void
    {
        this.health = this.maxHealth;
    }
}