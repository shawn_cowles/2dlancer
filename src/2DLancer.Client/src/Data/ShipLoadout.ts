﻿// Holds information for building a ship
class ShipLoadout
{
    public ShipClass: string;
    public Color: number;
    public FactionId: string;
    public ShowSpawnEffect: boolean;
    public MountedModules: string[];

    constructor(
        shipClass: string,
        color: number,
        factionId: string,
        showSpawnEffect: boolean,
        mountedModules: string[])
    {
        this.ShipClass = shipClass;
        this.Color = color;
        this.FactionId = factionId;
        this.ShowSpawnEffect = showSpawnEffect;
        this.MountedModules = mountedModules;
    }
}