﻿using System;

namespace _2DLancer.Server.Interfaces
{
    /// <summary>
    /// A provider of <see cref="ILogger"/>s.
    /// </summary>
    public interface ILoggerProvider
    {
        /// <summary>
        /// Get a logger for a type.
        /// </summary>
        /// <param name="t">The type to create a logger for.</param>
        /// <returns>A logger for the provided type.</returns>
        ILogger GetLoggerFor(Type t);
    }
}
