﻿/// <reference path="AbstractComponent.ts" />

// ExplodeOnDeath will spawn an explosion when the GameObject is destroyed.
class ExplodeOnDeath extends AbstractComponent
{
    public OnMessage(zone: Zone, message: string): void
    {
        super.OnMessage(zone, message);

        if (message == Messages.DESTROYED)
        {
            var blast = new GameObject(this.GameObject.Position, this.GameObject.Facing);
            blast.AddComponent(new TimedLife(0.5, false));
            blast.AddComponent(new GrowingAndFadingSprite(
                "large_blast", FlightState.LAYER_MIDGROUND, HexColor.WHITE, 0.5,
                0, 1, // scale
                1, 0)); // alpha
            zone.GameObjects.push(blast);
        }
    }
}