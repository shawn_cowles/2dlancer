﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages.Analytics
{
    /// <summary>
    /// An event for the player spawning, either the first spawn of the session or spawning after death.
    /// </summary>
    [TsClass(Module = "", Name = "IPlayerSpawnedEvent")]
    [DataContract]
    public class PlayerSpawnedEvent : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "PlayerSpawnedEvent";

        /// <summary>
        /// Construct a new PlayerSpawnedEvent.
        /// </summary>
        public PlayerSpawnedEvent() 
            : base(NAME)
        {
        }
    }
}
