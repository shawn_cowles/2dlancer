﻿/// <reference path="AbstractComponent.ts" />

// A component that displays a sprite at a fixed position relative to the camera
class CameraFixedSprite extends AbstractComponent
{
    protected spriteName: string;
    protected spriteLayer: number;
    protected tint: number;
    protected sprite: Phaser.Sprite;
    protected scale: number;
    protected cameraRelPosition: Vector2;

    constructor(
        spriteName: string,
        spriteLayer: number,
        tint: number,
        scale: number,
        cameraRelPosition: Vector2)
    {
        super();
        this.spriteName = spriteName;
        this.spriteLayer = spriteLayer;
        this.tint = tint;
        this.scale = scale;
        this.cameraRelPosition = cameraRelPosition;
    }

    // initialize the sprite display by creating the sprite in phaser
    public Initialize(zone: Zone, state: FlightState): void
    {
        super.Initialize(zone, state);

        this.sprite = state.SpriteGroups[this.spriteLayer].create(0, 0, this.spriteName);
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = this.tint;
        this.sprite.scale = new Phaser.Point(this.scale, this.scale);
    }

    // Perform a simulation update
    public LateUpdate(zone: Zone, state: FlightState): void
    {
        super.LateUpdate(zone, state);

        if (this.sprite == null)
        {
            return;
        }

        this.sprite.position.set(
            state.camera.x + this.cameraRelPosition.X * state.camera.width,
            state.camera.y + this.cameraRelPosition.Y * state.camera.height);
    }

    // Clean up the sprite
    public Uninitialize(): void
    {
        super.Uninitialize();

        if (this.sprite != null)
        {
            this.sprite.destroy();
            this.sprite = null;
        }
    }
}