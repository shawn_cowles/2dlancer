﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages.Analytics
{
    /// <summary>
    /// An event for the player transitioning to another zone.
    /// </summary>
    [TsClass(Module = "", Name = "IZoneVisitedEvent")]
    [DataContract]
    public class ZoneVisitedEvent : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "ZoneVisitedEvent";

        /// <summary>
        /// The name of the Zone that the player moved to.
        /// </summary>
        [DataMember]
        public string ZoneName { get; set; }

        /// <summary>
        /// Construct a new ZoneTransitionEvent.
        /// </summary>
        public ZoneVisitedEvent() 
            : base(NAME)
        {
        }
    }
}
