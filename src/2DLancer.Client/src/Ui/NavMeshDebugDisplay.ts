﻿/// <reference path="AbstractUiWidget.ts" />

// A widget that displays the NavMesh for debugging.
class NavMeshDebugDisplay extends AbstractUiWidget
{
    private graphics: Phaser.Graphics;
    
    constructor()
    {
        super();
    }

    // Initialize the widget before its first update
    public Initialize(state: FlightState): void
    {
        super.Initialize(state);
        
        this.graphics = state.game.add.graphics(0, 0);
    }

    // Draw the nav mesh
    public Update(state: FlightState, elapsedSeconds: number): void
    {
        super.Update(state, elapsedSeconds);
        
        this.graphics.clear();

        this.graphics.lineStyle(2, HexColor.WHITE, 0.5);

        for (var i = 0; i < state.Navigation.Waypoints.length; i++)
        {
            var waypoint = state.Navigation.Waypoints[i];

            if (waypoint.ZoneName == state.GetActiveZone().Name)
            {

                for (var k = 0; k < waypoint.DirectLinks.length; k++)
                {
                    var dst = waypoint.DirectLinks[k];

                    this.graphics.moveTo(waypoint.Position.X, waypoint.Position.Y);
                    this.graphics.lineTo(dst.Position.X, dst.Position.Y);
                }
            }
        }
    }

    // The debug display doesn't interfere with input
    public PointIsInPanel(point: Vector2): boolean
    {
        return false;
    }

    public Destroy()
    {
        super.Destroy();

        this.graphics.destroy();
    }
}