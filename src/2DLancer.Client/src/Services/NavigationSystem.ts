﻿// Stores the universe-wide navigation mesh
class NavigationSystem
{
    public Waypoints: Waypoint[];

    constructor()
    {
        this.Waypoints = [];
    }

    public Initialize(state: FlightState): void
    {
        var zoneData = <IZone[]>state.game.cache.getJSON("zones");

        var waypointLookup = new Array<Waypoint>();

        for (var i = 0; i < zoneData.length; i++)
        {
            var zone = zoneData[i];

            for (var j = 0; j < zone.NavPoints.length; j++)
            {
                var waypoint = this.BuildWaypoint(zone, zone.NavPoints[j]);

                waypointLookup[waypoint.Id] = waypoint;
                this.Waypoints.push(waypoint);
            }
        }

        for (var i = 0; i < zoneData.length; i++)
        {
            var zone = zoneData[i];

            for (var j = 0; j < zone.NavPoints.length; j++)
            {
                var data = zone.NavPoints[j];

                var waypoint = waypointLookup[data.Id];

                for (var k = 0; k < data.DirectLinks.length; k++)
                {
                    waypoint.DirectLinks.push(waypointLookup[data.DirectLinks[k]]);
                }

                for (var k = 0; k < data.JumpLinks.length; k++)
                {
                    waypoint.JumpLinks.push(waypointLookup[data.JumpLinks[k]]);
                }
            }
        }
    }

    private BuildWaypoint(zone: IZone, navPoint: INavPoint): Waypoint
    {
        return new Waypoint(navPoint.Id, zone.Name, Vector2.FromPosition(navPoint.Position));
    }

    public Path(position: Vector2, startZone: Zone, destination: Waypoint): Waypoint[]
    {
        var startWaypoint = this.FindStartWaypoint(position, startZone.Name);

        var open = new Array<Waypoint>();
        var closed = new Array<Waypoint>();

        var cameFrom = new Array<Waypoint>(); // used as a dictionary
        var distTo = new Array<number>(); // used as a dictionary

        open.push(startWaypoint);
        cameFrom[startWaypoint.Id] = null;
        distTo[startWaypoint.Id] = 0;
        
        while (open.length > 0)
        {
            var lowestIndex = this.GetIndexOfLowest(open, distTo);
            var current = open[lowestIndex];
            open.splice(lowestIndex, 1);
            closed.push(current);

            if (current == destination)
            {
                return this.ConstructPath(current, cameFrom);
            }
            
            for (var i = 0; i < current.DirectLinks.length; i++)
            {
                var neighbor = current.DirectLinks[i];
                var dist = current.Position.DistanceTo(current.DirectLinks[i].Position) + distTo[current.Id];

                if (closed.indexOf(neighbor) < 0)
                {
                    if (open.indexOf(neighbor) < 0)
                    {
                        distTo[neighbor.Id] = dist;
                        cameFrom[neighbor.Id] = current;
                        open.push(neighbor);
                    }
                    else
                    {
                        if (dist < distTo[neighbor.Id])
                        {
                            distTo[neighbor.Id] = dist;
                            cameFrom[neighbor.Id] = current;
                        }
                    }
                }
            }

            for (var i = 0; i < current.JumpLinks.length; i++)
            {
                var neighbor = current.JumpLinks[i];
                var dist = distTo[current.Id];

                if (closed.indexOf(neighbor) < 0)
                {
                    if (open.indexOf(neighbor) < 0)
                    {
                        distTo[neighbor.Id] = dist;
                        cameFrom[neighbor.Id] = current;
                        open.push(neighbor);
                    }
                    else
                    {
                        if (dist < distTo[neighbor.Id])
                        {
                            distTo[neighbor.Id] = dist;
                            cameFrom[neighbor.Id] = current;
                        }
                    }
                }
            }
        }
        
        throw "Path failure from position " + position + " in zone " + startZone.Name + " to " + destination + " in " + destination.ZoneName;
    }

    private FindStartWaypoint(position: Vector2, currentZoneName: string): Waypoint
    {
        var lowDist = <number>null;
        var nearPoint = <Waypoint>null;

        for (var i = 0; i < this.Waypoints.length; i++)
        {
            var waypoint = this.Waypoints[i];

            if (waypoint.ZoneName == currentZoneName)
            {
                var dist = position.DistanceTo(waypoint.Position);

                if (lowDist == null || dist < lowDist)
                {
                    lowDist = dist;
                    nearPoint = waypoint;
                }
            }
        }

        return nearPoint;
    }

    private GetIndexOfLowest(open: Waypoint[], distTo: number[]): number
    {
        var lowIndex = 0;

        for (var i = 0; i < open.length; i++)
        {
            if (distTo[i] < distTo[lowIndex])
            {
                lowIndex = i;
            }
        }

        return lowIndex;
    }

    private ConstructPath(current: Waypoint, cameFrom: Waypoint[]): Waypoint[]
    {
        var path = new Array<Waypoint>();

        while (current != null)
        {
            path.push(current);

            current = cameFrom[current.Id];
        }

        return path.reverse();
    }
}