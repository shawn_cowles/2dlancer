﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages
{
    /// <summary>
    /// Base class for any messages that can be passed to or from the client.
    /// </summary>
    [TsClass(Module = "", Name = "IAbstractClientMessage")]
    [DataContract]
    public abstract class AbstractClientMessage : AbstractMessage
    {
        /// <summary>
        /// The name of the message, used for determining which subclass to deserialize it into.
        /// </summary>
        [DataMember]
        public string MessageName { get; private set; }

        /// <summary>
        /// Construct a new AbstractClientMessage.
        /// </summary>
        /// <param name="messageName">The name of the message.</param>
        protected AbstractClientMessage(string messageName)
        {
            MessageName = messageName;
        }
    }
}
