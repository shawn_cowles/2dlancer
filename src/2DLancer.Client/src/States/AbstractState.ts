﻿// Base class for all 2DLancer states, exposes a typed property for LancerGame
abstract class AbstractState extends Phaser.State
{
    public LancerGame: LancerGame;

    preload()
    {
        this.LancerGame = <LancerGame>this.game;
    }
}