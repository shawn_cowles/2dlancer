﻿using System;
using System.IO;
using System.Windows.Forms;

namespace _2DLancer.Import
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">Command line arguments, argument 0 is the file to import, argument 1 is the destination directory.</param>
        [STAThread]
        static void Main(string[] args)
        {
            var importPath = "";
            var outputPath = "";

            if (args.Length >= 2)
            {
                importPath = Directory.GetParent(Path.GetDirectoryName(args[0])).FullName;
                outputPath = args[1];
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ImportResultsForm(importPath, outputPath));
        }
    }
}
