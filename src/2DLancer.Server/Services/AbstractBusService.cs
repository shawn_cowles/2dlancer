﻿using System;
using System.Linq;
using System.Threading;
using _2DLancer.Common.Messages;
using _2DLancer.Server.Interfaces;

namespace _2DLancer.Server.Services
{
    /// <summary>
    /// An abstract base for <see cref="IBusService"/>s.
    /// </summary>
    public abstract class AbstractBusService : IBusService
    {
        /// <summary>
        /// The millisecond delay between each poll of the message bus.
        /// </summary>
        private const int BUS_POLL_TICK = 30;

        private MessageBus _messageBus;
        private Timer _pollTimer;
        private Predicate<AbstractMessage> _messageMatchPredicate;
        
        /// <summary>
        /// Construct a new AbstractBusService.
        /// </summary>
        /// <param name="matchPredicate">The predicate to use to match messages to process.</param>
        protected AbstractBusService(Predicate<AbstractMessage> matchPredicate)
        {
            _messageMatchPredicate = matchPredicate;
        }

        /// <summary>
        /// Start processing messages.
        /// </summary>
        /// /// <param name="bus">The message bus to use for sending and getting messages.</param>
        public virtual void Start(MessageBus bus)
        {
            _messageBus = bus;
            _pollTimer = new Timer(PollBus, null, BUS_POLL_TICK, BUS_POLL_TICK);
        }
        
        /// <summary>
        /// Stop processing messages.
        /// </summary>
        public virtual void Stop()
        {
            _pollTimer.Dispose();
        }
        
        private void PollBus(object state)
        {
            var messages = _messageBus.GetMessages(_messageMatchPredicate);

            if (messages.Any())
            {
                HandleMessages(messages);
            }
        }

        /// <summary>
        /// Send a message on the bus.
        /// </summary>
        /// <param name="message">The message to send.</param>
        protected void SendMessage(AbstractMessage message)
        {
            _messageBus.SendMessage(message);
        }

        /// <summary>
        /// Handle messages from the bus.
        /// </summary>
        /// <param name="messages">The incoming messages.</param>
        protected abstract void HandleMessages(AbstractMessage[] messages);
    }
}
