﻿using System.Runtime.Serialization;
using TypeLite;

namespace _2DLancer.Common.Messages.Analytics
{
    /// <summary>
    /// An event for the player earning money.
    /// </summary>
    [TsClass(Module = "", Name = "IMoneyEarnedEvent")]
    [DataContract]
    public class MoneyEarnedEvent : AbstractIncomingClientMessage
    {
        /// <summary>
        /// The name of this message.
        /// </summary>
        public const string NAME = "MoneyEarnedEvent";

        /// <summary>
        /// The category of the income.
        /// </summary>
        [DataMember]
        public string Category { get; set; }

        /// <summary>
        /// The specific income source (item sold or event)
        /// </summary>
        [DataMember]
        public string Item { get; set; }

        /// <summary>
        /// The amount of credits earned.
        /// </summary>
        [DataMember]
        public double Amount { get; set; }

        /// <summary>
        /// Construct a new MoneyEarnedEvent.
        /// </summary>
        public MoneyEarnedEvent() 
            : base(NAME)
        {
        }
    }
}
