﻿class MoneySpentEvent implements IMoneySpentEvent
{
    public MessageName = "MoneySpentEvent";
    
    public Amount: number;
    public Category: string;
    public Item: string;

    constructor(category: string, item: string, amount: number)
    {
        this.Category = category;
        this.Item = item;
        this.Amount = amount;
    }
}