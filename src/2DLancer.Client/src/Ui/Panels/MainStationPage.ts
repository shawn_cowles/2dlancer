﻿/// <reference path="AbstractStationPage.ts" />

// The main station menu page, shows a summary of the station and buttons to access services.
class MainStationPage extends AbstractStationPage
{
    private repairButton: TextButton;

    public BuildContent(
        panel: SpaceStationInteractionPanel,
        state: FlightState,
        station: SpaceStationBehavior,
        title: Phaser.Text,
        buttons: TextButton[],
        mainBodyContent: Phaser.Sprite[]): void
    {
        title.text = station.Name;

        var repairText = "Repair - $" + this.RepairCostFor(state.PlayerShip);

        this.repairButton = new TextButton(state.game, 0, 0, repairText,
            () =>
            {
                var repairCost = this.RepairCostFor(state.PlayerShip);

                var balance = state.PlayerDataService.GetBalance();

                if (repairCost > 0 && balance >= repairCost)
                {
                    state.Analytics.ShipRepaired(station.Name);

                    state.PlayerDataService.SetBalance(balance - repairCost);
                    state.PlayerShip.GetComponent<HullHealth>(HullHealth).Repair();
                    state.Analytics.MoneySpent("service", "repairs", repairCost);
                }
            },
            this);

        buttons.push(this.repairButton);

        buttons.push(new TextButton(state.game, 0, 0, "Shipyard",
            () =>
            {
                panel.SetPage(new ShipyardStationPage());
            },
            this));

        buttons.push(new TextButton(state.game, 0, 0, "Outfitting",
            () =>
            {
                panel.SetPage(new OutfittingStationPage());
            },
            this));
        
        buttons.push(new TextButton(state.game, 0, 0, "Leave",
            () =>
            {
                state.GetActiveZone().GameObjects.push(state.PlayerShip);
                state.SetupFlightUI();
                // Zero out any velocity from docking
                state.PlayerShip.PhysicsBody.Velocity = Vector2.ZERO;
            },
            this));

        var descriptionElement = new Phaser.Text(state.game, 0, 0,
            Util.WrapText("Welcome to " + station.Name + ", to the left you can find buttons to access the various station services. Click “Leave” when you are finished.", 55),
            this.BodyTextStyle);
        state.game.add.existing(descriptionElement);
        mainBodyContent.push(descriptionElement);
    }

    protected OnUpdate(state: FlightState): void
    {
        this.repairButton.Text = "Repair - $" + this.RepairCostFor(state.PlayerShip);
    }

    private RepairCostFor(ship: GameObject): number
    {
        return Math.round(500 * (1 - ship.GetComponent<HullHealth>(HullHealth).HealthPercent() / 100));
    }
}